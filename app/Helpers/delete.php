<?php 

if (!function_exists('dt_delete_deleteAnyFolder')) {
    function dt_delete_deleteAnyFolder($dirname) {
        if (is_dir($dirname))
            $dir_handle = opendir($dirname);
        if(isset($dir_handle)){
            if (!$dir_handle)
                return false;
            while($file = readdir($dir_handle)) {
                if ($file != "." && $file != "..") {
                    if (!is_dir($dirname."/".$file))
                        unlink($dirname."/".$file);
                    else
                        dt_delete_deleteAnyFolder($dirname.'/'.$file);
                }
            }
            closedir($dir_handle);
            rmdir($dirname);
            return true;
        }
    }
}

if (!function_exists('dt_delete_deleteCacheFolder')) {
    function dt_delete_deleteCacheFolder($dirname) {
        if (is_dir($dirname))
            $dir_handle = opendir($dirname);
        if(isset($dir_handle)){
            if (!$dir_handle)
                return false;
            while($file = readdir($dir_handle)) {
                if ($file != "." && $file != "..") {
                    if (!is_dir($dirname."/".$file)){
                        $file_size = filesize($dirname."/".$file)/(1024*1024);
                        if($file_size >= 2){
                            unlink($dirname."/".$file);
                        }
                    }  
                    else{
                        dt_delete_deleteCacheFolder($dirname.'/'.$file);
                    }   
                }
            }
            closedir($dir_handle);
            // rmdir($dirname);
            return true;
        }
    }
}