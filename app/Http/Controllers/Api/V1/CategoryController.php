<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Content;
use App\Presenters\CategoryPresenter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Category\CategoryService;
use App\Presenters\PaginatorPresenter;
use App\Models\SyncHistory;
use App\Models\Category;
use App\Services\DrmSyncService;
use Illuminate\Support\Facades\Artisan;

class CategoryController extends Controller
{
    private $service;

    public function __construct(CategoryService $categoryService)
    {
        $this->service = $categoryService;
    }

    public function index(Request $request): \Illuminate\Http\JsonResponse
    {
        $data = $this->service->all($request->all())->toArray();
        $data = (new PaginatorPresenter($data))->presentBy(CategoryPresenter::class);

        return response()->json($data);
    }

    public function show($id): \Illuminate\Http\JsonResponse
    {
        $data = $this->service->getById($id);
        $data = (new CategoryPresenter($data))->get();

        return response()->json($data);
    }

    public function store(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $shop = Content::where([
                'content_type' => 'page',
                'url' => 'shop',
//                'title' => 'Shop',
            ])->first();

            if ($request->get('parent_id') == 0){
                $rel_id = $shop->id;
            }else{
                $rel_id = 0;
            }

            $requestData = array_merge($request->only('title', 'drm_ref_id'), [
                'url' => mw()->url_manager->slug($request->get('title')).'-'.rand(),
                'parent_id' => $request->get('parent_id'),
                'rel_type' => 'content',
                'rel_id' => $rel_id,
                'is_hidden' => 1,
                'category_subtype' => 'default',
                'data_type' => 'category',
            ]);
            $data = $this->service->store($requestData);

            app()->log_manager->save('is_system=y');

            return response()->json((new CategoryPresenter($data))->get());
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => $e->getMessage()], 422);
        }
    }

    public function update($id, Request $request): \Illuminate\Http\JsonResponse
    {
        $shop = Content::where([
            'content_type' => 'page',
            'url' => 'shop',
//            'title' => 'Shop',
        ])->first();

        if ($request->get('parent_id') == 0){
            $rel_id = $shop->id;
        }else{
            $rel_id = 0;
        }


        try {
            $data = $this->service->update($id, array_merge(
                    $request->only('title'),
                    [
                        'parent_id' => $request->get('parent_id'),
                        'rel_type' => 'content',
                        'rel_id' => $rel_id,
                        'category_subtype' => 'default',
                        'data_type' => 'category',
                    ]
                )
            );

            app()->log_manager->save('is_system=y');

            return response()->json((new CategoryPresenter($data))->get());
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => $e->getMessage()], 422);
        }
    }

    public function destroy($id): \Illuminate\Http\JsonResponse
    {
        try {
            $data = $this->service->destroy($id);
            app()->log_manager->save('is_system=y');

            return response()->json(['success' => true, 'message' => 'Category deleted successfully', 'data' => $data]);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => $e->getMessage()], 422);
        }
    }

    public function __destruct()
    {
        Artisan::call('cache:clear');
    }

}
