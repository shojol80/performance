<?php


namespace App\Services\Order;

use App\Models\Content;
use App\Enums\SyncEvent;
use App\Models\Category;
use App\Models\Order;
use App\Models\SyncHistory;
use App\Services\DrmSyncService;
use Illuminate\Support\Facades\DB;
use App\Services\BaseService;
use Illuminate\Support\Arr;

class OrderService extends BaseService
{
    public function all(array $filters = [])
    {
        $query = Order::whereHas('carts')->with('carts.content:id,ean,drm_ref_id');

        $limit = Arr::get($filters, 'limit', 20);

        $query_modify = $limit != '-1' ? $query->paginate($limit) : $query->get();

        $order_modify = query_modify($query_modify);
        return $order_modify;
    }

    public function getById($id)
    {
        return Order::with('carts')->find($id);
    }

    public function store(array $data)
    {
        return $this->saveOrder($data);
    }

    public function update($id, array $data)
    {
        return $this->saveOrder($data, $id);
    }

    public function destroy($id)
    {
        return Order::where('drm_ref_id', $id)->delete();
    }

    private function saveOrder($data, $id = null)
    {
        $product = Order::firstOrNew(['drm_ref_id' => $id]);
        $product->fill($data);
        $product->save();

        return $product;
    }

    public function syncOrderToDrm(SyncHistory $syncHistory)
    {
        $syncService = new DrmSyncService("http://165.22.24.129");
        switch ($syncHistory->sync_event) {
            case SyncEvent::CREATE:
                try {
                    $order = Order::with('carts')->with('carts.content:id,ean,drm_ref_id')->find($syncHistory->model_id);
                    $data = [];
                    if($order){

                        $taxs = mw()->tax_manager->get();
                        $tax_sum = 0;
                        $tax_sum = $taxs[0]['rate'];
                        $country_tax = taxRateCountry($order->country);
                        if(@$country_tax and !empty($country_tax)){
                            $tax_sum = $country_tax;
                        }
                        $country = @$order->country ?? null;
                        $order->carts->map(function ($cart) use ($country,$tax_sum){
                            $cart->ean = $cart->content->first()->ean;
                            $cart->price_with_tax = (function_exists('roundPrice')) ? roundPrice(taxPrice_cart($cart->price,$tax_sum)) : taxPrice_cart($cart->price,$tax_sum);
                            return $cart;

                        });
                        $shipping = array(
                            'name' => $order->shipping_name,
                            'address' => $order->address,
                            'country' => $order->country,
                            'city' => $order->city,
                            'zip' => $order->zip,
                        );
                        $blilling = array(
                            'name' => $order->billing_name,
                            'address' => $order->billing_address,
                            'country' => $order->billing_country,
                            'city' => $order->billing_city,
                            'zip' => $order->billing_zip,
                        );
                        $order->shipping_info = $shipping ?? [];
                        $order->blilling_info = $blilling ?? [];

                        $pay_methode = explode("/",$order->payment_gw);
                        $order->payment_gw = end($pay_methode) ?? $order->payment_gw;
                        $order->payment_type = $order->payment_gw;
                        $order->tax_rate = $tax_sum;
                        $order = $order->toArray();

                        $response = $syncService->storeOrder($order);
                        if (!empty($response['id'])) {
                            DB::table('cart_orders')->update([
                                'drm_ref_id' => $response['id']
                            ]);
                            $syncHistory->update([
                                'response' => json_encode($response),
                                'synced_at' => date('Y-m-d H:i:s'),
                            ]);
                        }
                    }
                    $syncHistory->increment('tries');
                } catch (\Exception $e) {
                    $syncHistory->update([
                        'exception'=>json_encode($e->getMessage()),
                        'tries' => $syncHistory->tries + 1
                    ]);
                }
                break;

            case SyncEvent::UPDATE:
                try {
                    $order = Order::with('carts')->find($syncHistory->model_id)->toArray();
                    if ($order && $order->drm_ref_id) {
                        $response = $syncService->updateOrder($order->drm_ref_id, $order);
                        if (!empty($response['id'])) {
                            DB::table('cart_orders')->update([
                                'drm_ref_id' => $response['id']
                            ]);
                            $syncHistory->update([
                                'response' => json_encode($response),
                                'synced_at' => date('Y-m-d H:i:s'),
                            ]);
                        }
                    }
                    $syncHistory->increment('tries');
                } catch (\Exception $e) {
                    $syncHistory->update([
                        'exception'=>json_encode($e->getMessage()),
                        'tries' => $syncHistory->tries + 1
                    ]);
                }
                break;
        }

        return $syncHistory;
    }
}
