<?php

/**
 * Constructor function.
 *
 * @param null $class
 *
 * @return mixed|\MicroweberPackages\Application
 */
function mw($class = null)
{
    return app($class);
}


if (!function_exists('app')) {
    /**
     * Get the available container instance.
     *
     * @param  string  $abstract
     * @param  array   $parameters
     * @return mixed|\MicroweberPackages\Application
     */
    function app($abstract = null, array $parameters = [])
    {
        if (is_null($abstract)) {
            return Container::getInstance();
        }

        return empty($parameters)
            ? Container::getInstance()->make($abstract)
            : Container::getInstance()->makeWith($abstract, $parameters);
    }
}

function mw_is_installed()
{
    return Config::get('microweber.is_installed');
}

if (!function_exists('d')) {
    function d($dump)
    {
        var_dump($dump);
    }
}

if (!function_exists('site_url')) {
    function site_url($add_string = false)
    {
        static $site_url;

        if (defined('MW_SITE_URL')) {
            $site_url = MW_SITE_URL;
        }


        if ($site_url == false) {
            $pageURL = 'http';
            if (is_https()) {
                $pageURL .= 's';
            }
            $subdir_append = false;
            if (isset($_SERVER['PATH_INFO'])) {
                // $subdir_append = $_SERVER ['PATH_INFO'];
            } elseif (isset($_SERVER['REDIRECT_URL'])) {
                $subdir_append = $_SERVER['REDIRECT_URL'];
            }

            $pageURL .= '://';

            if (isset($_SERVER['HTTP_HOST'])) {
                $pageURL .= $_SERVER['HTTP_HOST'];
            } elseif (isset($_SERVER['SERVER_NAME']) and isset($_SERVER['SERVER_PORT']) and $_SERVER['SERVER_PORT'] != '80' and $_SERVER['SERVER_PORT'] != '443') {
                $pageURL .= $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'];
            } elseif (isset($_SERVER['SERVER_NAME'])) {
                $pageURL .= $_SERVER['SERVER_NAME'];
            } elseif (isset($_SERVER['HOSTNAME'])) {
                $pageURL .= $_SERVER['HOSTNAME'];
            }
            $pageURL_host = $pageURL;
            $pageURL .= $subdir_append;
            $d = '';
            if (isset($_SERVER['SCRIPT_NAME'])) {
                $d = dirname($_SERVER['SCRIPT_NAME']);
                $d = trim($d, DIRECTORY_SEPARATOR);
            }

            if ($d == '') {
                $pageURL = $pageURL_host;
            } else {
                $pageURL_host = rtrim($pageURL_host, '/') . '/';
                $d = ltrim($d, '/');
                $d = ltrim($d, DIRECTORY_SEPARATOR);
                $pageURL = $pageURL_host . $d;
            }
            if (isset($_SERVER['QUERY_STRING'])) {
                //    $pageURL = str_replace($_SERVER['QUERY_STRING'], '', $pageURL);
            }

            $uz = parse_url($pageURL);
            //            if (isset($uz['query'])) {
            //                $pageURL = str_replace($uz['query'], '', $pageURL);
            //                $pageURL = rtrim($pageURL, '?');
            //            }

            $url_segs = explode('/', $pageURL);

            $i = 0;
            $unset = false;
            foreach ($url_segs as $v) {
                if ($unset == true and $d != '') {
                    unset($url_segs[$i]);
                }
                if ($v == $d and $d != '') {
                    $unset = true;
                }

                ++$i;
            }
            $url_segs[] = '';
            $site_url = implode('/', $url_segs);
        }
        if (defined('MW_SITE_URL_PATH_PREFIX')) {
            $site_url .= MW_SITE_URL_PATH_PREFIX;
        }

        if (!$site_url) {
            $site_url = 'http://localhost/';
        }

        return $site_url . $add_string;
    }
}

/**
 * Converts a path in the appropriate format for win or linux.
 *
 * @param string $path
 *                         The directory path.
 * @param bool $slash_it
 *                         If true, ads a slash at the end, false by default
 *
 * @return string The formatted string
 */
if (!function_exists('normalize_path')) {
    function normalize_path($path, $slash_it = true)
    {
        $path_original = $path;
        $s = DIRECTORY_SEPARATOR;
        $path = preg_replace('/[\/\\\]/', $s, $path);
        $path = str_replace($s . $s, $s, $path);
        if (strval($path) == '') {
            $path = $path_original;
        }
        if ($slash_it == false) {
            $path = rtrim($path, DIRECTORY_SEPARATOR);
        } else {
            $path .= DIRECTORY_SEPARATOR;
            $path = rtrim($path, DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR);
        }
        if (strval(trim($path)) == '' or strval(trim($path)) == '/') {
            $path = $path_original;
        }
        if ($slash_it == false) {
        } else {
            $path = $path . DIRECTORY_SEPARATOR;
            $path = reduce_double_slashes($path);
        }

        return $path;
    }
}

if (!function_exists('reduce_double_slashes')) {
    /**
     * Removes double slashes from sting.
     *
     * @param $str
     *
     * @return string
     */
    function reduce_double_slashes($str)
    {
        return preg_replace('#([^:])//+#', '\\1/', $str);
    }
}

if (!function_exists('lipsum')) {
    function lipsum($number_of_characters = false)
    {
        if ($number_of_characters == false) {
            $number_of_characters = 100;
        }

        $lipsum = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc quis justo et sapien varius gravida. Fusce porttitor consectetur risus ut tincidunt. Maecenas pellentesque nulla sodales enim consectetur commodo. Aliquam non dui leo, adipiscing posuere metus. Duis adipiscing auctor lorem ut pulvinar. Donec non magna massa, feugiat commodo felis. Donec ut nibh elit. Nulla pellentesque nulla diam, vitae consectetur neque.
        Etiam sed lorem augue. Vivamus varius tristique bibendum. Phasellus vitae tempor augue. Maecenas consequat commodo euismod. Aenean a lorem nec leo dignissim ultricies sed quis nisi. Fusce pellentesque tellus lectus, eu varius felis. Mauris lacinia facilisis metus, sed sollicitudin quam faucibus id.
        Donec ultrices cursus erat, non pulvinar lectus consectetur eu. Proin sodales risus a ante aliquet vel cursus justo viverra. Duis vel leo felis. Praesent hendrerit, sem vitae scelerisque blandit, enim neque pulvinar mi, vel lobortis elit dui vel dui. Donec ac sem sed neque consequat egestas. Curabitur pellentesque consequat ante, quis laoreet enim gravida eu. Donec varius, nisi non bibendum pellentesque, felis metus pretium ipsum, non vulputate eros magna ac sapien. Donec tincidunt porta tortor, et ornare enim facilisis vitae. Nulla facilisi. Cras ut nisi ac dolor lacinia tempus at sed eros. Integer vehicula arcu in augue adipiscing accumsan. Morbi placerat consectetur sapien sed gravida. Sed fringilla elit nisl, nec molestie felis. Nulla aliquet diam vitae diam iaculis porttitor.
        Integer eget tortor nulla, non dapibus erat. Sed ultrices consectetur quam at scelerisque. Nullam varius hendrerit nisl, ac cursus mi bibendum eu. Phasellus varius fermentum massa, sit amet ornare quam malesuada in. Quisque ac massa sem. Nulla eu erat metus, non tincidunt nibh. Nam consequat interdum nulla, at congue libero tincidunt eget. Sed cursus nulla eu felis faucibus porta. Nam sed lacus eros, nec pellentesque lorem. Sed dapibus, sapien mattis sollicitudin bibendum, libero augue dignissim felis, eget elementum felis nulla in velit. Donec varius, lectus non suscipit sollicitudin, urna est hendrerit nulla, vel vehicula arcu sem volutpat sapien. Ut nisi ipsum, accumsan vestibulum pulvinar eu, sodales id lacus. Nulla iaculis eros sit amet lectus tincidunt mattis. Ut eu nisl sit amet eros vestibulum imperdiet ut vel lorem. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
        In hac habitasse platea dictumst. Aenean vehicula auctor eros non tincidunt. Donec tempor arcu ac diam sagittis mattis. Aenean eget augue nulla, non volutpat lorem. Praesent ut cursus magna. Mauris consequat suscipit nisi. Integer eu venenatis ligula. Maecenas leo risus, lacinia et auctor aliquet, aliquet in mi.
        Aliquam tincidunt dapibus augue, et vulputate dui aliquet et. Praesent pharetra mauris eu justo dignissim venenatis ornare nec nisl. Aliquam justo quam, varius eget congue vel, congue eget est. Ut nulla felis, luctus imperdiet molestie et, commodo vel nulla. Morbi at nulla dapibus enim bibendum aliquam non et ipsum. Phasellus sed cursus justo. Praesent sit amet metus lorem. Vivamus ut lorem dapibus turpis rhoncus pharetra. Donec in lacus sagittis nisl tempor sagittis quis a orci. Nam volutpat condimentum ante ac facilisis. Cras sem magna, vulputate id consequat rhoncus, suscipit non justo. In fringilla dignissim cursus.
        Nunc fringilla orci tellus, et euismod lorem. Ut quis turpis lacus, ac elementum lorem. Praesent fringilla, metus nec tincidunt consequat, sem sapien hendrerit nisi, nec feugiat libero risus a nisl. Duis arcu magna, ullamcorper et semper vitae, tincidunt nec libero. Etiam sed lacus ante. In imperdiet arcu eget elit commodo ut malesuada sem congue. Quisque porttitor porta sagittis. Nam porta elit sit amet mauris fermentum eu feugiat ipsum pretium. Maecenas sollicitudin aliquam eros, ut pretium nunc faucibus quis. Mauris id metus vitae libero viverra adipiscing quis ut nulla. Pellentesque posuere facilisis nibh, facilisis vehicula felis facilisis nec.
        Etiam pharetra libero nec erat pellentesque laoreet. Sed eu libero nec nisl vehicula convallis nec non orci. Aenean tristique varius nisl. Cras vel urna eget enim placerat vehicula quis sed velit. Quisque lacinia sagittis lectus eget sagittis. Pellentesque cursus suscipit massa vel ultricies. Quisque hendrerit lobortis elit interdum feugiat. Sed posuere volutpat erat vel lobortis. Vivamus laoreet mattis varius. Fusce tincidunt accumsan lorem, in viverra lectus dictum eu. Integer venenatis tristique dolor, ac porta lacus pellentesque pharetra. Suspendisse potenti. Ut dolor dolor, sollicitudin in auctor nec, facilisis non justo. Mauris cursus euismod gravida. In at orci in sapien laoreet euismod.
        Mauris purus urna, vulputate in malesuada ac, varius eget ante. Integer ultricies lacus vel magna dictum sit amet euismod enim dictum. Aliquam iaculis, ipsum at tempor bibendum, dolor tortor eleifend elit, sed fermentum magna nibh a ligula. Phasellus ipsum nisi, porta quis pellentesque sit amet, dignissim vel felis. Quisque condimentum molestie ligula, ac auctor turpis facilisis ac. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Praesent molestie leo velit. Sed sit amet turpis massa. Donec in tortor quis metus cursus iaculis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In hac habitasse platea dictumst. Proin leo nisl, faucibus non sollicitudin et, commodo id diam. Aliquam adipiscing, lorem a fringilla blandit, felis dui tristique ligula, vitae eleifend orci diam eget quam. Aliquam vulputate gravida leo eget eleifend. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
        Etiam et consectetur justo. Integer et ante dui, quis rutrum massa. Fusce nibh nisl, congue sit amet tempor vitae, ornare et nisi. Nulla mattis nisl ut ligula sagittis aliquam. Curabitur ac augue at velit facilisis venenatis quis sit amet erat. Donec lacus elit, auctor sed lobortis aliquet, accumsan nec mi. Quisque non est ante. Morbi vehicula pulvinar magna, quis luctus tortor varius et. Donec hendrerit nulla posuere odio lobortis interdum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec dapibus magna id ante sodales tempus. Maecenas at eleifend nulla.
        Sed eget gravida magna. Quisque vulputate diam nec libero faucibus vitae fringilla ligula lobortis. Aenean congue, dolor ut dapibus fermentum, justo lectus luctus sem, et vestibulum lectus orci non mauris. Vivamus interdum mauris at diam scelerisque porta mollis massa hendrerit. Donec condimentum lacinia bibendum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nam neque dolor, faucibus sed varius sit amet, vulputate vitae nunc.
        Etiam in lorem congue nunc sollicitudin rhoncus vel in metus. Integer luctus semper sem ut interdum. Sed mattis euismod diam, at porta mauris laoreet quis. Nam pellentesque enim id mi vestibulum gravida in vel libero. Nulla facilisi. Morbi fringilla mollis malesuada. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum sagittis consectetur auctor. Phasellus convallis turpis eget diam tristique feugiat. In consectetur quam faucibus purus suscipit euismod quis sed quam. Curabitur eget sodales dui. Quisque egestas diam quis sapien aliquet tincidunt.
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam velit est, imperdiet ac posuere non, dictum et nunc. Duis iaculis lacus in libero lacinia ut consectetur nisi facilisis. Fusce aliquet nisl id eros dapibus viverra. Phasellus eget ultrices nisl. Nullam euismod tortor a metus hendrerit convallis. Donec dolor magna, fringilla in sollicitudin sit amet, tristique eget elit. Praesent adipiscing magna in ipsum vulputate non lacinia metus vestibulum. Aenean dictum suscipit mollis. Nullam tristique commodo dapibus. Fusce in tellus sapien, at vulputate justo. Nam ornare, lorem sit amet condimentum ultrices, ipsum velit tempor urna, tincidunt convallis sapien enim eget leo. Proin ligula tellus, ornare vitae scelerisque vitae, fringilla fermentum sem. Phasellus ornare, diam sed luctus condimentum, nisl felis ultricies tortor, ac tempor quam lacus sit amet lorem. Nunc egestas, nibh ornare dictum iaculis, diam nisl fermentum magna, malesuada vestibulum est mauris quis nisl. Ut vulputate pharetra laoreet.
        Donec mattis mauris et dolor commodo et pellentesque libero congue. Sed tristique bibendum augue sed auctor. Sed in ante enim. In sed lectus massa. Nulla imperdiet nisi at libero faucibus sagittis ac ac lacus. In dui purus, sollicitudin tempor euismod euismod, dapibus vehicula elit. Aliquam vulputate, ligula non dignissim gravida, odio elit ornare risus, a euismod est odio nec ipsum. In hac habitasse platea dictumst. Mauris posuere ultrices mattis. Etiam vitae leo vitae nunc porta egestas at vitae nibh. Sed pharetra, magna nec bibendum aliquam, dolor sapien consequat neque, sit amet euismod orci elit vitae enim. Sed erat metus, laoreet quis posuere id, congue id velit. Mauris ac velit vel ipsum dictum ornare eget vitae arcu. Donec interdum, neque at lacinia imperdiet, ante libero convallis quam, pellentesque faucibus quam dolor id est. Ut cursus facilisis scelerisque. Sed vitae ligula in purus malesuada porta.
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vestibulum vestibulum metus. Integer ultrices ultricies pellentesque. Nulla gravida nisl a magna gravida ullamcorper. Vestibulum accumsan eros vel massa euismod in aliquam felis suscipit. Ut et purus enim, id congue ante. Mauris magna lectus, varius porta pellentesque quis, dignissim in est. Nulla facilisi. Nullam in malesuada mauris. Ut fermentum orci neque. Aliquam accumsan justo a lacus vestibulum fermentum. Donec molestie, quam id adipiscing viverra, massa velit aliquam enim, vitae dapibus turpis libero id augue. Quisque mi magna, mollis vel tincidunt nec, adipiscing sed metus. Maecenas tincidunt augue quis felis dapibus nec elementum justo fringilla. Sed eget massa at sapien tincidunt porta eu id sapien.';

        return character_limiter($lipsum, $number_of_characters, '');
    }
}

/**
 * Returns the current microtime.
 *
 * @return bool|string $date The current microtime
 *
 * @category Date
 *
 * @link     http://www.webdesign.org/web-programming/php/script-execution-time.8722.html#ixzz2QKEAC7PG
 */
if (!function_exists('microtime_float')) {
    function microtime_float()
    {
        list($msec, $sec) = explode(' ', microtime());
        $microtime = (float)$msec + (float)$sec;

        return $microtime;
    }
}

/**
 * Limits a string to a number of characters.
 *
 * @param        $str
 * @param int $n
 * @param string $end_char
 *
 * @return string
 *
 * @category Strings
 */
if (!function_exists('character_limiter')) {
    function character_limiter($str, $n = 500, $end_char = '&#8230;')
    {
        if (strlen($str) < $n) {
            return $str;
        }
        $str = strip_tags($str);
        $str = preg_replace("/\s+/", ' ', str_replace(array("\r\n", "\r", "\n"), ' ', $str));

        if (strlen($str) <= $n) {
            return $str;
        }

        $out = '';
        foreach (explode(' ', trim($str)) as $val) {
            $out .= $val . ' ';

            if (strlen($out) >= $n) {
                $out = trim($out);

                return (strlen($out) == strlen($str)) ? $out : $out . $end_char;
            }
        }
    }
}



function api_url($str = '')
{
    $str = ltrim($str, '/');

    return site_url('api/' . $str);
}

function api_nosession_url($str = '')
{
    $str = ltrim($str, '/');

    return site_url('api_nosession/' . $str);
}

function auto_link($text)
{
    return mw()->format->auto_link($text);
}

function prep_url($text)
{
    return mw()->format->prep_url($text);
}

function is_arr($var)
{
    return isarr($var);
}

function isarr($var)
{
    if (is_array($var) and !empty($var)) {
        return true;
    } else {
        return false;
    }
}

function array_search_multidimensional($array, $column, $key)
{
    return (array_search($key, array_column($array, $column)));
}

if (!function_exists('is_ajax')) {
    function is_ajax()
    {
        return mw()->url_manager->is_ajax();
    }
}
if (!function_exists('url_current')) {
    function url_current($skip_ajax = false, $no_get = false)
    {
        return mw()->url_manager->current($skip_ajax, $no_get);
    }
}
if (!function_exists('url_segment')) {
    function url_segment($k = -1, $page_url = false)
    {
        return mw()->url_manager->segment($k, $page_url);
    }
}

/**
 * Returns the curent url path, does not include the domain name.
 *
 * @return string the url string
 */
function url_path($skip_ajax = false)
{
    return mw()->url_manager->string($skip_ajax);
}

/**
 * Returns the curent url path, does not include the domain name.
 *
 * @return string the url string
 */
function url_string($skip_ajax = false)
{
    return mw()->url_manager->string($skip_ajax);
}

function url_title($text)
{
    return mw()->url_manager->slug($text);
}

function url_param($param, $skip_ajax = false)
{
    return mw()->url_manager->param($param, $skip_ajax);
}

function url_set_param($param, $value)
{
    return site_url(mw()->url_manager->param_set($param, $value));
}

function url_unset_param($param)
{
    return site_url(mw()->url_manager->param_unset($param));
}

/**
 *  Gets the data from the cache.
 *
 *  If data is not found it return false
 *
 *
 * @example
 * <code>
 *
 * $cache_id = 'my_cache_'.crc32($sql_query_string);
 * $cache_content = cache_get_content($cache_id, 'my_cache_group');
 *
 * </code>
 *
 * @param string $cache_id id of the cache
 * @param string $cache_group (default is 'global') - this is the subfolder in the cache dir.
 * @param bool $expiration_in_seconds You can pass custom cache object or leave false.
 *
 * @return mixed returns array of cached data or false
 */
function cache_get($cache_id, $cache_group = 'global', $expiration = false)
{
    return mw()->cache_manager->get($cache_id, $cache_group, $expiration);
}

/**
 * Stores your data in the cache.
 * It can store any value that can be serialized, such as strings, array, etc.
 *
 * @example
 * <code>
 * //store custom data in cache
 * $data = array('something' => 'some_value');
 * $cache_id = 'my_cache_id';
 * $cache_content = cache_save($data, $cache_id, 'my_cache_group');
 * </code>
 *
 * @param mixed $data_to_cache
 *                                      your data, anything that can be serialized
 * @param string $cache_id
 *                                      id of the cache, you must define it because you will use it later to
 *                                      retrieve the cached content.
 * @param string $cache_group
 *                                      (default is 'global') - this is the subfolder in the cache dir.
 * @param int $expiration_in_seconds
 *
 * @return bool
 */
function cache_save($data_to_cache, $cache_id, $cache_group = 'global', $expiration_in_seconds = false)
{
    return mw()->cache_manager->save($data_to_cache, $cache_id, $cache_group, $expiration_in_seconds);
}

api_expose_admin('clearcache');
/**
 * Clears all cache data.
 *
 * @example
 *          <code>
 *          //delete all cache
 *          clearcache();
 *          </code>
 *
 * @return bool
 */
function clearcache()
{
    mw()->cache_manager->clear();
    mw()->template->clear_cache();
    $empty_folder = userfiles_path() . 'cache' . DS;

    if (is_dir($empty_folder)) {
        rmdir_recursive($empty_folder, true);
    }

    if (!is_dir($empty_folder)) {
        mkdir_recursive($empty_folder);
    }

    $empty_folder = mw_cache_path() . 'composer';
    if (is_dir($empty_folder)) {
        rmdir_recursive($empty_folder, false);
    }
    if (!is_dir($empty_folder)) {
        mkdir_recursive($empty_folder);
    }

    if (isset($_GET['redirect_to'])) {
        return redirect($_GET['redirect_to']);
    }

    return true;
}

/**
 * Prints cache debug information.
 *
 * @return array
 *
 * @example
 * <code>
 * //get cache items info
 *  $cached_items = cache_debug();
 * print_r($cached_items);
 * </code>
 */
function cache_debug()
{
    return mw()->cache_manager->debug();
}

/**
 * Deletes cache for given $cache_group recursively.
 *
 * @param string $cache_group
 *                            (default is 'global') - this is the subfolder in the cache dir.
 *
 * @return bool
 *
 * @example
 * <code>
 * //delete the cache for the content
 *  cache_clear("content");
 *
 * //delete the cache for the content with id 1
 *  cache_clear("content/1");
 *
 * //delete the cache for users
 *  cache_clear("users");
 *
 * //delete the cache for your custom table eg. my_table
 * cache_clear("my_table");
 * </code>
 */
function cache_clear($cache_group = 'global')
{
    return mw()->cache_manager->delete($cache_group);
}

//same as cache_clear
function cache_delete($cache_group = 'global')
{
    return mw()->cache_manager->delete($cache_group);
}


if (!function_exists('is_cli')) {
    function is_cli()
    {
        static $is;
        if ($is !== null) {
            return $is;
        }
        if (
            function_exists('php_sapi_name') and
            php_sapi_name() === 'apache2handler'
        ) {
            $is = false;
            return false;
        }


        if (
            defined('STDIN')
            or php_sapi_name() === 'cli'
            or php_sapi_name() === 'cli-server'
            or array_key_exists('SHELL', $_ENV)

        ) {
            $is = true;
            return true;
        }


        $is = false;
        return false;
    }
}


if (!function_exists('is_https')) {
    function is_https()
    {
        if (isset($_SERVER['HTTPS']) and (strtolower($_SERVER['HTTPS']) == 'on')) {
            return true;
        } else if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) and (strtolower($_SERVER['HTTP_X_FORWARDED_PROTO']) == 'https')) {
            return true;
        }
        return false;
    }
}
if (!function_exists('is_closure')) {
    function is_closure($t)
    {
        return is_object($t) or ($t instanceof \Closure);
    }
}

if (!function_exists('collection_to_array')) {
    function collection_to_array($data)
    {
        if (
            $data instanceof \Illuminate\Database\Eloquent\Collection
            or $data instanceof \Illuminate\Support\Collection
            or $data instanceof \Illuminate\Database\Eloquent\Model
        ) {
            return $data->toArray();
        }
        return $data;
    }
}


function str_replace_bulk($search, $replace, $subject, &$count = null)
{
    // Assumes $search and $replace are equal sized arrays
    $lookup = array_combine($search, $replace);
    $result = preg_replace_callback(
        '/' .
            implode('|', array_map(
                function ($s) {
                    return preg_quote($s, '/');
                },
                $search
            )) .
            '/',
        function ($matches) use ($lookup) {
            return $lookup[$matches[0]];
        },
        $subject,
        -1,
        $count
    );
    if (
        $result !== null ||
        count($search) < 2 // avoid infinite recursion on error
    ) {
        return $result;
    }
    // With a large number of replacements (> ~2500?),
    // PHP bails because the regular expression is too large.
    // Split the search and replacements in half and process each separately.
    // NOTE: replacements within replacements may now occur, indeterminately.
    $split = (int)(count($search) / 2);
    $result = str_replace_bulk(
        array_slice($search, $split),
        array_slice($replace, $split),
        str_replace_bulk(
            array_slice($search, 0, $split),
            array_slice($replace, 0, $split),
            $subject,
            $count1
        ),
        $count2
    );
    $count = $count1 + $count2;
    return $result;
}


/**
 * @param $money
 * @return formated_money
 */
function format_money_pdf($money, $currency = null)
{
    if (!$currency) {
        $currency = \MicroweberPackages\Invoice\Currency::findOrFail(\MicroweberPackages\Invoice\CompanySetting::getSetting('currency', 1));
    }

    $format_money = number_format(
        $money,
        $currency->precision,
        $currency->decimal_separator,
        $currency->thousand_separator
    );

    $currency_with_symbol = '';
    if ($currency->swap_currency_symbol) {
        $currency_with_symbol = $format_money . '<span style="font-family: DejaVu Sans;">' . $currency->symbol . '</span>';
    } else {
        $currency_with_symbol = '<span style="font-family: DejaVu Sans;">' . $currency->symbol . '</span>' . $format_money;
    }
    return $currency_with_symbol;
}

if (!function_exists('array_recursive_diff')) {
    function array_recursive_diff($aArray1, $aArray2)
    {
        $aReturn = array();

        foreach ($aArray1 as $mKey => $mValue) {
            if (array_key_exists($mKey, $aArray2)) {
                if (is_array($mValue)) {
                    $aRecursiveDiff = array_recursive_diff($mValue, $aArray2[$mKey]);
                    if (count($aRecursiveDiff)) {
                        $aReturn[$mKey] = $aRecursiveDiff;
                    }
                } else {
                    if ($mValue != $aArray2[$mKey]) {
                        $aReturn[$mKey] = $mValue;
                    }
                }
            } else {
                $aReturn[$mKey] = $mValue;
            }
        }
        return $aReturn;
    }
}



function cat_reset_logic()
{
    $cats = \App\Models\Category::get();
    if (isset($cats)) {
        foreach ($cats as $cat) {
            $catItem = \App\Models\CategoryItem::where('parent_id', $cat->id)->first();

            if (isset($catItem)) {
                $all_content = DB::table('content')->where('id', $catItem->rel_id)->where('is_deleted', 0)->first();
                if (!isset($all_content)) {
                    $catItem->delete();
                }
                \App\Models\Category::where('id', $cat->id)->update(['is_hidden' => 0]);
            } else {
                \App\Models\Category::where('id', $cat->id)->update(['is_hidden' => 1]);
            }
        }
    }
}


function abandoned_shopping_carts()
{

    $session_id = mw()->user_manager->session_id();
    $user_id = user_id();
    $query = \Illuminate\Support\Facades\DB::table('cart');
    if ($user_id) {
        $query = $query->where('created_by', $user_id);
    } else {
        $user_id = null;
        $query = $query->where('created_by', null);
    }
    $all = $query->orderBy('created_at', 'desc')->first();
    if (isset($all) and $all->session_id != $session_id) {
        $userToken = Config::get('microweber.userToken');
        $userPassToken = Config::get('microweber.userPassToken');
        $drm_auth = array(
            'userToken' => $userToken,
            'userPassToken' => $userPassToken
        );

        $abandoned_carts['userToken'] = $userToken;
        $abandoned_carts['userPassToken'] = $userPassToken;

        $abandoned_carts['results'] = \App\Models\Cart::with(['creator:id,username,email,first_name,middle_name,last_name,phone,role', 'content:id,content_type,subtype,url,title,parent,description'])->where('order_completed', '0')->where('created_by', $user_id)->where('session_id', '!=', $session_id)->get(['id', 'title', 'rel_id', 'rel_type', 'price', 'session_id', 'qty', 'order_completed', 'created_by']);

        if (count($abandoned_carts['results']) > 0) {
            $userToken = Config::get('microweber.userToken');
            $userPassToken = Config::get('microweber.userPassToken');
            try {

                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => 'https://eu-dropshipping.com/api/v1/dt_abandoned_carts',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => array('data' => $abandoned_carts),
                    CURLOPT_HTTPHEADER => array(
                        'userToken: ' . Config::get('microweber.userToken'),
                        'userPassToken: ' . Config::get('microweber.userPassToken'),
                        'Cookie: SameSite=None'
                    ),
                ));

                $response = curl_exec($curl);
            } catch (Exception $e) {
                return true;
            }
        }
    }
}

function new_wishlists()
{

    $userToken = Config::get('microweber.userToken');
    $userPassToken = Config::get('microweber.userPassToken');
    $drm_auth = array(
        'userToken' => $userToken,
        'userPassToken' => $userPassToken
    );
    $wishLists['userToken'] = $userToken;
    $wishLists['userPassToken'] = $userPassToken;

    $wishLists['results'] = \App\Models\WishlistSession::with(['user:id,username,email,first_name,middle_name,last_name,phone,role', 'wlproducts:id,wishlist_id,product_id', 'wlproducts.product:id,content_type,subtype,url,title,parent,description'])->get(['id', 'user_id', 'name']);

    if (count($wishLists['results']) > 0) {
        try {

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://eu-dropshipping.com/api/v1/dt_wishlist',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => array('data' => $wishLists),
                CURLOPT_HTTPHEADER => array(
                    'userToken: ' . Config::get('microweber.userToken'),
                    'userPassToken: ' . Config::get('microweber.userPassToken'),
                    'Cookie: SameSite=None'
                ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);
        } catch (Exception $e) {
            return true;
        }
    }
}


function blog_word_limit()
{
    $limit = Config::get('custom.blog_limit');
    if (!$limit) {
        Config::set('custom.blog_limit', 1000);
        Config::save(array('custom'));
    }
    return Config::get('custom.blog_limit');
}

function get_drm_product_limit()
{
    $userToken = Config::get('microweber.userToken');
    $userPassToken = Config::get('microweber.userPassToken');

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://drm.software/api/drm-product-count?token=dtm3decr435mpdt&userToken=' . $userToken . '&userPassToken=' . $userPassToken,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    return $response ?? [];
}

function query_modify($data)
{

    //detect pagination
    $is_paginator = $data instanceof \Illuminate\Pagination\LengthAwarePaginator;
    $collection = $is_paginator ? $data->getCollection() : $data;

    if (!($collection instanceof  \Illuminate\Support\Collection)) return [];

    $taxs = mw()->tax_manager->get();
    $tax_sum = 0;
    $tax_sum =  $taxs[0]['rate'];

    $items = $collection->map(function ($item) use ($tax_sum) {
        $country_tax = taxRateCountry(@$item->country);
        if (@$country_tax and !empty($country_tax)) {
            $tax_sum = $country_tax;
        }
        $country = @$item->country ?? null;
        $carts = $item->carts->map(function ($cart) use ($country, $tax_sum) {

            $cart->ean = @$cart->content->first()->ean ?? null;
            $cart->price_with_tax = (function_exists('roundPrice')) ? roundPrice(taxPrice_cart($cart->price, $tax_sum)) : taxPrice_cart($cart->price, $tax_sum);

            return $cart;
        });
        //        foreach ($item->carts as $cart){
        //            dd();
        //        }
        $pay_methode = explode("/", $item->payment_gw);
        $item->payment_gw = end($pay_methode) ?? $item->payment_gw;
        $item->payment_type = $item->payment_gw;
        $item->tax_rate = $tax_sum;
        return $item;
    });



    if ($is_paginator) {
        $data->setCollection($items);
        return $data;
    }


    return $items;
}

function showCat($data)
{
    if ($data == 'header') {
        $a['header'] = 'show';
        $a['sidebar'] = 'hide';
    } else if ($data == 'sidebar') {
        $a['header'] = 'hide';
        $a['sidebar'] = 'show';
    }
    return $a;
}

function taxPrice_cart($price, $tax_rate)
{

    if (@$price && @$tax_rate) {

        $price = $price + ($tax_rate * $price) / 100;
    }
    return round($price, 2);
}

function deletable($id)
{
    $delete = Config::get('custom.deleteable') ?? [];
    if (in_array($id, @$delete)) {
        return false;
    } else {
        return true;
    }
}

//word limit
function limitTextWords($content = false, $limit = false, $stripTags = false, $ellipsis = false)
{
    if ($content && $limit) {
        $content = ($stripTags ? strip_tags($content) : $content);
        $content = explode(' ', trim($content), $limit + 1);
        array_pop($content);
        if ($ellipsis) {
            array_push($content, '...');
        }
        $content = implode(' ', $content);
    }
    return $content;
}


//german price to normal
function normalPrice($price)
{
    if (!empty($price)) {
        return str_replace(',', '.', $price) ?? $price;
    }
    return $price;
}


function header_cate_status()
{
    $header = $GLOBALS['custom_header'];
    $sidebar = $GLOBALS['custom_sidebar'];
    $shop_cat = $GLOBALS['shop_data'][0]['id'];
    $blog_cat = $GLOBALS['blog_data'][0]['id'];
    $active_head_cat = intval($GLOBALS['custom_active_category']);
    if (!@$header and !@$sidebar) {
        //        Config::set('custom.sidebar', 'sidebar');
        save_option('custom_sidebar', 'sidebar', 'category_customization');

        //        if(Config::get('custom.active_category') == null){
        if ($GLOBALS['custom_active_category'] == "null") {
            //            Config::set('custom.header', null);
            save_option('custom_header', 'null', 'category_customization');
        }
        //        Config::save(array('custom'));
    }
    //    dd($active_head_cat,$shop_cat,$blog_cat);
    if (empty($active_head_cat) && !isset($active_head_cat)) {
        template_head("<script>
        $(window).on('load',function (){
            $('#hide_shop').attr('style','display: none !important;');
            $('#hide_blog').attr('style','display: none !important;');
        });

        		</script>");
    }

    if (isset($_REQUEST['keywords'])) {

        template_head("<script>
        $(window).on('load',function (){
            $('#hide_shop').attr('style','display: none !important;');
            $('#hide_blog').attr('style','display: none !important;');
        });
        		</script>");
    }

    template_head("<script>
    $(window).on('load',function (){
        var shop_id = $('#shop_" . PAGE_ID . "').val();
        var blog_id = $('#blog_" . PAGE_ID . "').val();
            if(" . $shop_cat . " == '" . $active_head_cat . "' && typeof shop_id != 'undefined'){
                $('.header-cat').attr('style','display: block;');
            }
            if(typeof blog_id != 'undefined' && " . $blog_cat . " == '" . $active_head_cat . "'){
                $('.header-cat').attr('style','display: block;');
            }
            if(" . $shop_cat . " == '" . $active_head_cat . "'){
                $('#hide_blog').attr('style','display: none !important;');
            }
            if(" . $blog_cat . " == '" . $active_head_cat . "'){
                $('#hide_shop').attr('style','display: none !important;');
            }
            if('" . $active_head_cat . "' == null){
                $('#hide_shop').attr('style','display: none !important;');
                $('#hide_blog').attr('style','display: none !important;');

            }
        if(typeof shop_id != 'undefined' && " . $shop_cat . " == '" . $active_head_cat . "'){
            $.post('" . url('/') . "/api/v1/save_page_for_cat_header', { shop_id: shop_id }, (res) => {

            });

        }
        if(typeof blog_id != 'undefined' && " . $blog_cat . " == '" . $active_head_cat . "'){
            $.post('" . url('/') . "/api/v1/save_page_for_cat_header', { blog_id: blog_id }, (res) => {

            });
        }

        if(typeof shop_id == 'undefined'  && " . $shop_cat . " == '" . $active_head_cat . "'){
            $.post('" . url('/') . "/api/v1/clear_page_for_cat_header', { shop_id: " . PAGE_ID . " }, (res) => {

            });
        }
        if(typeof blog_id == 'undefined' && " . $blog_cat . " == '" . $active_head_cat . "'){
            $.post('" . url('/') . "/api/v1/clear_page_for_cat_header', { blog_id: " . PAGE_ID . " }, (res) => {

            });
        }

	});
		</script>");
    //    if(typeof id == 'undefined'){
    //        $.post('". url('/') ."/api/v1/clear_page_for_cat_header', { page_id: ".PAGE_ID." }, (res) => {
    //
    //        });
    //        }


    return category_hide_or_show();
}

function category_hide_or_show()
{
    $header = $GLOBALS['custom_header'];
    $sidebar = $GLOBALS['custom_sidebar'];
    $shop_cat = $GLOBALS['shop_data'][0]['id'];
    $blog_cat = $GLOBALS['blog_data'][0]['id'];
    $active_head_cat = intval($GLOBALS['custom_active_category']);
    if ($shop_cat == $active_head_cat) {
        $showHeader['blog'] = "hide";
    }
    if ($blog_cat == $active_head_cat) {
        $showHeader['shop'] = "hide";
    }
    if (!isset($active_head_cat) || $active_head_cat == null) {
        $showHeader['shop'] = "hide";
        $showHeader['blog'] = "hide";
    }
    //    dd(json_decode($GLOBALS['custom_blog_category_header']));
    if ($shop_cat == $active_head_cat) {
        $cat_array = (array)json_decode($GLOBALS['custom_shop_category_header']) ?? [];
        $cat_array_ignore = (array)json_decode($GLOBALS['custom_shop_category_header_ignore']) ?? [];
    } elseif ($blog_cat == $active_head_cat) {
        $cat_array = (array)json_decode($GLOBALS['custom_blog_category_header']) ?? [];
        $cat_array_ignore = (array)json_decode($GLOBALS['custom_blog_category_header_ignore']) ?? [];
    } else {
        $cat_array = [];
        $cat_array_ignore = [];
    }
    //    dd($sidebar,$header,$cat_array);
    if (@$header == 'header' && in_array(PAGE_ID, @$cat_array) == true && in_array(PAGE_ID, @$cat_array_ignore) != true) {
        $showHeader = showCat('header');
    } else if (@$sidebar == 'sidebar') {
        $showHeader = showCat('sidebar');
    } else {
        save_option('custom_sidebar', 'sidebar', 'category_customization');
        //        Config::set('custom.sidebar', 'sidebar');
        //        Config::save(array('custom'));
    }
    //    if(Config::get('custom.active_category') == "null"){
    if ($GLOBALS['custom_active_category'] == "null") {
        $showHeader['header'] = "hide";
    }
    if ((in_array(PAGE_ID, $cat_array) != true && in_array(PAGE_ID, $cat_array_ignore) != true) || $GLOBALS['custom_active_category'] == 'null') {
        $showHeader['button'] = 'hide';
    }
    return $showHeader;
}

function temp_blog_collapse()
{
    $header = $GLOBALS['custom_header'];
    $sidebar = $GLOBALS['custom_sidebar'];
    //$active = Config::get('custom.active_category');
    $active = $GLOBALS['custom_active_category'];

    if (@$header == 'header') {
        $showHeader = showCat('header');
    } else if (@$sidebar == 'sidebar') {
        $showHeader = showCat('sidebar');
    }

    if ($active == get_content('layout_file=layouts__blog.php')[0]['id']) {
        $showHeader['sidebar'] = "hide";
    } else {
        $showHeader['sidebar'] = "show";
    }

    if ($active == "null") {
        $showHeader['header'] = "hide";
    }
    template_head("<script>
    var shop = $('#all_cat').val();
    var shop_temp = $('#blog_" . PAGE_ID . "').val();
    if(shop == " . PAGE_ID . " && shop_temp == " . PAGE_ID . "){
        $('.header-cat').attr('style','display: block !important;');
    }
		</script>");
    return $showHeader;
}

function temp_shop_collapse()
{
    $header = $GLOBALS['custom_header'];
    $sidebar = $GLOBALS['custom_sidebar'];
    $active = $GLOBALS['custom_active_category'];

    if (@$header == 'header') {
        $showHeader = showCat('header');
    } else if (@$sidebar == 'sidebar') {
        $showHeader = showCat('sidebar');
    }

    if ($active == $GLOBALS['shop_data'][0]['id']) {
        $showHeader['sidebar'] = "hide";
    } else {

        $showHeader['sidebar'] = "show";
    }

    if ($active == "null") {
        $showHeader['header'] = "hide";
    }
    template_head("<script>
    var shop = $('#shop_cat').val();
    var shop_temp = $('#shop_" . PAGE_ID . "').val();
    if(shop == " . PAGE_ID . " && shop_temp == " . PAGE_ID . "){
        $('.header-cat').attr('style','display: block !important;');
    }
		</script>");
    return $showHeader;
}

function hide_delete()
{
    return array(
        "thank-you", "search", "agb", "impressum", "datenschutz", "versandkosten", "widerrufsrecht", "imprint", "privacy", "terms-and-conditions-first", "checkout", "shop", "blog", "home"
    );
}


function delivery_bill_url($order_id)
{
    $userToken = Config::get('microweber.userToken');
    $token = "token=" . $userToken . "&delivery=" . $order_id;
    $ciphering = "AES-128-CTR"; //Cipher method
    $iv_length = openssl_cipher_iv_length($ciphering); // Use OpenSSl Encryption method
    $options = 0;
    $encryption_iv = '1234567891011122'; // Non-NULL Initialization Vector for encryption
    $encryption_key = "trr45fdd4fgy34"; //encryption key

    $key = openssl_encrypt($token, $ciphering, $encryption_key, $options, $encryption_iv);
    $key = base64_encode($key);
    return "https://drm.software/droptienda-delivery-note?token=" . $key;
}


function licence_drm()
{
    try {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://eu-dropshipping.com/api/v1/dt-license-check',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_HTTPHEADER => array(
                'userToken: ' . Config::get('microweber.userToken'),
                'userPassToken: ' . Config::get('microweber.userPassToken'),
                'Cookie: SameSite=None'
            ),
        ));
        $response = json_decode(curl_exec($curl));
    } catch (Exception $e) {
        return false;
    }
    if (isset($response->success) && $response->success != false) {
        $string_js = "<script>
                      window.onUsersnapCXLoad = function(api) {
                        api.init();
                      }
                      var script = document.createElement('script');
                      script.async = 1;
                      script.src = 'https://widget.usersnap.com/load/9c33bd00-3564-480d-b89f-b104f6f0fb90?onload=onUsersnapCXLoad';
                      document.getElementsByTagName('head')[0].appendChild(script);
                    </script>";
        return $string_js;
    }
}


function rss_data_get($data)
{
    $mul_rss_link = get_option('rss_link', 'rss_data');
    $mul_rss_link = explode(",", $mul_rss_link);

    $all_data['rssLink'] = $rssLink = $mul_rss_link ?? null;

    $all_data['rssoption'] = $rssoption = get_option('rss_option', 'rss_data') ?? null;
    if (intval($rssoption) == 1 || intval($rssoption) == 2) {
        $rssItem = rss_data($rssLink, $rssoption);
        if (@$rssItem) {
            $data = rss_post_pagination($rssItem);
        }
    } else {
        $data = $data;
    }
    $all_data['data'] = $data;
    return $all_data;
}

function rss_url_image_link($item, $rssoption)
{
    if ($rssoption == 1 || $rssoption == 2) {
        $item['link'] = $item['url'] ?? site_url() . $item['slug'] . '/rss';
    }
    $item['image'] = $item['image'] ?? $item['media'][0]['filename'];

    return $item;
}

function rss_pagination($rssoption, $paging_param)
{

    $perpage_item  = intval(get_option('rss_page', 'rss_page')) ?? 6;
    $perpage_item = $perpage_item == 0 ? 6 : $perpage_item;
    if ($perpage_item and $perpage_item != 0 and $rssoption != null and $rssoption != 0) :
        if (function_exists('total_page')) {
            $page = total_page();
        }
        if ($page != null && $page > 1) :
            $aaa = "<module type='pagination' template='bootstrap4' pages_count='" . $page . "' paging_param='" . $paging_param . "' />";
            echo $aaa;
        endif;
    endif;
}

if (!function_exists('rss_data')) {
    function rss_data($rssLink, $rssoption)
    {
        $url = $rssLink;

        $rss_link_data = [];
        foreach ($url as $link) {
            try {
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $link);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
                $xmlstr = curl_exec($ch);
                curl_close($ch);
                $xml = simplexml_load_string($xmlstr);
                $json = json_encode($xml);
                $rssArray = json_decode($json, TRUE);
                $rssItem = $rssArray['channel']['item'] ?? null;
                if (isset($rssItem)) {
                    foreach ($rssItem as $ite) {
                        $temp = [];
                        $temp = $ite;
                        array_push($rss_link_data, $temp);
                    }
                }
            } catch (Exception $e) {
            }
        }


        $rssItem = $rss_link_data ?? null;


        if ($rssoption == 2) {
            return $rssItem;
        } elseif ($rssoption == 1) {
            $blogdata = get_posts("is_active=1", true);
            if ($rssItem != null) {
                $rssItem = array_merge($blogdata, $rssItem);
                return $rssItem;
            } else {
                return $rssItem;
            }
        }
    }
}

if (!function_exists('rss_post_pagination')) {
    function rss_post_pagination($array)
    {
        $perpage_item  = intval(get_option('rss_page', 'rss_page')) ?? 6;
        $perpage_item = $perpage_item == 0 ? 6 : $perpage_item;
        if (isset($perpage_item)) {
            $url_full = explode('=', url()->full());
            if (isset($url_full[1]) && count($url_full) <= 2) {
                $current_page = $url_full[1];
            } else {
                $current_page = 1;
            }
            $total = count($array);
            $pagination = $total / $perpage_item;
            $page = is_int($pagination) ? $pagination : (((int)$pagination) + 1);
            $GLOBALS['pages'] = $page;
            $start = ($current_page * $perpage_item) - $perpage_item;
            $end = $current_page * $perpage_item;
            $end = ($end <= $total) ? $end : $total;
            $counter = 0;
            $item = [];
            for ($i = $start; $i < $end; $i++) {
                $item[$counter] = $array[$i];
                $counter++;
            }
            return $item;
        } else {
            return $array;
        }
    }
}

if (!function_exists('total_page')) {
    function total_page()
    {
        $perpage_item  = intval(get_option('rss_page', 'rss_page'));
        if ($perpage_item) {
            if (isset($GLOBALS['pages'])) {
                return $GLOBALS['pages'];
            } else {
                return 0;
            }
        }
    }
}

function rss_single()
{
    $rssLink = get_option('rss_link', 'rss_data') ?? null;
    $mul_rss_link = explode(",", $rssLink);
    $rssoption = get_option('rss_option', 'rss_data') ?? null;

    $rss_link_data = [];
    foreach ($mul_rss_link as $link) {
        try {

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $link);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

            $xmlstr = curl_exec($ch);
            curl_close($ch);

            $xml = simplexml_load_string($xmlstr);

            $json = json_encode($xml);

            $rssArray = json_decode($json, TRUE);

            $rssItem = $rssArray['channel']['item'] ?? null;
            if (isset($rssItem)) {
                foreach ($rssItem as $ite) {

                    $temp = [];
                    $temp = $ite;

                    array_push($rss_link_data, $temp);
                }
            }
        } catch (Exception $e) {
        }
    }


    $rssItem = $rss_link_data ?? null;
    return $rssItem;
}

if (!function_exists('basicGoogleAnalytical')) {
    function basicGoogleAnalytical()
    {
        $ads_id = get_option('google-ads-id', 'website');
        $tags_id = get_option('google-tag-manager-id', 'website');
        if ($ads_id) {
            template_head("<script async src='https://www.googletagmanager.com/gtag/js?id=AW-" . $ads_id . "'></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'AW-" . $ads_id . "');
        </script>");
        }
        if ($tags_id) {
            template_head("<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id=" . $tags_id . "'+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','" . $tags_id . "');</script>");
        }
    }
}


function chat_footer()
{
    $all_orders = get_orders('no_limit=true');
    $chat_modal = '';
    if (isset($all_orders) && $all_orders != false) {
        foreach (@$all_orders as $order) {
            $chat_modal .= '<div class="modal chat-modal ordreChat_Modal" id="order-chat-modal-' . $order["id"] . '" data-order-id="' . $order["id"] . '" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="currentOrderId">Order Id: #<span>00</span></h4>
                        <input type="hidden" name="order_id" id="order_id" value="0">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">

                        <!-- Dt Order Chat Box Style End -->

                        <input class="currentOrderNo" type="hidden" value="" />

                        <div class="order-chat-box-wrapper">

                            <div class="chat-popup" id="chat-popup-' . $order["id"] . '">

                                <div class="chat-area" id="chat-area' . $order["id"] . '">
                                </div>
                                <div class="input-area">
                                    <input type="text" class="ordermsg emojionearea1" placeholder="Type Massage ..." id="emojionearea' . $order["id"] . '">
                                    <button class="submitOrderText btn btn-success" data-id="' . $order["id"] . '" id="submitOrderText' . $order["id"] . '"> Send</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                </div>
            </div>
        </div>';
        }
    }
    $chat_modal .= '<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/emojionearea/3.4.2/emojionearea.min.css" integrity="sha512-vEia6TQGr3FqC6h55/NdU3QSM5XR6HSl5fW71QTKrgeER98LIMGwymBVM867C1XHIkYD9nMTfWK2A0xcodKHNA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <style>

        h4.currentOrderId {
            display: inline-block;
            font-size: 18px;
            margin: 0;
            line-height: 1;
            font-weight: 400;
            text-transform: capitalize;
            letter-spacing: 0;
            color: #fff;
        }
        .ordreChat_Modal .modal-header {
            background: #00a65a;
            padding: 10px;
            color: #fff;
        }
        .emojiPicker nav {
            display: none !important;
        }
        .ordreChat_Modal  .modal-header {
            border-bottom: 3px solid #00a65a;
            padding: 10px;
        }
        .Sentchat_Massage .other-message {
            background: red !important;
        }
        .Sentchat_Massage .message.my-message.float-right,
        .getChat_massage .message.my-message.float-right {
            background: #00a65a;
        }
        .ordreChat_Modal .modal-content {
            border: none !important;
        }

        .ordreChat_Modal .modal-header button>span {
            color: #fff !important;
        }
        .order-new-info {
            display: flex;
            align-items: center;
            justify-content: space-between;
            margin-top: 10px;
        }

        .order-chat-option {
            text-align: right;
            margin-bottom: 10px;
        }

        .order-chat-option span {
            font-size: 30px;
            display:inline-block;
        }
        .order-chat-modal{
            z-index: 999999;
        }
        .AKSASGH,
        .AKSASGH .modal.my-orders-modal{
            overflow-y: hidden;
            overflow: hidden !important;
        }

        .my-orders-modal .modal-body{
            max-height: 90vh;
            overflow-y: auto;
        }
        .productOrderTable tr td{
            vertical-align: middle;
        }
        .re-order-btn button{
            width: 83px;
        }


        @media only screen and (max-width: 991px) {
            .my-orders-modal .modal-body {
                padding: 20px ;
            }
            .order-chat-option {
                text-align: center;
                margin-bottom: 0;
            }
            .my-order-status.pull-right {
                float: unset !important;
                text-align: center;
                margin-bottom: 10px;
            }
            .my-orders-modal .modal-body p {
                text-align: center;
            }
            .order-new-info {
                display: block !important;
            }
            .edit[field=order_modal_top] .text>p {
                margin-bottom: 15px;
            }
        }
        @media only screen and (max-width: 991px) and (min-width: 768px) {
            #ordersModal .modal-dialog {
                max-width: 620px;
            }
        }
    </style>
    <style>





        /* ========================\
        Order Chat Style Start
        ======================== */

        .order-chat-box-wrapper {
            position: relative;
            min-height: 300px;
        }

        .order-chat-box-wrapper .chat-btn{
            position: absolute;
            right:50px;
            bottom: 50px;
            background: dodgerblue;
            color: white;
            width:60px;
            height: 60px;
            border-radius: 50%;
            opacity: 0.8;
            transition: opacity 0.3s;
            box-shadow: 0 5px 5px rgba(0,0,0,0.4);
        }

        .order-chat-box-wrapper .chat-btn:hover, .submit:hover, #emoji-btn:hover{
            opacity: 1;
        }
        /*
        .order-chat-box-wrapper .chat-popup{
        display: none;
        position: absolute;
        bottom:80px;
        right:120px;
        height: 400px;
        width: 300px;
        background-color: white;
        / display: flex; /
        flex-direction: column;
        justify-content: space-between;
        padding: 0.75rem;
        box-shadow: 5px 5px 5px rgba(0,0,0,0.4);
        border-radius: 10px;
        } */
        .order-chat-box-wrapper .chat-popup{
            display: none;
            position: absolute;
            top: 0;
            right: 0;
            height: 100%;
            width: 100%;
            background-color: white;
            justify-content: space-between;
            padding: 0.75rem;
            box-shadow: 5px 5px 5px rgb(0 0 0 / 40%);
            border-radius: 10px;
        }

        .order-chat-box-wrapper .show{
            display: flex;
        }

        .order-chat-box-wrapper .show {
            display: block !important;
        }

        .order-chat-box-wrapper .chat-area{
            height: 82%;
            overflow-y: auto;
            overflow-x: hidden;
            padding-left: 10px;
            padding-right: 10px;
        }
        .order-chat-box-wrapper .chat-area li:nth-child(1) {
            margin-top: 10px;
        }

        .order-chat-box-wrapper .income-msg{
            display: flex;
            align-items: center;
        }

        .order-chat-box-wrapper .avatar{
            width:45px;
            height: 45px;
            border-radius: 50%;
            object-fit: cover;
        }

        .order-chat-box-wrapper .income-msg .msg{
            background-color: dodgerblue;
            color: white;
            padding:0.5rem;
            border-radius: 25px;
            margin-left: 1rem;
            box-shadow: 0 2px 5px rgba(0,0,0,0.4);
        }

        .order-chat-box-wrapper .badge{
            position: absolute;
            width: 30px;
            height: 30px;
            background-color: red;
            color:white;
            border-radius: 50%;
            display: flex;
            justify-content: center;
            align-items: center;
            top:-10px;
            right: -10px;
        }
        .input-area .emojiPickerIconWrap input {
            width: 100% !important;
            height: 34px !important;
            border-color: #d2d6de !important;
            padding: 6px 37px 6px 12px;
            border-top-left-radius: 0 !important;
            border-bottom-left-radius: 0 !important;
        }
        .input-area .emojiPickerIconWrap input:focus {
            outline: 0;
            border: 1px solid #3c8dbc !important
        }

        .order-chat-box-wrapper .input-area{
            position: relative;
            display: flex;
            justify-content: center;
            overflow: hidden;
            border-top: 1px solid #eee;
            padding: 10px;
        }
        .emojiPickerIcon.black {
            height: 34px !important;
        }

        .order-chat-box-wrapper input[type="text"]{
            width:100%;
            border: 1px solid #ccc;
            font-size: 1rem;
            border-radius: 5px;
            height: 2.2rem;
        }

        .order-chat-box-wrapper #emoji-btn{
            position: absolute;
            font-size: 1.2rem;
            background: transparent;
            right: 50px;
            top:2px;
            opacity:0.5;
        }

        .order-chat-box-wrapper .submit{
            padding: 0.25rem 0.5rem;
            margin-left: 0.5rem;
            background-color: green;
            color:white;
            display: flex;
            justify-content: center;
            align-items: center;
            border-radius: 5px;
            opacity: 0.7;
        }


        .order-chat-box-wrapper .out-msg{
            display: flex;
            justify-content: flex-end;
            align-items: center;
        }
        .order-chat-box-wrapper .my-msg{
            display: flex;
            justify-content: flex-end;
            margin: 0.75rem;
            padding: 0.5rem;
            background-color: #ddd;
            border-radius: 25px;
            box-shadow: 0 2px 5px rgba(0,0,0,0.4);
            word-break: break-all;
        }





        .order-chat-box-wrapper .chat-popup.show {
            box-shadow: none;
            border: 1px solid #dee2e6;
            border-radius: 0px;
            border-top: 0px;
            border-bottom: 0px;
            padding-bottom: 0px;
            padding: 0px !important;
        }
        button.submitOrderText i {
            font-size: 18px;
        }
        // button.submitOrderText {
        //     background-color: #000;
        //     color: #fff;
        //     padding: 7px 5px 0px !important;
        //     border: none;
        // }
        button#submitOrderText1 {
            border-radius: 0 !important;
        }
        .emojionearea, .emojionearea.form-control, .order-chat-box-wrapper input[type="text"] {
            border-radius: 0px !important;
        }


        .order-chat-box-wrapper .badge {
            right: 40px;
            top: -40px;
        }
        .chat-modal .modal-body {
            padding: 0px;
        }

        @media (max-width:500px){

            .order-chat-box-wrapper .chat-popup{
                bottom: 120px;
                right:10%;
                width: 80vw;
            }
        }


        .order-outgoing-msg-info {
            position: relative;
        }

        .order-outgoing-msg-info span.message-data-time {
            font-size: 10px;
            line-height: 12px;
            color: #888;
            display: block;
        }

        .order-outgoing-msg-info span.email {
            font-size: 10px;
            color: #9e9e9e;
            position: relative;
            margin-top: -22px;
            display: block;
        }

        .order-outgoing-msg-info span.message-data-time br {}
        .message.other-message{
            position: relative;
            display: inline-block;
            background-color: #3390ff;
            width: auto;
            border-radius: 5px;
            text-align: left;
            padding: 5px 10px;
            color: #fff;
            font-size: 14px;
            line-height: 19px;
            margin-bottom: 10px;
        }
        .message.my-message{
            position: relative;
            display: inline-block;
            background-color: #00a65a;
            width: auto;
            border-radius: 5px;
            text-align: left;
            padding: 5px 10px;
            color: #fff;
            font-size: 14px;
            line-height: 19px;
            margin-bottom: 10px;
        }
        span#my-order-ChatOption {
            cursor: pointer;
        }
        ul, li {
            list-style: none;
        }
        .modal-header .close:focus {
            outline: 0;
        }
        .order-chat-modal span.badge {
            position: absolute;
            background-color: #f00;
            color: #fff;
            font-size: 10px;
            z-index: 1;
            border-radius: 50%;
            padding: 5px;
            right: -4px;
            top: 5px;
        }

        .order-chat-modal {
            position: relative;
        }
        /* ========================
        Order Chat Style End
        ======================== */
    </style>';
    return $chat_modal;
}



//word count
function random_word_check($data)
{
    if ($data) {
        $temp_all_word = explode(' ', $data['text']);
        $replc_all_word = str_replace(".", "", $data['text']);
        $replc_all_word = str_replace(",", "", $replc_all_word);
        $finl_all_word = explode(' ', $replc_all_word);
        $finl_all_word = array_filter($finl_all_word);

        $word_count = 1;
        $sub_string = null;
        if (@$data['single_word'] && @$data['word_number']) {
            $checkword = substr_count($replc_all_word, $data['single_word']);
            if ($checkword >= $data['word_number']) {
                foreach ($finl_all_word as $key => $single_word) {
                    $sub_string = $sub_string . ' ' . $temp_all_word[$key];
                    if ($finl_all_word[$key] == $data['single_word']) {
                        if ($word_count == $data['word_number']) {
                            break;
                        } else {
                            $word_count++;
                        }
                    }
                }

                $sub_string = force_balance_tags($sub_string);
            } else {
                $sub_string = "String not found";
            }
        } else {
            $sub_string = "String not found";
        }

        return $sub_string;
    }
}

function last_word_check($data)
{

    if ($data) {
        $temp_all_word = explode(' ', $data);
        $replc_all_word = str_replace(".", "", $data);
        $replc_all_word = str_replace(",", "", $replc_all_word);
        $finl_all_word = explode(' ', $replc_all_word);
        $finl_all_word = array_filter($finl_all_word);
        $checkword['word_number'] = substr_count($replc_all_word, end($finl_all_word));
        $checkword['single_word'] =  end($finl_all_word);
        return $checkword;
    }
}

function force_balance_tags($text)
{
    $tagstack  = array();
    $stacksize = 0;
    $tagqueue  = '';
    $newtext   = '';
    // Known single-entity/self-closing tags.
    $single_tags = array('area', 'base', 'basefont', 'br', 'col', 'command', 'embed', 'frame', 'hr', 'img', 'input', 'isindex', 'link', 'meta', 'param', 'source');
    // Tags that can be immediately nested within themselves.
    $nestable_tags = array('blockquote', 'div', 'object', 'q', 'span');

    // WP bug fix for comments - in case you REALLY meant to type '< !--'.
    $text = str_replace('< !--', '<    !--', $text);
    // WP bug fix for LOVE <3 (and other situations with '<' before a number).
    $text = preg_replace('#<([0-9]{1})#', '&lt;$1', $text);

    /**
     * Matches supported tags.
     *
     * To get the pattern as a string without the comments paste into a PHP
     * REPL like `php -a`.
     *
     * @see https://html.spec.whatwg.org/#elements-2
     * @see https://w3c.github.io/webcomponents/spec/custom/#valid-custom-element-name
     *
     * @example
     * ~# php -a
     * php > $s = [paste copied contents of expression below including parentheses];
     * php > echo $s;
     */
    $tag_pattern = ('#<' . // Start with an opening bracket.
        '(/?)' . // Group 1 - If it's a closing tag it'll have a leading slash.
        '(' . // Group 2 - Tag name.
        // Custom element tags have more lenient rules than HTML tag names.
        '(?:[a-z](?:[a-z0-9._]*)-(?:[a-z0-9._-]+)+)' .
        '|' .
        // Traditional tag rules approximate HTML tag names.
        '(?:[\w:]+)' .
        ')' .
        '(?:' .
        // We either immediately close the tag with its '>' and have nothing here.
        '\s*' .
        '(/?)' . // Group 3 - "attributes" for empty tag.
        '|' .
        // Or we must start with space characters to separate the tag name from the attributes (or whitespace).
        '(\s+)' . // Group 4 - Pre-attribute whitespace.
        '([^>]*)' . // Group 5 - Attributes.
        ')' .
        '>#' // End with a closing bracket.
    );

    while (preg_match($tag_pattern, $text, $regex)) {
        $full_match        = $regex[0];
        $has_leading_slash = !empty($regex[1]);
        $tag_name          = $regex[2];
        $tag               = strtolower($tag_name);
        $is_single_tag     = in_array($tag, $single_tags, true);
        $pre_attribute_ws  = isset($regex[4]) ? $regex[4] : '';
        $attributes        = trim(isset($regex[5]) ? $regex[5] : $regex[3]);
        $has_self_closer   = '/' === substr($attributes, -1);

        $newtext .= $tagqueue;

        $i = strpos($text, $full_match);
        $l = strlen($full_match);

        // Clear the shifter.
        $tagqueue = '';
        if ($has_leading_slash) { // End tag.
            // If too many closing tags.
            if ($stacksize <= 0) {
                $tag = '';
                // Or close to be safe $tag = '/' . $tag.

                // If stacktop value = tag close value, then pop.
            } elseif ($tagstack[$stacksize - 1] === $tag) { // Found closing tag.
                $tag = '</' . $tag . '>'; // Close tag.
                array_pop($tagstack);
                $stacksize--;
            } else { // Closing tag not at top, search for it.
                for ($j = $stacksize - 1; $j >= 0; $j--) {
                    if ($tagstack[$j] === $tag) {
                        // Add tag to tagqueue.
                        for ($k = $stacksize - 1; $k >= $j; $k--) {
                            $tagqueue .= '</' . array_pop($tagstack) . '>';
                            $stacksize--;
                        }
                        break;
                    }
                }
                $tag = '';
            }
        } else { // Begin tag.
            if ($has_self_closer) { // If it presents itself as a self-closing tag...
                // ...but it isn't a known single-entity self-closing tag, then don't let it be treated as such
                // and immediately close it with a closing tag (the tag will encapsulate no text as a result).
                if (!$is_single_tag) {
                    $attributes = trim(substr($attributes, 0, -1)) . "></$tag";
                }
            } elseif ($is_single_tag) { // Else if it's a known single-entity tag but it doesn't close itself, do so.
                $pre_attribute_ws = ' ';
                $attributes      .= '/';
            } else { // It's not a single-entity tag.
                // If the top of the stack is the same as the tag we want to push, close previous tag.
                if ($stacksize > 0 && !in_array($tag, $nestable_tags, true) && $tagstack[$stacksize - 1] === $tag) {
                    $tagqueue = '</' . array_pop($tagstack) . '>';
                    $stacksize--;
                }
                $stacksize = array_push($tagstack, $tag);
            }

            // Attributes.
            if ($has_self_closer && $is_single_tag) {
                // We need some space - avoid <br/> and prefer <br />.
                $pre_attribute_ws = ' ';
            }

            $tag = '<' . $tag . $pre_attribute_ws . $attributes . '>';
            // If already queuing a close tag, then put this tag on too.
            if (!empty($tagqueue)) {
                $tagqueue .= $tag;
                $tag       = '';
            }
        }
        $newtext .= substr($text, 0, $i) . $tag;
        $text     = substr($text, $i + $l);
    }

    // Clear tag queue.
    $newtext .= $tagqueue;

    // Add remaining text.
    $newtext .= $text;

    while ($x = array_pop($tagstack)) {
        $newtext .= '</' . $x . '>'; // Add remaining tags to close.
    }

    // WP fix for the bug with HTML comments.
    $newtext = str_replace('< !--', '<!--', $newtext);
    $newtext = str_replace('<    !--', '< !--', $newtext);

    return $newtext;
}

function show_email_when_stockout($product_id, $product_url, $product_title, $ean)
{
    return '<style>
                /* Email Notification Css Start*/

                nav.navbar.navbar-light.bg-light {
                    padding: 0px 11px;
                    margin-bottom: 20px;
                }
                .nav-text i {
                    font-size: 20px;
                    color: #fff;
                    background-color: dodgerblue;
                    padding: 22px 18px;
                    margin-left: -11px;
                    border-radius: 5px;
                }
                .nav-text a {
                    font-size: 15px;
                    margin-left: 13px;
                }

                .shop-modules.text h2 {
                    font-size: 14px;
                    margin-top: 20px;
                    margin-bottom: 35px;
                }
                .input-container {
                    display: flex;
                    width: 100%;
                    margin-bottom: 15px;
                    margin-top: 12px;
                }

                .input-container button {
                    background: dodgerblue !important;
                    color: #fff !important;
                    min-width: 81px !important;
                    text-align: center !important;
                }
                .input-container button:hover {
                    color: #fff;
                    background-color: #222222;

                }
                .email-field {
                    width: 100%;
                    padding: 10px;
                    outline: none;
                    background-color: #FAF9F6;
                    height: 48px;
                }

                /* Email Notification Css End*/
            </style>
            <div class="email_notification">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <nav class="navbar navbar-light bg-light">
                                    <div class="nav-text">
                                        <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                        <a href="">Benachrichtigen Sie mich, sobald der Artikel lieferbar ist.</a>
                                    </div>
                                </a>
                            </nav>
                            <div class="input-container">
                                <input class="email-field form-control" type="email" placeholder="Ihre E-Mail-Address" name="email" value="' . user_email() . '">
                                    <button class="btn mail-in-stockout" type="button" data-id="' . $product_id . '" data-ean="' . $ean . '" data-url="' . url('/') . "/" . $product_url . '" data-title="' . $product_title . '"><i class="fa fa-envelope icon"></i></button>

                            </div>
                            <div class="shop-modules text">
                                <h2>ich habe die <a href="' . url("/") . '/datenschutz">Datenschutzbestimmungen</a> zur Kenntnis genommen</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script>
            $(document).on("click", ".mail-in-stockout", function (){
                let product_id = $(this).data("id");
                let url = $(this).data("url");
                let title = $(this).data("title");
                let ean = $(this).data("ean");
                let email = $(this).parent(".input-container").find(".email-field").val();
                console.log(email);
                $.post("' . url("/") . '/api/v1/send_mail_in_stockout", { id:product_id,url: url,title:title,ean:ean,email:email
            })
            .then((res, err) => {
                if(res.success){
                    mw.notification.success(res.message);
                }else{
                    mw.notification.warning(res.message);

                }
            });
            });
</script>
';
}


//image resize
function resize_image($image, $thumbnail_width, $file_kb_size, $minimum_size)
{
    $main_image_path = $image;
    $path = explode("/", $main_image_path);
    $image_name = end($path);

    $explode_image_name = explode(".", $image_name);
    if (count($explode_image_name) > 2) {
        $remove = array_pop($explode_image_name);
        $explode_image_name = implode(".", $explode_image_name);
    }

    $img_resize['only_image_name'] = @array_shift($explode_image_name) ?? $explode_image_name;
    $img_resize['webp_save_path'] = public_path('userfiles/media/templates.microweber.com/' . $thumbnail_width . '/');
    $img_resize['image_name'] = $image_name;

    $savePath = public_path('userfiles/media/templates.microweber.com/' . $thumbnail_width . '/thumbnails' . '/') . $img_resize['only_image_name'] . '.webp';

    $folder = $savePath;
    $data = explode("/", $folder);
    $removed = array_pop($data);
    $dirname = implode("/", $data);

    if (!file_exists($dirname)) {
        mkdir($dirname, 0777, true);
    }

    $activate_compressor = get_option('img_compressor', 'compressor');
    if (@$activate_compressor == 1 && $activate_compressor != false) {
        if (!empty($minimum_size) && $minimum_size < $file_kb_size) {
            if (!file_exists($savePath)) {
                $img = \Intervention\Image\ImageManagerStatic::make($main_image_path);
                $img->resize($thumbnail_width, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->encode('webp');
                @$img->save($savePath);
            }
        }
    }

    return $img_resize;
}

function exixting_image_compressed($image, $compress_size, $thumbnail_width, $minimum_size, $file_kb_size)
{
    $main_image_path = $image;
    $path = explode("/", $main_image_path);
    $image_name = end($path);

    $explode_image_name = explode(".", $image_name);
    if (count($explode_image_name) > 2) {
        $remove = array_pop($explode_image_name);
        $explode_image_name = implode(".", $explode_image_name);
    }

    $img_resize['only_image_name'] = @array_shift($explode_image_name) ?? $explode_image_name;
    $img_resize['webp_save_path'] = public_path('userfiles/media/default/');
    $img_resize['image_name'] = $image_name;

    $savePath = public_path('userfiles/media/default/thumbnails/') . $img_resize['only_image_name'] . '.webp';
    $folder = $savePath;
    $data = explode("/", $folder);
    $removed = array_pop($data);
    $dirname = implode("/", $data);

    if (!file_exists($dirname)) {
        mkdir($dirname, 0777, true);
    }

    //existing image delete
    if (file_exists($savePath)) {
        unlink($savePath);
    }
    if (file_exists($img_resize['webp_save_path'] . $img_resize['only_image_name'] . '.webp')) {
        unlink($img_resize['webp_save_path'] . $img_resize['only_image_name'] . '.webp');
    }

    // Check if you have this file and
    $activate_compressor = get_option('img_compressor', 'compressor');
    if (@$activate_compressor == 1 && $activate_compressor != false) {
        if (!empty($minimum_size) && $minimum_size < $file_kb_size) {
            if (!file_exists($savePath)) {
                $img = \Intervention\Image\ImageManagerStatic::make($main_image_path);
                $img->resize($thumbnail_width, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->encode('webp');
                @$img->save($savePath);
            }
        }
    }
    //webp image save
    $webp_image = \Intervention\Image\ImageManagerStatic::make($main_image_path)->encode('webp', 90);
    @$webp_image->save($img_resize['webp_save_path'] . $img_resize['only_image_name'] . '.webp', $compress_size ? $compress_size : 100);

    return $img_resize;
}

function insert_resized_image($id, $image, $resized_image, $thumbnail_width, $file_kb_size, $minimum_size)
{
    //dd($id,$image,$resized_image,$thumbnail_width);
    $resize_image = '{SITE_URL}userfiles/media/templates.microweber.com/' . $thumbnail_width . '/thumbnails' . '/' . $resized_image['only_image_name'] . '.webp';
    $webp_image   = '{SITE_URL}userfiles/media/templates.microweber.com/' . $thumbnail_width . '/' . $resized_image['only_image_name'] . '.webp';
    $activate_compressor = get_option('img_compressor', 'compressor');

    if (@$activate_compressor == 1 && $activate_compressor != false) {
        if (!empty($minimum_size) && $minimum_size < $file_kb_size) {
            $insert = DB::table('media')->where('rel_id', $id)->where('filename', $image)->update([
                'resize_image' => $resize_image,
                'webp_image' => $webp_image,
            ]);
        }
    } else {
        $insert = DB::table('media')->where('rel_id', $id)->where('filename', $image)->update([
            'webp_image' => $webp_image,
        ]);
    }
}

function live_editor_image_compressed($image)
{
    // Start product image resize code
    $compress_size  = DB::table('image_optimize')->where('status', 5)->first();
    $minimum_size   = DB::table('image_optimize')->where('status', 6)->first();
    $main_image_path = $image;

    // get image size
    $ch = curl_init($main_image_path);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, TRUE);
    curl_setopt($ch, CURLOPT_NOBODY, TRUE);
    $msr = curl_exec($ch);
    $file_byte_size = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
    $file_kb_size = round($file_byte_size / 1024, 4);
    // end get image size

    $main_image_path = $image;
    $path = explode("/", $main_image_path);
    $image_name = end($path);
    $savePath = public_path('userfiles/media/default/') . $image_name;

    $folder = $savePath;
    $data = explode("/", $folder);
    $removed = array_pop($data);
    $dirname = implode("/", $data);

    if (!file_exists($dirname)) {
        mkdir($dirname, 0777, true);
    }

    // Start Webp code
    $explode_image_name = explode(".", $image_name);
    $only_image_name = array_shift($explode_image_name);
    $webp_save_path = public_path('userfiles/media/default/');

    $webp_image = \Intervention\Image\ImageManagerStatic::make($main_image_path)->encode('webp', 100);
    @$webp_image->save($webp_save_path . $only_image_name . '.webp', $compress_size ? $compress_size->live_edit_compress : 100);

    $resize_image = url('/') . '/userfiles/media/default/' . $only_image_name . '.webp';
    return  $resize_image;

    // end product image resize code
}


function remote_file_exists($url)
{
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_NOBODY, true);
    curl_exec($ch);
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    if ($httpCode == 200) {
        return true;
    }
}
