<?php
/**
 * Created by PhpStorm.
 * User: Bojidar
 * Date: 8/19/2020
 * Time: 4:09 PM
 */

namespace MicroweberPackages\Product\Http\Controllers\Api;

use App\Enums\SyncEvent;
use App\Enums\SyncType;
use App\Models\Category;
use App\Models\CategoryItem;
use App\Models\SyncHistory;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;
use MicroweberPackages\Product\Http\Requests\ProductRequest;
use MicroweberPackages\Product\Http\Requests\ProductCreateRequest;
use MicroweberPackages\Product\Http\Requests\ProductUpdateRequest;
use MicroweberPackages\Product\Repositories\ProductRepository;

class ProductApiController
{
    public $product;

    public function __construct(ProductRepository $product)
    {
        $this->product = $product;
    }


    /**
    /**
     * Display a listing of the product.
     *
     * @param ProductRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        return (new JsonResource(
            $this->product
                ->filter($request->all())
                ->paginate($request->get('limit', 30))
                ->appends($request->except('page'))

        ))->response();

    }

    /**
     * Store product in database
     *
     * @param ProductCreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ProductCreateRequest $request)
    {
        $cates = explode(',',$_REQUEST['category_ids']);
        $arr = [];
        foreach ($cates as $cat){
            if(!empty($cat)){
                $category = \MicroweberPackages\Category\Models\Category::where('id', $cat)->with('parent')->get()->toArray()[0];
                $arr[] = $category;
                for (; ;) {
                    if (is_array($category['parent'])) {
                        $arr[] = $category['parent'];
                        $category = $category['parent'];
                    } else {
                        break;
                    }
                }
            }
        }

        $result = $this->product->create($request->all());
        foreach ($arr as $ar) {

            DB::table('categories')->where('id' , $ar['id'])
                ->update(['is_hidden' => 0]);
            DB::table('categories_items')->updateOrInsert([
                'parent_id' => $ar['id'],
                'rel_type' => 'content',
                'rel_id' => $result->id,
            ]);
        }
//        if (env('SYNC_ENABLE') && $request->content_type == 'product') {
        if ($request->content_type == 'product') {
            SyncHistory::create([
                'sync_type' => SyncType::PRODUCT,
                'sync_event' => SyncEvent::CREATE,
                'model_id' => $result->id
            ]);
        }

        // product Brand,upadate,Delivery-time insert time update
        if ($request->content_type == 'product') {
            DB::table('content')->where('id',$result['id'])->update([
                'brand'        => $request['brand'],
                'delivery_days'=> $request['delivery_days'],
                'item_size'  => $request['item_size'],
                'item_weight'  => $request['item_weight'],
                'item_color'   => $request['item_color'],
                'materials'    => $request['materials'],
                'production_year'=> $request['production_year'],
            ]);
        }
        //product upselling add in new product
        if ($request->content_type == 'product') {
            $all_upselling_item = db_get('table=product_upselling');
            foreach($all_upselling_item as $upselling_item){
                if($request['upselling'.$upselling_item['id']]){
                    $select_item = array(
                        'product_id' => $result['id'], 
                        'item_id' => $upselling_item['id']
                    );
                    db_save('product_upselling_item', $select_item);
                }
            }
        }

        //thank you page add in new product
        if ($request->content_type == 'product') {
            for($x = 1; $x <= 6; $x++){
                if($request['theme'.$x]){
                    $select_item = array(
                        'template_name' => $x, 
                        'product_id' => $result['id']
                    );
                    db_save('thank_you_pages', $select_item);
                }
            }
        }

        //checkout bumbs add in new product
        if($request->content_type == 'product'){
            $select_bumbs = $request['checkBumbs'];
            if($request['bumbs'] == 'cartbumbs'){
                $cartbumbs = 1;
                $checkoutbumbs = 0;
            }else if($request['bumbs'] == 'checkoutbumbs'){
                $cartbumbs = 0;
                $checkoutbumbs = 1;
            }
            if($select_bumbs){
                if(db_get('table=checkout_bumbs&single=true')){
                    DB::table('checkout_bumbs')->update(['product_id' => $result['id'],'show_cart' => $cartbumbs,'show_checkout' => $checkoutbumbs]);
                }else{
                    db_save('checkout_bumbs',array('product_id' => $result['id'],'show_cart' => $cartbumbs, 'show_checkout' => $checkoutbumbs));
                }
            }else if($request['bumbs']){
                DB::table('checkout_bumbs')->update(['show_cart' => $cartbumbs,'show_checkout' => $checkoutbumbs]);
            }
        }

        cat_reset_logic();

        return (new JsonResource($result))->response();
    }

    /**
     * Display the specified resource.show
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $result = $this->product->show($id);

        return (new JsonResource($result))->response();
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  ProductRequest $request
     * @param  string $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ProductUpdateRequest $request, $product)
    {



        CategoryItem::where(['rel_type' => 'content', 'rel_id' => $_REQUEST['id']])->delete();

        $cates = explode(',',$_REQUEST['category_ids']);
        $arr = [];
        foreach ($cates as $cat){
            if(!empty($cat)){
                $category = \MicroweberPackages\Category\Models\Category::where('id', $cat)->with('parent')->get()->toArray()[0];
                $arr[] = $category;
                for (; ;) {
                    if (is_array($category['parent'])) {
                        $arr[] = $category['parent'];
                        $category = $category['parent'];
                    } else {
                        break;
                    }
                }
            }
        }
        $result = $this->product->update($request->all(), $product);
        foreach ($arr as $ar) {
            DB::table('categories')->where('id' , $ar['id'])
                ->update(['is_hidden' => 0]);
            DB::table('categories_items')->updateOrInsert([
                'parent_id' => $ar['id'],
                'rel_type' => 'content',
                'rel_id' => $product,
            ]);
        }
//        if (env('SYNC_ENABLE') && $request->content_type == 'product') {
        if ($request->content_type == 'product') {
            SyncHistory::create([
                'sync_type' => SyncType::PRODUCT,
                'sync_event' => SyncEvent::UPDATE,
                'model_id' => $product
            ]);
        }

        // product Brand,upadate,Delivery-time,weight update
        if ($request->content_type == 'product') {
            DB::table('variants')->where('rel_id',$request['id'])->update([
                'size'=> $request['size']
            ]);
            DB::table('content')->where('id',$request['id'])->update([
                'brand'        => $request['brand'],
                'delivery_days'=> $request['delivery_days'],
                'item_size'    => $request['item_size'],
                'item_weight'  => $request['item_weight'],
                'item_color'   => $request['item_color'],
                'materials'    => $request['materials'],
                'production_year'=> $request['production_year'],
            ]);
        }

        cat_reset_logic();

        return (new JsonResource($result))->response();
    }

    /**
     * Destroy resources by given ids.
     *
     * @param string $ids
     * @return void
     */
    public function delete($id)
    {
        $product = $this->product->show($id);
        $data = $this->product->delete($id);

//        if (env('SYNC_ENABLE') && $product->content_type = 'product') {
        if ($product->content_type = 'product') {
            SyncHistory::create([
                'sync_type' => SyncType::PRODUCT,
                'sync_event' => SyncEvent::DELETE,
                'model_id' => $product->id,
                'drm_ref_id'=> $product->drm_ref_id,
            ]);
        }

        return $data;
    }

    /**
     * Delete resources by given ids.
     *
     * @param string $ids
     * @return void
     */
    public function destroy($ids)
    {
        foreach ($ids as $id) {
            $product = $this->product->show($id);

//            if (env('SYNC_ENABLE') && $product->content_type = 'product') {
            if ($product->content_type = 'product') {
                SyncHistory::create([
                    'sync_type' => SyncType::PRODUCT,
                    'sync_event' => SyncEvent::DELETE,
                    'model_id' => $product->id,
                    'drm_ref_id'=> $product->drm_ref_id,
                ]);
            }
        }

        return $this->product->destroy($ids);
    }
}
