<?php

use App\Models\Content;
use App\Models\Order;
use App\Models\SyncHistory;
use App\Services\Customer\CustomerService;
use App\Services\Order\OrderService;
use App\Services\Product\ProductService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;
use App\Services\DrmSyncService;
use MicroweberPackages\Tax\TaxType;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
$is_installed = mw_is_installed();
if ($is_installed) {
    $GLOBALS = array(
        'all_tax' => TaxType::all(),
        'all_tax_rates' => DB::table('tax_rates')->get(),
        'user_country_tax' => DB::table('users')
            ->select('tax_rates.charge', 'users.country')
            ->join('tax_rates', 'tax_rates.country', '=', 'users.country')
            ->where('users.id', user_id())->first(),
        'shop_data' => get_content('is_shop=1') ?? get_content('layout_file=layouts__shop.php') ?? get_content('layout_file=index.php'),
        'blog_data' => get_content('url=blog') ?? get_content('layout_file=layouts__blog.php') ?? get_content('layout_file=index.php'),
        'custom_shop_category_header' => get_option('custom_shop_category_header','category_customization'),
        'custom_blog_category_header' => get_option('custom_blog_category_header','category_customization'),
        'custom_blog_category_header_ignore' => get_option('custom_blog_category_header_ignore','category_customization'),
        'custom_shop_category_header_ignore' => get_option('custom_shop_category_header_ignore','category_customization'),
        'custom_active_category' => get_option('custom_active_category','category_customization'),
        'custom_sidebar' => get_option('custom_sidebar','category_customization'),
        'custom_header' => get_option('custom_header','category_customization'),
    );

}

Route::apiResource('products', 'ProductController');
Route::apiResource('categories', 'CategoryController');
Route::apiResource('orders', 'OrderController');
Route::apiResource('customer', 'CustomerController');

Route::get('drm-guest-checkout/{id}', 'ProductController@guestCheckout');

Route::get('test-order-sync-to-drm', function () {

    $syncService = new DrmSyncService("http://165.22.24.129");
    $order = Order::with('carts')->with('carts.content:id,ean,drm_ref_id')->find(93)->toArray();

    if ($order) {
        $response = $syncService->storeOrder($order);
        dd($response);
    }
});


Route::get('sync-to-drm', function () {
    $syncHistories = SyncHistory::whereNull('synced_at')
        ->where('tries', '<', 3)
        ->limit(10)
        ->get();

    $synced = [];
    foreach($syncHistories as $syncHistory) {
        switch($syncHistory->sync_type) {
            case \App\Enums\SyncType::CATEGORY:
                $updatedData = app(\App\Services\Category\CategoryService::class)->syncCategoryToDrm($syncHistory);
                if (!empty($updatedData->synced_at)) {
                    $synced[] = [
                        'dt_ref_id' => $updatedData->id,
                        'synced_at' => $updatedData->synced_at,
                    ];
                }
                break;

            case \App\Enums\SyncType::CUSTOMER:
                $updatedData = app(CustomerService::class)->syncCustomerToDrm($syncHistory);
                if (!empty($updatedData->synced_at)) {
                    $synced[] = [
                        'dt_ref_id' => $updatedData->id,
                        'synced_at' => $updatedData->synced_at,
                    ];
                }
                break;

            case \App\Enums\SyncType::PRODUCT:
                $updatedData = app(ProductService::class)->syncProductToDrm($syncHistory);
                if (!empty($updatedData->synced_at)) {
                    $synced[] = [
                        'dt_ref_id' => $updatedData->id,
                        'synced_at' => $updatedData->synced_at,
                    ];
                }
                break;

            case \App\Enums\SyncType::ORDER:
                $updatedData = app(OrderService::class)->syncOrderToDrm($syncHistory);
                if (!empty($updatedData->synced_at)) {
                    $synced[] = [
                        'dt_ref_id' => $updatedData->id,
                        'synced_at' => $updatedData->synced_at,
                    ];
                }
                break;
        }
    }

    return response()->json(['success' => true, 'message' => 'Synced successfully.', 'data' => $synced]);
});

Route::get('product/{code}', function ($code){
    $product = \App\Models\Content::where('drm_ref_id', $code)->first();

    if(!$product) {
        return response()->json(['success' => false, 'message' => 'Product not found!'], 422);
    }
    $randomString = uniqid();

    $ins_array = array(
        'products_id' => $product->id,
        'user_id' => 1,
        'slug' => $randomString,
    );

    DB::table('quick_checkout')->insert($ins_array);
    $url = site_url() . 'checkout?slug=' . $randomString;

    return redirect('checkout?slug=' . $randomString);
});

Route::get('validate-droptienda-shop', function (Request $request) {
    $userToken = $request->input('userToken');
    $userPassToken = $request->input('userPassToken');

    if(!empty($userToken) && $userToken == config('microweber.userToken') && !empty($userPassToken) && $userPassToken == config('microweber.userPassToken')) {
        return response()->json(['success' => true, 'message' => 'Droptienda Shop verified successfully.'], 200);
    }

    return response()->json(['success' => false, 'message' => 'Droptienda Shop verification failed.'], 401);
});

Route::post('drm_token_get',function(){
    try {

        if(isset($_REQUEST['is_register'])){
            $register = $_REQUEST['is_register'];
            $payLoad = [
                'name' => $_REQUEST['name'],
                'email'=> $_REQUEST['installUserName'],
                'password'=> $_REQUEST['installUserPass'],
                'url' => $_REQUEST['url'],
                'is_register' => $register,

            ];
        }else{

            $payLoad = [
                'email'=> $_REQUEST['installUserName'],
                'password'=> $_REQUEST['installUserPass'],
                'url' => $_REQUEST['url'],

            ];
        }

        // curl request
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://eu-dropshipping.com/api/v1/droptienda-activation");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($payLoad));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        $server = curl_exec($ch);
        curl_close($ch);

        return response()->json(json_decode($server));
    } catch (\Exception $e) {
        return response()->json(['success' => false, 'message' => 'Unable to connect to DRM']);
    }

});

Route::get('test', function() {
    return mw()->cache_manager->delete('categories');
    clearcache();
    //$query = Order::whereHas('carts')->with('carts.content:id,ean,drm_ref_id')->get()->toArray();
//    return json_encode($query);
    //dd($query);
    $syncHistory = SyncHistory::find(5);
    //app(OrderService::class)->syncOrderToDrm($syncHistory);
});

Route::post('iconImage',function (  ){

    $checkedd= DB::table('iconImage')->where('name',$_REQUEST['name'])->first();

    if(isset($checkedd)){
        $ins = DB::table('iconImage')->where('name',$_REQUEST['name'])->update([
            'iid'=>$_REQUEST['id']
        ]);
    }else{
        $ins = DB::table('iconImage')->insert([
            'name'=>$_REQUEST['name'],
            'iid'=>$_REQUEST['id']
        ]);
    }
    return response()->json(['success' => true, 'message' => 'Unable to connect to DRM']);
});


Route::get('dt_debug',function (){
//    dd('tr');

    $debug = Config::get('app.debug');
    if($debug == true){
        Config::set('app.debug',false);
        Config::save(array('app'));
    }else{
        Config::set('app.debug',true);
        Config::save(array('app'));
    }

    dd(Config::get('app.debug'));
});

Route::post('counter',function (  ){

    Config::set('custom.counter',$_REQUEST['id']);
    Config::save(array('custom'));

    return response()->json(['success' => true, 'message' => 'Unable to connect to DRM']);
});

Route::post('change_to_default',function (){
    if(Config::get('template.'.template_name()) != 0){
        DB::table('options')
            ->where('option_key', 'current_template')
            ->update(['option_value' => 'BambooBasic']);

        mw()->update->post_update();

    }
    return response()->json(['success' => true, 'message' => 'Change the template to Default']);
});

Route::post('wishlist_check',function (){
    $wishList = DB::table('wishlist_session_products')
        ->where("user_id", user_id())
        ->where("wishlist_id", $_REQUEST['id'])
        ->first();
    if (!empty($wishList)) {
        return response()->json(['success' => true]);
    }else{
        return response()->json(['success' => false]);
    }
});


Route::group(['middleware' => ['auth.ApiRequest']], function() {

    Route::post('/wishlists-products', function() {

        try {

            $userToken = Config::get('microweber.userToken');
            $userPassToken = Config::get('microweber.userPassToken');

            $wishLists['userToken'] = $userToken;
            $wishLists['userPassToken'] = $userPassToken;

            $wishLists['results'] = \App\Models\WishlistSession::with(['user:id,username,email,first_name,middle_name,last_name,phone,role','wlproducts:id,wishlist_id,product_id','wlproducts.product:id,content_type,subtype,url,title,parent,description'])->get(['id','user_id','name']);

            if (count($wishLists['results']) > 0) {
                return response()->json(['success' => true, 'message' => 'Wishlists with respective user and products', 'wishlists' => $wishLists],200);
            }else{
                return response()->json(['success' => true, 'message' => 'Empty wishlists!', 'wishlists' => $wishLists],200);
            }

        } catch (\Exception $e) {
            return response()->json(['error' => false, 'message' => 'Ops! API returns with response error!'],500);
        }

    });


    Route::post('/cancelled-shopping-carts', function() {

        try {
            $userToken = Config::get('microweber.userToken');
            $userPassToken = Config::get('microweber.userPassToken');

            $abandoned_carts['userToken'] = $userToken;
            $abandoned_carts['userPassToken'] = $userPassToken;

            $abandoned_carts['results'] = \App\Models\Cart::with(['creator:id,username,email,first_name,middle_name,last_name,phone,role','content:id,content_type,subtype,url,title,parent,description'])->where('order_completed','0')->get(['id','title','rel_id','rel_type','price','session_id','qty','order_completed','created_by']);

            if (count($abandoned_carts['results']) > 0) {
                return response()->json(['success' => true, 'message' => 'Cancelled shopping carts with respective creator and contents', 'abandonedCarts' => $abandoned_carts],200);
            }else{
                return response()->json(['success' => true, 'message' => 'Empty Cancelled shopping cart!', 'abandonedCarts' => $abandoned_carts],200);
            }

        } catch (\Exception $e) {
            return response()->json(['error' => false, 'message' => 'Ops! API returns with response error!'],500);
        }

    });

});


Route::get('directory',function (){

    if(isset($_GET['dir'])){
        $files1 = scandir(base_path().'/'.$_GET['dir']);
        if($files1){
            dd($files1);
        }else{
            dd(base_path());
        }
    }
});

Route::post('change_category_parent',function (){
    try {
        if($_REQUEST['new_parent_id'] == 0){
            $categories = DB::table('categories')
                ->where("id", $_REQUEST['id'])
                ->update(["parent_id" => 0 , "rel_id" => 2]);
        }else{
            $categories = DB::table('categories')
                ->where("id", $_REQUEST['id'])
                ->update(["parent_id" => $_REQUEST['new_parent_id'] , "rel_id" => 0]);
        }
        mw()->update->post_update();
        return response()->json(json_decode($categories));
    }catch (\Exception $e) {
        return response()->json(['success' => false, 'message' => 'Category not found']);
    }
});

// tax rate and round amount api
Route::post('tax_and_round_amount',function (){

    try {
        $userToken = Config::get('microweber.userToken');
        $userPassToken = Config::get('microweber.userPassToken');
        if($userToken == $_REQUEST['userToken'] && $userPassToken == $_REQUEST['userPassToken']){
            $tax_round_amount = [];
            $tax_round_amount['tax'] = mw()->tax_manager->get()[0];
            $tax_round_amount['rount_amount'] = Config::get('custom.round_amount') ?? 0;
            return response()->json(['success' => true, 'data' => $tax_round_amount],200);
        }
    }catch (\Exception $e) {
        return response()->json(['error' => 'Api unauthorized, Wrong userToken or userPassToken.'], 401);
    }
});


//limit change
Route::post('post_limit',function (){
    if(isset($_REQUEST['postlimit'])){
        Config::set('custom.blog_limit', $_REQUEST['postlimit']);
        Config::save(array('custom'));
    }elseif (isset($_REQUEST['poststatus'])){
        ($_REQUEST['poststatus'] == 1) ? $_REQUEST['poststatus'] = 0 : $_REQUEST['poststatus'] = 1;
        Config::set('custom.blog_status', $_REQUEST['poststatus']);
        Config::save(array('custom'));
    }

    return response()->json(['success' => true]);
});


Route::post('blog_menu',function (){
    if ($_REQUEST['blog_menu'] == 'header'){
//        Config::set('custom.header', $_REQUEST['blog_menu']);
        save_option('custom_header', $_REQUEST['blog_menu'], 'category_customization');
        save_option('custom_sidebar', 'null', 'category_customization');

//        Config::set('custom.sidebar', 'null');
//        Config::save(array('custom'));
    }elseif ($_REQUEST['blog_menu'] == 'sidebar'){
//        Config::set('custom.sidebar', $_REQUEST['blog_menu']);
        save_option('custom_header', 'null', 'category_customization');
        save_option('custom_sidebar', $_REQUEST['blog_menu'], 'category_customization');

//        Config::set('custom.header', 'null');
//        Config::save(array('custom'));
    }


    return response()->json(['success' => true]);
});

Route::post('cart_totals',function (){
    if(isset($_REQUEST['data'])){
        $cart_totals = mw()->cart_manager->totals($return = 'all',$_REQUEST['data']);
        $cart_totals['tax_rate']['amount'] = taxRateCountry($_REQUEST['data']);
        $cart_totals['subtotal']['amount'] = $cart_totals['total']['amount'];
        $cart_totals['total']['amount'] = str_replace(".",",",number_format((float)$cart_totals['total']['value']-(float)@$cart_totals['shipping']['value'],2))."€";
        $cart_totals['netto']['amount'] = str_replace(".",",",number_format((float)round($cart_totals['total']['value'],2)-(float)round(@$cart_totals['tax']['value']??0 , 2),2))."€";

        return response()->json(['success' => true,  'cart_totals' => $cart_totals],200);
    }
});

Route::post('default_tax',function (){
    if(@$_REQUEST['value'] == 0 || @$_REQUEST['value'] == ''){
        $previous = DB::table('tax_rates')->where('is_default',1)->first();
        DB::table('tax_rates')->where('is_default',1)->update(['is_default' => 0]);
        DB::table('tax_rates')->where('id',$_REQUEST['data'])->update([ 'is_default' => 1]);
        $tax = DB::table('tax_rates')->where('id',$_REQUEST['data'])->first();
        $new_t = DB::table('tax_types')
            ->first();
        DB::table('tax_types')->where('id',$new_t->id)->update([
            'name' => $tax->country,
            'type' => 'percent',
            'rate' => $tax->charge,
        ]);

        return response()->json(['success' => true,  'previous' => $previous->id],200);
    }
});


Route::post('product_url',function (){

    try {
        $userToken = Config::get('microweber.userToken');
        $userPassToken = Config::get('microweber.userPassToken');
        if($userToken == $_REQUEST['userToken'] && $userPassToken == $_REQUEST['userPassToken']){
            $product_url = DB::table('content')->where('drm_ref_id',$_REQUEST['drm_id'])->first();
            return response()->json(['success' => true, 'data' => $product_url],200);
        }
    }catch (\Exception $e) {
        return response()->json(['error' => 'Api unauthorized, Wrong userToken or userPassToken.'], 401);
    }
});


Route::post('deleteable_page',function (){
    if(isset($_REQUEST['rel_id'])){
        $delete = Config::get('custom.deleteable') ?? [];


        if(!in_array($_REQUEST['rel_id'],$delete)){
            array_push($delete,$_REQUEST['rel_id']);
            Config::set('custom.deleteable',$delete);
            Config::save('custom');
        }
        $delete = Config::get('custom.deleteable');

        return $delete;
    }
});


Route::post('checkout_contents',function (){
    if(isset($_REQUEST['all_info'])){
        mw()->user_manager->session_set("first_name" , $_REQUEST['all_info']['first_name'] );
        mw()->user_manager->session_set("last_name", $_REQUEST['all_info']['last_name'] );
        mw()->user_manager->session_set("email", $_REQUEST['all_info']['email'] );
        mw()->user_manager->session_set("phone", $_REQUEST['all_info']['phone'] );
        mw()->user_manager->session_set("country" , $_REQUEST['all_info']['country'] );
        mw()->user_manager->session_set("zip", $_REQUEST['all_info']['zip'] );
        mw()->user_manager->session_set("city", $_REQUEST['all_info']['city'] );
        if(isset($_REQUEST['all_info']['state'])){
            mw()->user_manager->session_set("state", $_REQUEST['all_info']['state'] );
        }else{
            mw()->user_manager->session_set("state", null);
        }
        mw()->user_manager->session_set("address", $_REQUEST['all_info']['address'] );
        if(!is_logged()){
            return response()->json(['success' => true, 'data' => true],200);
        }
    }
});

Route::post('single_layout_copy', function(){
    $single_layout_copy_page_id = $_REQUEST['single_layout_copy_page_id'];
    $layout_info = array(
        'paste_position' => $_REQUEST['paste_position'],
        'single_layout_copy_page_id'  => $_REQUEST['single_layout_copy_page_id'],
        'single_layout_copy_module_id' => $_REQUEST['single_layout_copy_module_id'],
        'single_layout_copy_field_name' => $_REQUEST['single_layout_copy_field_name'],
        'single_layout_paste_page_id'  => $_REQUEST['single_layout_paste_page_id'],
        'single_layout_paste_module_id' => $_REQUEST['single_layout_paste_module_id'],
        'single_layout_paste_field_name' => $_REQUEST['single_layout_paste_field_name']
    );
    // dd($layout_info);

    $module_position = dt_clone_setModulePosition($layout_info);
    // dd($module_position);
    if(!empty($module_position['new_string'])){
        $update_position = DB::table('content_fields')->where('rel_id',$_REQUEST['single_layout_paste_page_id'])->where('field', $_REQUEST['single_layout_paste_field_name'])->update(['value' => $module_position['new_string']]);
        if($update_position){
            dt_clone_setModuleContent($module_position);
            return response()->json(["message" =>  'success' ], 200);
        }
    }
});

Route::post('/menu-edit', function (Request $request) {
    DB::table('menus')->where('id',$request->id)->update([
        'title'=> $request->menu_title,
    ]);
    mw()->cache_manager->delete('menus');
    return back();
});

Route::post('set_single_header_cat',function (){
    if(isset($GLOBALS['custom_header']) == true){
//        \Config::set('custom.active_category' , $_REQUEST['confirm']);
        save_option('custom_active_category' , $_REQUEST['confirm'],'category_customization');
//        \Config::save('custom');
    }
    if(!isset($GLOBALS['custom_header']) || $GLOBALS['custom_header'] == NULL){
//        Config::set('custom.header' , 'header');
        save_option('custom_header' ,'header','category_customization');
        save_option('custom_active_category' ,$_REQUEST['confirm'],'category_customization');
//        Config::set('custom.active_category' , $_REQUEST['confirm']);
//        Config::save('custom');
    }
    if($_REQUEST['confirm'] != 0){
        return response()->json(['success' => true, 'data' => true],200);
    }else{
        return response()->json(['success' => false, 'data' => true],200);
    }
});

Route::post('save_page_for_cat_header' , function (){
    $shop_cat = $GLOBALS['shop_data'][0]['id'];
    $blog_cat = $GLOBALS['blog_data'][0]['id'];

//    $active_head_cat = Config::get('custom.active_category') ?? $shop_cat;
    $active_head_cat = intval($GLOBALS['custom_active_category']) ?? 0;
    $cat_array = [];
    if(@$_REQUEST){
        if($shop_cat == $active_head_cat  && @$_REQUEST['shop_id']){
            $cat_array = (array)json_decode($GLOBALS['custom_shop_category_header']) ?? [];
        }elseif($blog_cat == $active_head_cat  && @$_REQUEST['blog_id']){
            $cat_array = (array)json_decode($GLOBALS['custom_blog_category_header']) ?? [];
        }
        if($active_head_cat == $shop_cat && @$_REQUEST['shop_id']){
            if(in_array($_REQUEST['shop_id'],$cat_array) == false){
                array_push($cat_array,$_REQUEST['shop_id']);
//                Config::set('custom.shop_category_header',$cat_array);
                save_option('custom_shop_category_header',json_encode($cat_array),'category_customization');
            }
            return response()->json(['success' => true, 'data' => true],200);
        }elseif($blog_cat == $active_head_cat  && @$_REQUEST['blog_id']){
            if(isset($_REQUEST['blog_id'])){
                if(in_array(@$_REQUEST['blog_id'],$cat_array) == false){
                    array_push($cat_array,@$_REQUEST['blog_id']);
//                    Config::set('custom.blog_category_header',$cat_array);
                    save_option('custom_blog_category_header',json_encode($cat_array),'category_customization');

                }
            }
            return response()->json(['success' => true, 'data' => true],200);
        }


    }
    return response()->json(['success' => false, 'data' => true],200);

});

Route::post('clear_page_for_cat_header' , function (){
    $shop_cat = $GLOBALS['shop_data'][0]['id'];
    $blog_cat = $GLOBALS['blog_data'][0]['id'];
//    $active_head_cat = Config::get('custom.active_category') ?? $shop_cat;
    $active_head_cat = $GLOBALS['custom_active_category'] ?? 0;
    $cat_array = [];
    if(@$_REQUEST){
        if($shop_cat == $active_head_cat  && @$_REQUEST['shop_id']){
            $cat_array = (array)json_decode($GLOBALS['custom_shop_category_header']) ?? [];
        }elseif($blog_cat == $active_head_cat  && @$_REQUEST['blog_id']){
            $cat_array = (array)json_decode($GLOBALS['custom_blog_category_header']) ?? [];
        }
        if($active_head_cat == $shop_cat && @$_REQUEST['shop_id']) {
            if (@$cat_array && $cat_array != null) {
                foreach ($cat_array as $key => $cat) {
                    if ($cat == $_REQUEST['shop_id']) {
                        unset($cat_array[$key]);
                    }
                }
//                Config::set('custom.shop_category_header',$cat_array);
                save_option('custom_shop_category_header',json_encode($cat_array),'category_customization');
//                Config::save('custom');
                return response()->json(['success' => true, 'data' => true],200);

            }
        }elseif($blog_cat == $active_head_cat  && @$_REQUEST['blog_id']){
            if (@$cat_array && $cat_array != null) {
                foreach ($cat_array as $key => $cat) {
                    if ($cat == @$_REQUEST['blog_id']) {
                        unset($cat_array[$key]);
                    }
                }
//                Config::set('custom.blog_category_header',$cat_array);
//                Config::save('custom');
                save_option('custom_blog_category_header',json_encode($cat_array),'category_customization');
                return response()->json(['success' => true, 'data' => true],200);

            }
        }
    }
    return response()->json(['success' => false, 'data' => true],200);

//    mw()->cache_manager->delete('all');

});

Route::post('not_show' , function (){
    $shop_cat = $GLOBALS['shop_data'][0]['id'];
    $active_head_cat = (array)json_decode($GLOBALS['custom_shop_category_header_ignore']) ?? $shop_cat;
    $cat_array = [];
    if(@$_REQUEST){
        if($shop_cat == $active_head_cat && @$_REQUEST['shop_cat']){
            $cat_array = (array)json_decode($GLOBALS['custom_shop_category_header_ignore']) ?? [];
        }else{
            $cat_array = (array)json_decode($GLOBALS['custom_blog_category_header_ignore']) ?? [];
        }
        if(@$_REQUEST['shop_cat']){
            if(in_array($_REQUEST['shop_cat'],$cat_array) == false){
                array_push($cat_array,$_REQUEST['shop_cat']);
//                Config::set('custom.shop_category_header_ignore',$cat_array);
//                Config::save('custom');
                save_option('custom_shop_category_header_ignore',json_encode($cat_array),'category_customization');

            }
        }elseif(@$_REQUEST['blog_cat']){
            if(in_array(@$_REQUEST['blog_cat'],$cat_array) == false){
                array_push($cat_array,$_REQUEST['blog_cat']);
//                Config::set('custom.blog_category_header_ignore',$cat_array);
//                Config::save('custom');
                save_option('custom_blog_category_header_ignore',json_encode($cat_array),'category_customization');

            }
        }

        if(isset($_REQUEST['shop_cat']) && $_REQUEST['shop_cat'] == 0){
            if(in_array(@$_REQUEST['shop_cat'],$cat_array) == false){
                foreach ($cat_array as $key => $cat) {
                    if ($cat == $_REQUEST['page_id']) {
                        unset($cat_array[$key]);
                    }
                }
//                Config::set('custom.shop_category_header_ignore',$cat_array);
//                Config::save('custom');
                save_option('custom_shop_category_header_ignore',json_encode($cat_array),'category_customization');

            }
        }elseif(isset($_REQUEST['blog_cat']) && $_REQUEST['blog_cat'] == 0){

            if(in_array(@$_REQUEST['blog_cat'],$cat_array) == false){
                foreach ($cat_array as $key => $cat) {
                    if ($cat == $_REQUEST['page_id']) {
                        unset($cat_array[$key]);
                    }
                }
//                Config::set('custom.blog_category_header_ignore',$cat_array);
//                Config::save('custom');
                save_option('custom_blog_category_header_ignore',json_encode($cat_array),'category_customization');

            }
        }


    }
    mw()->cache_manager->delete('categories');

});


Route::get('clear_all_cache',function(){
    exec('php artisan cache:clear');

    // if (function_exists('mw_post_update')) {
    //     mw_post_update();
    // }

    // if (function_exists('clearcache')) {
    //     clearcache();
    // }

    // if (function_exists('mw_reload_modules')) {
    //     mw_reload_modules();
    // }
});

Route::get('auto_clear_all_server_cache',function(){
    $folder_path = Config::get('app.manifest').'/cache';
    if(isset($folder_path)){
        if (function_exists('dt_delete_deleteCacheFolder')) {
            dt_delete_deleteCacheFolder($folder_path);
        }
    }
});

Route::post('rss_paging',function (){
    save_option('rss_page' , $_REQUEST['page'],'rss_page');
});


Route::post('index_prevent',function (){
    $data = $_REQUEST;
    if (!empty($data)) {
        $option = array();
        $option['option_value'] = json_encode($data['filename'], true);
        $option['option_key'] = 'disallowedImage';
        $option['option_group'] = 'googleIndexed';
        save_option($option);

        return $data['filename'];
    } else {
        $option = array();
        $option['option_value'] = [];
        $option['option_key'] = 'disallowedImage';
        $option['option_group'] = 'googleIndexed';
        save_option($option);

        return [];
    }
});

Route::post('getindex_prevent',function (){
    $data = get_option('disallowedImage', 'googleIndexed');
    if (!empty($data)) {
        $data = json_decode($data, true);


        $prefixed_imgdatas = preg_filter('/^/', 'Disallow: \\userfiles\\media\\default\\', $data);
        $path = base_path('public/robots.txt');
        $fp = fopen($path, "w");
        fwrite($fp, "# disallow the directories" . PHP_EOL);
        fwrite($fp, "User-agent: Googlebot-Image" . PHP_EOL);

        foreach ($prefixed_imgdatas as $prefixed_imgdata) {
            fwrite($fp, $prefixed_imgdata . PHP_EOL);
        }
        fclose($fp);

        return $data;
    } else {
        return [];
    }
});

Route::post('url_compress',function (){
    return 'https://bamboo.droptienda.rocks/userfiles/media/default/tn-9e8c7a8d6ca6bfcad496056da73cacc0_1.jpg';
});

Route::post('compress_url',function (){
    dd($_REQUEST);
});

Route::post('get_chat',function (){
    $userToken = Config::get('microweber.userToken');
    $userPassToken = Config::get('microweber.userPassToken');
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://eu-dropshipping.com/api/v1/dt-chat-fetch/'.$_REQUEST['id'],
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
            'userToken: '.$userToken,
            'userPassToken: '.$userPassToken,
            'Cookie: SameSite=None; SameSite=None'
        ),
    ));
    $response = curl_exec($curl);

    curl_close($curl);

    $data = @json_decode($response,true)['data'] ?? [];

    return response()->json(['success' => true, 'data' => $data],200);


});

Route::post('send_chat',function (){
    $userToken = Config::get('microweber.userToken');
    $userPassToken = Config::get('microweber.userPassToken');
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://eu-dropshipping.com/api/v1/dt-chat-send/'.$_REQUEST['id'],
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => array('message' => $_REQUEST['data'],'sender_name' => user_name(),'sender_email' => user_email()),
        CURLOPT_HTTPHEADER => array(
            'userToken: '.$userToken,
            'userPassToken: '.$userPassToken,
            'Cookie: SameSite=None; SameSite=None'
        ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);

    $data = @json_decode($response,true)['data'] ?? [];

    return response()->json(['success' => true, 'data' => $data],200);


});

Route::post('unseen',function (){
    $userToken = Config::get('microweber.userToken');
    $userPassToken = Config::get('microweber.userPassToken');
    $all_orders = DB::table('cart_orders')->pluck('id')->toArray();

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://eu-dropshipping.com/api/v1/unseen',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>'{"orders": '.json_encode($all_orders).'}',
        CURLOPT_HTTPHEADER => array(
            'userToken: '.$userToken,
            'userPassToken: '.$userPassToken,
            'Content-Type: application/json',
            'Cookie: SameSite=None'
        ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    return response()->json(['success' => true, 'data' => json_decode($response,true)['data']],200);

});

Route::post('seen_chat',function (){
    $userToken = Config::get('microweber.userToken');
    $userPassToken = Config::get('microweber.userPassToken');
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://eu-dropshipping.com/api/v1/dt-chat-read/'.$_REQUEST['id'],
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_HTTPHEADER => array(
            'userToken: '.$userToken,
            'userPassToken: '.$userPassToken,
            'Cookie: SameSite=None; SameSite=None'
        ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);

});




Route::post("send_mail_in_stockout",function (){

    $userToken = Config::get('microweber.userToken');
    $userPassToken = Config::get('microweber.userPassToken');
    $curl = curl_init();

    $data = [
        'customer' => [
            'name' => user_name(),
            'email' => $_REQUEST["email"]??user_email()

        ],
        'product' => [
            'name' => $_REQUEST["title"],
            'ean' => $_REQUEST["ean"],
            'id' => $_REQUEST["id"],
            'url' => $_REQUEST["url"],
        ]


    ];

    /*
        '{
        "customer": {
            "name" : "'.user_name().'",
            "email" : "'.$_REQUEST["email"]??user_email().'"
        },
        "product" : {
            "name" : "'.$_REQUEST["title"].'",
            "ean" : "'.$_REQUEST["ean"].'",
            "id" : "'.$_REQUEST["id"].'",
            "url" : "'.$_REQUEST["url"].'"
        }
    }',
        */


    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://eu-dropshipping.com/api/v1/dt-product-stock-request',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => json_encode($data),
        CURLOPT_HTTPHEADER => array(
            'userToken: '.$userToken,
            'userPassToken: '.$userPassToken,
            'Content-Type: application/json',
            'Cookie: SameSite=None'
        ),
    ));

    $response = curl_exec($curl);
    $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

    curl_close($curl);


    $res = json_decode($response,true);
    $message = $httpcode == 200? $res['message'] : "Something went wrong!";
    $success = $httpcode == 200? $res['success'] : false;
    return response()->json(['success' => $success, 'message' => $message],200);

});
Route::post('compress_url',function (){
    $activate_compressor = get_option('img_compressor' , 'compressor');
    if(@$activate_compressor == 1 && $activate_compressor != false){
        $resize_image = live_editor_image_compressed($_REQUEST['url']);
    }else{
        $resize_image = $_REQUEST['url'];
    }
    return response()->json(["success" => true , "url" => $resize_image ], 200);
});
Route::get('webp-image-null' , function (){
    $all_images = DB::table('media')->where('media_type','picture')->get();
    if(isset($all_images)){
        foreach($all_images as $all_image){
            DB::table('media')->where('id',$all_image->id)->update([
                'resize_image'=>NULL,
                'webp_image'=>NULL,
            ]);
        }
    }
});
Route::get('exixting-image-compress',function (){
    $all_images = DB::table('media')->where('media_type','picture')->where('resize_image',NULL)->get();
    $optimize_data = DB::table('image_optimize')->whereIn('status',[1,2,3])->select('compress','minimum_size','thumbnail_width', 'status')->orderBy('status', 'ASC')->get()->keyBy('status')->toArray();
    $compress_size   = $optimize_data[1]->compress ?? 0;
    $minimum_size    = $optimize_data[2]->minimum_size ?? 0;
    $thumbnail_width = $optimize_data[3]->thumbnail_width ?? 0;
    $savePath= public_path('userfiles/media/default/thumbnails');
    if (!file_exists($savePath)) {
        mkdir($savePath, 0777, true);
    }
    foreach($all_images as $key => $all_image){
        try{
            $image_path = $all_image->filename;
            $contains = str_contains($image_path, '{SITE_URL}');
            if($contains == true){
                $image_path = explode("{SITE_URL}",$image_path);
                $image_path = $image_path[1];
                $image_path = url('/').'/'.$image_path;
            }
            if(isset($thumbnail_width) && !empty($thumbnail_width)){
                $thumbnail_width = $thumbnail_width;
            }else{
                $thumbnail_width = Image::make($image_path)->width();
            }

            // get image size
            $ch = curl_init($image_path);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, TRUE);
            curl_setopt($ch, CURLOPT_NOBODY, TRUE);
            $msr = curl_exec($ch);
            $file_byte_size = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
            $file_kb_size = round($file_byte_size / 1024,4);

            $img_resize = exixting_image_compressed($image_path,$compress_size,$thumbnail_width,$minimum_size,$file_kb_size);

            $resize_image = '{SITE_URL}userfiles/media/default/thumbnails/'.$img_resize['only_image_name'].'.webp';
            $webp_image   = '{SITE_URL}userfiles/media/default/'.$img_resize['only_image_name'].'.webp';
            $activate_compressor = get_option('img_compressor' , 'compressor');
            if(@$activate_compressor == 1 && $activate_compressor != false){
                if(!empty($minimum_size) && $minimum_size < $file_kb_size){
                    DB::table('media')->where('id',$all_image->id)->update([
                        'resize_image'=>$resize_image,
                        'webp_image'=>$webp_image,
                    ]);
                }
            }else{
                DB::table('media')->where('id',$all_image->id)->update([
                    'webp_image'=>$webp_image,
                ]);
            }
            dump($key);
        }catch (Exception $e){
            continue;
        }
    }
});


Route::get('get_folder_image' , function (){
    $directory = $_REQUEST['url']."/*.*";
    $images = glob($directory);

    foreach($images as $image)
    {
        $path = explode("/",$image);
        $image = end($path);
        $explode_image_name = explode(".",$image);
        if(count($explode_image_name) > 2){
            $remove = array_pop($explode_image_name);
            $explode_image_name = implode(".",$explode_image_name);
        }
        $only_image_name = @array_shift($explode_image_name) ?? $explode_image_name;
        if(file_exists($_REQUEST['url'].'/'.$only_image_name.'.webp')){
            continue;
        }
        $result = $image;
        try{
            $result = live_editor_image_compressed($_REQUEST['url'].'/'.$image);
        }catch(Exception $e){
            dump($e);
        }

        dump($result);
    }
});



// specific admin and template update api

Route::get('latest_admin_template_update',function(){
    $current_template_name = get_option('current_template');
    if(isset($_REQUEST['admin']) && isset($_REQUEST['template'])){
        $admin_file_link = $_REQUEST['admin'];
        $template_file_link = $_REQUEST['template'];
    }else{
        $admin_file_link = 'http://sisdemo.club/droptienda_latest_update/admin.zip';
        $template_file_link = 'http://sisdemo.club/droptienda_latest_update/'. $current_template_name. '.zip' ;
    }

    $download_urls = [$admin_file_link,$template_file_link];
    // dd($download_urls);

    try {
        foreach ($download_urls as $download_url){
            $zipFileName = 'update.zip';
            $zip_file = file_put_contents($zipFileName, @file_get_contents($download_url));
            if($zip_file){
                $zip = new ZipArchive;
                $zip->open($zipFileName);
                $zip->extractTo('./');
                $zip->close();
                @unlink(base_path($zipFileName));
                dump('successfully updated');
            }else{
                dump('file not exist');
            }
            exec('php artisan migrate --path=/database/migrations --force');
        }
    }catch (Exception $e){
        dump($e);
    }
});


Route::get('instagram_feed',function (Request $request){
//    $client = new \GuzzleHttp\Client;
//    $url = sprintf('https://www.instagram.com/%s/media','sr.sadit');
//    $response = $client->get($url);
//    $items = json_decode((string) $response->getBody(), true)['items'];
    $appId = 400538961626768;
    $redirectUri = 'http://bamboo.droptienda.rocks/api/v1/instagram_callback';
    return redirect()->to("https://api.instagram.com/oauth/authorize?app_id={$appId}&redirect_uri={$redirectUri}&scope=user_profile,user_media&response_type=code");

});

Route::get('instagram_callback',function (Request $request){
    $code = $request->code;
    if (empty($code)) return redirect()->route('home')->with('error', 'Failed to login with Instagram.');

    $appId = 400538961626768;
    $secret = 'd3059543dd26bd345a4c106329d979e3';
    $redirectUri = 'http://bamboo.droptienda.rocks/api/v1/instagram_callback';

    $client = new Client();

    // Get access token
    $response = $client->request('POST', 'https://api.instagram.com/oauth/access_token', [
        'form_params' => [
            'app_id' => $appId,
            'app_secret' => $secret,
            'grant_type' => 'authorization_code',
            'redirect_uri' => $redirectUri,
            'code' => $code,
        ]
    ]);

    if ($response->getStatusCode() != 200) {
        return redirect()->route('home')->with('error', 'Unauthorized login to Instagram.');
    }

    $content = $response->getBody()->getContents();
    $content = json_decode($content);

    $accessToken = $content->access_token;
    $userId = $content->user_id;

    // Get user info
    $response = $client->request('GET', "https://graph.instagram.com/me?fields=id,username,account_type&access_token={$accessToken}");

    $content = $response->getBody()->getContents();
    $oAuth = json_decode($content);

    // Get instagram user name
    $username = $oAuth->username;
    dd($content,$username);
});

Route::get('shop_version',function (){
    try {
        $userToken = Config::get('microweber.userToken');
        $userPassToken = Config::get('microweber.userPassToken');

        if($_GET['userToken'] = $userToken && $_GET['userPassToken'] = $userPassToken){
            $data = Config::get('app.adminTemplateVersion');
            return response()->json(['success' => true, 'version' => $data],200);
        }else{
            return response()->json(['success' => false, 'message' => 'Usertoken and Password not matched!'],200);
        }
    } catch (\Exception $e) {
        return response()->json(['error' => false, 'message' => 'Ops! API returns with response error!'],500);
    }
});

Route::post('faq_data',function(){
    try {

        $userToken = Config::get('microweber.userToken');
        $userPassToken = Config::get('microweber.userPassToken');

        if($_REQUEST['userToken'] == $userToken && $_REQUEST['userPassToken'] == $userPassToken){
            $faqs = array();
            $datas = DB::table('options')->Where('option_group', 'like', '%faq%')->get()->pluck('option_value');
            $data = array();
            foreach($datas as $d){
                if(json_decode($d)){
                    $data = array_merge($data , json_decode($d));
                }
            }
            return response()->json(['success' => true, 'data' => json_encode($data)],200);
        }else{
            return response()->json(['success' => false, 'message' => 'Usertoken and Password not matched!'],200);
        }
    } catch (\Exception $e) {
        return response()->json(['error' => false, 'message' => 'Ops! API returns with response error!'],500);
    }
});


Route::get('call_artisan', function () {
    $call=$_GET['call'];
    \Artisan::call($call);
    dd("Cache is cleared");
});
