<?php if ($footer == 'true'): ?>
    <style>
        footer ul li a {
            display: block;
            color: #f3f3f3;
            text-decoration: none;
            padding: 10px 12px !important;
        }
        footer ul li {
            display: inline-block !important;
            color: #f3f3f3;
            width: auto;
        }
    </style>
    <footer class="p-t-60 p-b-60">
        <div class="container">
            <div class="edit nodrop safe-mode" field="bamboo_footer" rel="global">
                <div class="row">
                    <div class="mx-auto col-xl-8 allow-drop text-center">
                        <p><?php print _lang('THE CONTENT BELONGS TO  pivotsubscriptions.com', 'templates/bamboo'); ?></p>
                        <p><?php print _lang('All photo and video materials belong to their owners and are used for demonstration purposes only.', 'templates/bamboo'); ?></p>
                        <p><?php print _lang('Please do not use them in commercial projects.', 'templates/bamboo'); ?></p>
                        <br/><br/>
                    </div>
                </div>
            </div>

            <div class="edit row copyright" field="bamboo_footer_text" rel="global">
                <div class="col-12">
                    <!-- <p><?php print powered_by_link(); ?>. <?php print _e("All rights Reserved."); ?></p> -->
                    <p>Shopsystem & Template by <a href="https://www.droptienda.com/" target="_blank">Droptienda®</a> </p>
                </div>
            </div>
        </div>
    </footer>
<?php endif; ?>

</div>

<button id="to-top" class="btn" style="display: block;">
    <span class="material-icons">keyboard_arrow_up</span>
</button>

<?php include('footer_cart.php'); ?>

<!-- Plugins -->
<script src="https://maps.googleapis.com/maps/api/js?sensor=false&amp;language=en-GB&amp;key=AIzaSyDbN7i-eF7dlNNp-bxbERNomOGYpZld3B0"></script>

<script src="<?php print template_url(); ?>assets/js/libs/swiper/js/swiper.min.js"></script>
<script src="<?php print template_url(); ?>assets/plugins/elevatezoom/jquery.elevatezoom.js"></script>
<script type="text/javascript" src="<?php print template_url(); ?>assets/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="<?php print template_url(); ?>assets/plugins/masonry/masonry.pkgd.min.js"></script>
<script type="text/javascript" src="<?php print template_url(); ?>assets/plugins/masonry/isotope.pkgd.min.js"></script>

<script src="<?php print template_url(); ?>assets/js/libs/anime.min.js"></script>
<script src="<?php print template_url(); ?>assets/js/libs/particles.js"></script>
<script src="<?php print template_url(); ?>assets/js/libs/jquery.sticky-sidebar.min.js"></script>
<script>
    mw.lib.require('slick');
    mw.lib.require('collapse_nav');
</script>
<script src="<?php print template_url(); ?>assets/js/main.js"></script>

<script src="<?php print template_url(); ?>assets/js/fx.js"></script>

<script type="text/javascript">
// Check browser support
if (typeof(Storage) !== "undefined") {

    if(localStorage.getItem("popup") != "hide"){
        console.log(localStorage.getItem("popup"));

    document.addEventListener('mouseout', e => {
    if (!e.toElement && !e.relatedTarget) {
        document.querySelector('.exit-intent-popup').classList.add('visible');
        $(".custom-modal").addClass('bg-overlay')
        // $(".custom-modal").css("background","#00000063");
        // $("body").css('overflow','hidden');
    }
    });
    const mouseEvent = e => {
        if (!e.toElement && !e.relatedTarget) {
        document.removeEventListener('mouseout', mouseEvent);
        document.querySelector('.exit-intent-popup').classList.add('visible');


    }
    };

    document.addEventListener('mouseout', mouseEvent);

    const exit = e => {
    if (e.target.className === 'close') {
        document.querySelector('.exit-intent-popup').classList.remove('visible');
        // $(".custom-modal").css("background","none");
        $("body").css('overflow','scroll');

        $(".custom-modal").removeClass('bg-overlay');
        $('.custom-modal').addClass('hide');
    }
    };
    document.querySelector('.exit-intent-popup').addEventListener('click', exit);
    //User delete function
    $(document).on('click', '#user_delete_btn_fr', function(e){
    e.preventDefault();

    let url = $(this).data('url');
    swal({
        title: 'Delete Account',
        text: 'Are you sure?',
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#00a65a",
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
        closeOnConfirm: false
    }, function () {
        window.location = url;
    });
})
  // Store
  localStorage.setItem("popup", "hide");
  // Retrieve
}

} else {
  document.getElementById("result").innerHTML = "Sorry, your browser does not support Web Storage...";
}


</script>

</body>
</html>