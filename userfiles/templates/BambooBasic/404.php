<?php

/*

  type: layout
  content_type: static
  name: Error 404
  position: 11
  description: Error 404

*/

?>
<?php include template_dir() . "header.php"; ?>

<div class="edit" rel="content" field="bamboo_content">
    <module type="layouts" template="skin-17"/>
    <module type="layouts" template="skin-3"/>
</div>

<?php include template_dir() . "footer.php"; ?>
