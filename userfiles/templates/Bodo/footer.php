                <footer>
                    <module type="social_links" id="footer-social" show-icons="facebook,twitter,googleplus">

                        <div class="footer-bottom">
                            <div class="container">
                                <div class="row footer-copy-holder">
                                    <div class="col-lg-12">
                                        <p>Shopsystem & Template by <a style="color: #fff" href="https://www.droptienda.com/" target="_blank">Droptienda®</a> </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                </footer>
                <!--/.FOOTER-END-->

            <!--/.CONTENT END-->
            </div>
        <!--/.CONTENT-WRAP END-->
        </div>


        <script src="<?php print template_url(); ?>js/jquery.appear.js" type="text/javascript"></script>
        <script src="<?php print template_url(); ?>js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php print template_url(); ?>js/classie.js" type="text/javascript"></script>
        <script src="<?php print template_url(); ?>js/owl.carousel.min.js" type="text/javascript"></script>
        <script src="<?php print template_url(); ?>js/jquery.magnific-popup.min.js" type="text/javascript"></script>
        <script src="<?php print template_url(); ?>js/masonry.pkgd.min.js" type="text/javascript"></script>
        <script src="<?php print template_url(); ?>js/masonry.js" type="text/javascript"></script>
        <script src="<?php print template_url(); ?>js/smooth-scroll.min.js" type="text/javascript"></script>
        <script src="<?php print template_url(); ?>js/typed.js" type="text/javascript"></script>
        <script src="<?php print template_url(); ?>js/main.js" type="text/javascript"></script>
    </body>
</html>