<?php include template_dir() . "header.php"; ?>

    <section class="grey-bg">
        <div class="container">
            <div class="mw-ui-row">
                <div class="mw-ui-col">
                    <div class="text-layout-section2">
                        <div>
                            <div class="mw-ui-row shop-product-row">
                                <div class="mw-ui-col" style="width:50%">
                                    <div class="mw-ui-col-container product-gallery-col">
                                        <div class="wrap-card">
                                            <div class="card">
                                                <div class="edit" field="product_media" rel="post">
                                                    <module type="pictures" rel="content" template="product_gallery" class="no-thumbnails"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mw-ui-col" style="width:50%">
                                    <div class="mw-ui-col-container">
                                        <h2 class="edit title-inner" field="title" rel="content">Product inner page</h2>
                                        <div id="price-inner"></div>
                                        <div class="product-description">
                                            <div class="edit" field="content_body" rel="content">
                                                <p class="element">This text is set by default and it is suitable for edit in real time. By default the drag and drop core feature will allow you to
                                                    position it anywhere on the site. Get creative &amp; <strong style="font-weight: 600">Make Web</strong>.</p>
                                            </div>
                                            <module type="shop/cart_add" rel="post"/>
                                        </div>
                                        <script>document.querySelector('#price-inner').innerHTML = document.querySelector('.mw-price').textContent</script>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </section>

<?php include template_dir() . "footer.php"; ?>