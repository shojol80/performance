(function ($) {
    // "use strict";
    $(window).on("load", function () { // makes sure the whole site is loaded
        //preloader
        $("#status").fadeOut(); // will first fade out the loading animation
        $("#preloader").delay(450).fadeOut("slow"); // will fade out the white DIV that covers the website.

        //masonry
        $('.grid').masonry({
            itemSelector: '.grid-item'

        });
    });

    mw.share = mw.share || function () {
            $(".mw-share").each(function () {
                if (!this.mwshare) {
                    this.mwshare = true;
                    var url = $(this).attr('data-url') || location.href;
                    var sh = '<div class="mw-share-content">'
                        + '<a href="https://www.facebook.com/sharer/sharer.php?u=' + url + '" target="_blank"><span class="mw-icon-facebook"></span></a>'
                        + '<a href="https://twitter.com/share?text=&url=' + url + '" target="_blank"><span class="mw-icon-twitter"></span></a>'
                        + '<a href="https://plus.google.com/share?url=' + url + '" target="_blank"><span class="mw-icon-googleplus"></span></a></div>';

                    $(this).append(sh)
                    $(this).on('click', function () {
                        $(this).toggleClass('share-active');
                    });
                }
            })
        }


    $(document).ready(function () {
        mw.share();

        //active menu


        $('a[href^="#"]').on('click', function (e) {
            e.preventDefault();


            $('a').each(function () {
                $(this).removeClass('active');
            })
            $(this).addClass('active');

            var target = this.hash;
            $target = $(target);
            $('html, body').stop().animate({
                'scrollTop': $target.offset().top + 2
            }, 500, 'swing', function () {
                window.location.hash = target;

            });
        });


        //scroll js
        smoothScroll.init({
            selector: '[data-scroll]', // Selector for links (must be a valid CSS selector)
            selectorHeader: '[data-scroll-header]', // Selector for fixed headers (must be a valid CSS selector)
            speed: 500, // Integer. How fast to complete the scroll in milliseconds
            easing: 'easeInOutCubic', // Easing pattern to use
            updateURL: true, // Boolean. Whether or not to update the URL with the anchor hash on scroll
            offset: 0, // Integer. How far to offset the scrolling anchor location in pixels
            callback: function (toggle, anchor) {
            } // Function to run after scrolling
        });

        //menu
        var bodyEl = document.body,
            content = document.querySelector('.content-wrap'),
            openbtn = document.getElementById('open-button'),
            closebtn = document.getElementById('close-button'),
            isOpen = false;

        function inits() {
            initEvents();
        }

        function initEvents() {
            openbtn.addEventListener('click', toggleMenu);
            if (closebtn) {
                closebtn.addEventListener('click', toggleMenu);
            }

            // close the menu element if the target it´s not the menu element or one of its descendants..
            content.addEventListener('click', function (ev) {
                var target = ev.target;
                if (isOpen && target !== openbtn) {
                    toggleMenu();
                }
            });
        }

        function toggleMenu() {
            if (isOpen) {
                classie.remove(bodyEl, 'show-menu');
            }
            else {
                classie.add(bodyEl, 'show-menu');
            }
            isOpen = !isOpen;
        }

        inits();


        /*//typed js
         $(".typed").typed({
         strings: ["My Name is M.Reza", "I'm a Web Designer", "Love Simplicity"],
         typeSpeed: 100,
         backDelay: 900,
         // loop
         loop: true
         });*/

        var touchdragenabled = !$('body').hasClass('mw-live-edit');

        //owl carousel
        $('.owl-carousel').owlCarousel({
            autoPlay: 3000, //Set AutoPlay to 3 seconds

            items: 1,
            itemsDesktop: [1199, 1],
            itemsDesktopSmall: [979, 1],
            itemsTablet: [768, 1],
            itemsMobile: [479, 1],

            // CSS Styles
            baseClass: "owl-carousel",
            theme: "owl-theme",
            mouseDrag: touchdragenabled,
            touchDrag: touchdragenabled
        });

        $('.owl-carousel2').owlCarousel({
            autoPlay: 3000, //Set AutoPlay to 3 seconds

            items: 1,
            itemsDesktop: [1199, 1],
            itemsDesktopSmall: [979, 1],
            itemsTablet: [768, 1],
            itemsMobile: [479, 1],
            autoPlay: false,

            // CSS Styles
            baseClass: "owl-carousel",
            theme: "owl-theme",
            mouseDrag: touchdragenabled,
            touchDrag: touchdragenabled
        });

        //contact
        $('input').blur(function () {

            // check if the input has any value (if we've typed into it)
            if ($(this).val())
                $(this).addClass('used');
            else
                $(this).removeClass('used');
        });

        //pop up porfolio
        $('.portfolio-image li a').magnificPopup({
            type: 'image',
            gallery: {
                enabled: true
            }
            // other options
        });


    });


    //header
    function inits() {
        window.addEventListener('scroll', function (e) {
            var distanceY = window.pageYOffset || document.documentElement.scrollTop,
                shrinkOn = 300,
                header = document.querySelector(".for-sticky");
            if (distanceY > shrinkOn) {
                classie.add(header, "opacity-nav");
            } else {
                if (classie.has(header, "opacity-nav")) {
                    classie.remove(header, "opacity-nav");
                }
            }
        });
    }

    window.onload = inits();


})(jQuery);

$(document).ready(function () {
    $(window).bind('mw.cart.add', function (event, data) {

        if (document.getElementById('AddToCartModal') === null) {

            AddToCartModal = mw.modal({

                content: AddToCartModalContent(data.product.title),

                template: 'mw_modal_basic',

                name: "AddToCartModal",

                width: 400,

                height: 240

            });

        }

        else {

            AddToCartModal.container.innerHTML = AddToCartModalContent(data.product.title);

        }
    });
});