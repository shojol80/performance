# Bodo

Bodo is ready for use free Microweber template. It is perfect for the personal use website theme. 

Because it has a skills module and portfolio module (gallery) you can use it to show your work, skills, and portfolio. 

It contains also blog and shop functionality so you can use it also for a small online store or personal blog.  

##Features:

* Free Microweber Template
* 5 Main Pages (Home, All products, Clean page, Blog, Contact Us)
* 8 + Custom templates layouts
* 75+ Modules available
* Fully Blog functionality
* Portfolio (Gallery slider)
* Skills module
* Full eCommerce Support  (Shop Page, Products List, Product Inner, Shopping Cart, and Checkout Page)
* Pixel Perfect Design
* User-Friendly Code
* Personal Design
* Fully Responsive
* Cross Browser Support
* Easy to Customize
* Free Lifetime Updates
* Community Supports