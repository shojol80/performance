<?php

/*

type: layout

name: Default

description: Default

*/
?>

<div class="footer-top edit" field="foter-socials" rel="global">
    <ul class="socials">
        <?php if ($social_links_has_enabled == false) {
            print lnotif('Social links');
        } ?>

        <?php if ($facebook_enabled) { ?>
            <li class="facebook">
                <a href="//facebook.com/<?php print $facebook_url; ?>" target="_blank" data-hover="Facebook">Facebook</a>
            </li>
        <?php } ?>

        <?php if ($twitter_enabled) { ?>
            <li class="twitter">
                <a href="//twitter.com/<?php print $twitter_url; ?>" target="_blank" data-hover="Twitter">Twitter</a>
            </li>
        <?php } ?>

        <?php if ($googleplus_enabled) { ?>
            <li class="gplus">
                <a href="//plus.google.com/<?php print $googleplus_url; ?>" target="_blank" data-hover="Google +">Google +</a>
            </li>
        <?php } ?>

        <?php if ($pinterest_enabled) { ?>
            <li class="pinterest">
                <a href="//pinterest.com/<?php print $pinterest_url; ?>" target="_blank" data-hover="Pinterest">Pinterest</a>
            </li>
        <?php } ?>

        <?php if ($youtube_enabled) { ?>
            <li class="youtube">
                <a href="//youtube.com/<?php print $youtube_url; ?>" target="_blank" data-hover="YouTube">YouTube</a>
            </li>
        <?php } ?>

        <?php if ($instagram_enabled) { ?>
            <li class="instagram">
                <a href="https://instagram.com/<?php print $instagram_url; ?>" target="_blank" data-hover="Instagram">Instagram</a>
            </li>
        <?php } ?>

        <?php if ($linkedin_enabled) { ?>
            <li class="linkedin">
                <a href="//linkedin.com/<?php print $linkedin_url; ?>" target="_blank" data-hover="LinkedIn">LinkedIn</a>
            </li>
        <?php } ?>
    </ul>
</div>
