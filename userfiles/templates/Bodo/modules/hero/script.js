(function(){
    if(!!this.window && !this.MWParalax){
        MWParalax = {
          transform : function(){
            if(!!this._transform){
              return this._transform;
            }
            this._transform = '';
            var prefixes = ['transform', 'WebkitTransform', 'MozTransform', 'OTransform', 'msTransform'],
              test = document.createElement('div'), i = 0, l = prefixes.length;
              for( ; i < l; i++) {
                  if(test.style[prefixes[i]] !== undefined) {
                      this._transform = prefixes[i];
                      break;
                  }
              }
              return this._transform;
          },
          isInview : function(el){
              var $el = $(el);
              if($el.length === 0) { return false; }
              var dt = $(window).scrollTop(),
                  db = dt + $(window).height(),
                  et = $el.offset().top;
              return (et <= db) && !(dt > ($el.height() + et));
          },
          set:function(){
            $(".mw-paralax-image").not('.paralax-disabled').each(function(){
                  if(MWParalax.isInview(this)){
                    var pos = -($(this).parent().offset().top  - $(window).scrollTop())/2;
                    var transform = MWParalax.transform();
                    if(transform != ''){
                      $(this).css(transform, 'translateY('+pos+'px)');
                    }
                    else{
                      $(this).css('top', pos);
                    }
                  }
              });
          },
          setHeight:function(){
              $(".mw-paralax").each(function(){
                $(this).css('height', $(this.parentNode).height());
              });
          },
          inited:false,
          init:function(){

            $(document).ready(function(){
               MWParalax.setHeight();
               MWParalax.set();
            });
            $(window).bind('load resize scroll', function(e){
                MWParalax.set();
                if(e.type=='load' || e.type=='resize'){
                  MWParalax.setHeight();
                }
                if(e.type=='load' || e.type=='resize'){
                    $(".mw-paralax-image").each(function(){
                        var el = $(this),
                        h = el.parent().height(),
                        h = $(this).hasClass('paralax-disabled') ? h : h*2;
                        el.height(h);
                    });
                    if(e.type == 'load'){
                        setTimeout(function(){
                        $(".mw-paralax-image").each(function(){
                            var el = $(this),
                            h = el.parent().height(),
                            h = $(this).hasClass('paralax-disabled') ? h : h*2;
                            el.height(h);
                        });
                        }, 500);
                    }
                }
            });
          }
        }
        if(MWParalax.inited === false){
            MWParalax.inited = true
            MWParalax.init();
        }

    }
  })();