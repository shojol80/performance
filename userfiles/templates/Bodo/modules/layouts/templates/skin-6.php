<?php

/*

type: layout

name: Testimonials Section

position: 6

*/
?>

<div class="safe-mode edit" field="layout-skin-6-<?php print $params['id'] ?>" rel="module">
    <section class="testimonial-page">
        <div class="container">
            <div class="row wrap-testimonial">
                <div class="col-md-10 col-md-offset-1">
                    <module type="testimonials">
                </div>
            </div>
        </div>
        <div class="mask-testimonial"></div>
    </section>
</div>