<?php

/*

type: layout

name: Home Banner

position: 1

*/
?>


<div class="safe-mode edit" field="layout-skin-1-<?php print $params['id'] ?>" rel="module">
    <section class="home-page mw-layout-root">
        <module type="hero" file="<?php print template_url(); ?>images/hero1.jpeg" paralax="false" class="inaccessibleModule">
            <div class="container">
                <div class="row">
                    <div class="wrap-hero-content">
                        <div class="hero-content edit" rel="hero-hello" field="hero-hello">
                            <h1>Hello</h1>
                            <br>
                            <span class="typed safe-element">Welcome to Bodo</span>
                        </div>
                    </div>
                    <div class="mouse-icon margin-20">
                        <div class="scroll"></div>
                    </div>
                </div>
            </div>
    </section>
</div>