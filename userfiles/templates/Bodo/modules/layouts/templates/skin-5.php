<?php

/*

type: layout

name: Skills Section

position: 5

*/
?>

<div class="safe-mode edit" field="layout-skin-5-<?php print $params['id'] ?>" rel="module">
    <section class="white-bg" id="skill">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <h3 class="title-small">
                        <span class="safe-element">Skills</span>
                    </h3>
                    <div class=" allow-drop">
                        <p class="content-detail">
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod
                            tincidunt ut laoreet dolore magna aliquam erat volutpat.
                        </p>
                    </div>
                </div>
                <div class="col-md-9 content-right">
                    <module type="skills">
                </div>
            </div>
        </div>
    </section>
</div>