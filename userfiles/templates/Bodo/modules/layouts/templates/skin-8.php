<?php

/*

type: layout

name: About Section 2

position: 8

*/
?>
<script>
    $(document).ready(function () {
        var touchdragenabled = !$('body').hasClass('mw-live-edit');

        $('.owl-carousel', '#<?php print $params['id'] ?>').owlCarousel({
            autoPlay: 3000, //Set AutoPlay to 3 seconds

            items: 1,
            itemsDesktop: [1199, 1],
            itemsDesktopSmall: [979, 1],
            itemsTablet: [768, 1],
            itemsMobile: [479, 1],
            autoPlay: false,
            dots: true,
            // CSS Styles
            baseClass: "owl-carousel",
            theme: "owl-theme",
            mouseDrag: touchdragenabled,
            touchDrag: touchdragenabled
        });
    });
</script>
<style>
    #<?php print $params['id'] ?> .owl-theme .owl-controls .owl-page span{
        background: #000;
    }
</style>
<div class="safe-mode edit" field="layout-skin-3-<?php print $params['id'] ?>" rel="module">
    <section class="about-page">
        <div class="col-md-6 col-sm-12 col-xs-12 white-col">
            <div class="row">
                <!--OWL CAROUSEL2-->
                <div class="owl-carousel">
                    <div class="col-md-12">
                        <div class="wrap-about">
                            <div class="w-content allow-drop">
                                <p class="head-about">
                                    Design is the method of putting form and content together. Design, just as art, has multiple definitions there is no single definition. Design can be art.
                                    Design
                                    can be aesthetics. Design is so simple, that's why it is so complicated.
                                </p>

                                <h5 class="name">M. Reza</h5>
                                <img alt="signature" src="<?php print template_url(); ?>images/signature.png">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12 white-col">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12">
                                <div class="wrap-about edit" rel="home-car-2" field="home-car-2">

                                    <table class="w-content">
                                        <tr class="cloneable">
                                            <td class="title">Name</td>
                                            <td class="break">:</td>
                                            <td> M. Reza</td>
                                        </tr>
                                        <tr class="cloneable">
                                            <td class="title">Phone</td>
                                            <td class="break">:</td>
                                            <td> +62 8678 999 012</td>
                                        </tr>
                                        <tr class="cloneable">
                                            <td class="title">Email</td>
                                            <td class="break">:</td>
                                            <td> bdgpixel@gmail.com</td>
                                        </tr>
                                        <tr class="cloneable">
                                            <td class="title">Address</td>
                                            <td class="break">:</td>
                                            <td> Cipamokolan street 102 , Bandung, Indonesia</td>
                                        </tr>
                                        <tr class="cloneable">
                                            <td class="title">Skype</td>
                                            <td class="break">:</td>
                                            <td> bdgpixel</td>
                                        </tr>
                                        <tr class="cloneable">
                                            <td class="title">Dribbble</td>
                                            <td class="break">:</td>
                                            <td> Muh-Reza</td>
                                        </tr>
                                        <tr class="cloneable">
                                            <td class="title">Behance</td>
                                            <td class="break">:</td>
                                            <td> m-reza</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/.OWL CAROUSEL2 END-->
            </div>
        </div>
        <div class="col-md-6 col-xs-12 no-pad text-right">
            <div class="bg-about">
                <img src="<?php print template_url(); ?>images/reza.jpg" alt=""/>
            </div>
        </div>
    </section>
</div>