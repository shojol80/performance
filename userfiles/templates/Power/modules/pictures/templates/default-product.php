<?php

/*

type: layout

name: Default product

description: Default Product Template

*/

?>

<script>
    mw.$(document).ready(function () {
        mw.$('.pro-thumb-img-slider', '#<?php print $params['id']; ?>').slick({
            speed: 700,
            slidesToShow: 4,
            slidesToScroll: 1,
            prevArrow: '<button type="button" class="arrow-prev"><i class="fa fa-angle-left"></i></button>',
            nextArrow: '<button type="button" class="arrow-next"><i class="fa fa-angle-right"></i></button>',
            responsive: [
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 3,
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 3,
                    }
                },
            ]
        });
    });
</script>

<?php if (is_array($data)): ?>

    <?php $rand = uniqid(); ?>
    <div class="tab-content mb-10" id="mw-gallery-<?php print $rand; ?>">
        <?php $count = -1;
        foreach ($data as $key => $item): ?>
            <?php $count++;
            $active = '';
            if($key == 0){
                $active = 'active';
            }
            ?>

            <div class="pro-large-img tab-pane <?php print $active; ?>" id="pro-large-img-<?php print $key; ?>">
                <img src="<?php print thumbnail($item['filename'], 800); ?>" alt=""/>
            </div>
        <?php endforeach; ?>
    </div>
    <!-- Single Product Thumbnail Slider -->
    <div class="pro-thumb-img-slider" id="mw-gallery-thumbs-<?php print $rand; ?>">
        <?php $count = -1;
        foreach ($data as $key => $item): ?>
            <?php $count++; ?>
            <div><a href="#pro-large-img-<?php print $key; ?>" data-toggle="tab"><img src="<?php print thumbnail($item['filename'], 300); ?>" alt=""/></a></div>
        <?php endforeach; ?>
    </div>


    <div class="clearfix"></div>
    <script>gallery<?php print $rand; ?> = [
                <?php foreach($data  as $item): ?>{image: "<?php print ($item['filename']); ?>", description: "<?php print $item['title']; ?>"},
            <?php endforeach;  ?>
        ];</script>
<?php endif; ?>
