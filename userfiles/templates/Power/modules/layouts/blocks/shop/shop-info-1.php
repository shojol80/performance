<?php
$next = next_content();

$prev = prev_content();
?>

<script>mw.moduleCSS("<?php print modules_url(); ?>layouts/blocks/styles.css"); </script>

<?php if (!is_ajax()): ?>
    <div class="product-info-layout-1">
        <div class="next-previous-content">
            <?php if ($prev != false) { ?>
                <a href="<?php print content_link($prev['id']); ?>" class="prev-content tip" data-tip="#prev-tip"><i class="material-icons">arrow_backward</i></a>
                <div id="prev-tip" style="display: none">
                    <div class="next-previous-tip-content">
                        <img src="<?php print get_picture($prev['id']); ?>" alt="" width="90"/>
                        <h6><?php print $prev['title']; ?></h6>
                    </div>
                </div>
            <?php } ?>

            <a href="<?php print site_url(); ?>shop"><i class="material-icons">reorder</i></a>

            <?php if ($next != false) { ?>
                <a href="<?php print $next['url']; ?>" class="next-content tip" data-tip="#next-tip"><i class="material-icons">arrow_forward</i></a>
                <div id="next-tip" style="display: none">
                    <div class="next-previous-tip-content">
                        <img src="<?php print get_picture($next['id']); ?>" alt="" width="90"/>
                        <h6><?php print $next['title']; ?></h6>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
<?php endif; ?>

<div class="product-details section">
    <!-- Title -->
    <h1 class="edit title" field="title" rel="post">Product Name</h1>
    <!-- Price Ratting -->
    <div class="price-ratting section">
        <!-- Price -->
        <!-- <span class="price float-left">
        <span class="new"><?php print currency_format(get_product_price()); ?></span>
        <div class="product-tax-text" style="display:flex;align-items:center;font-size: 14px;">
                    <?php $tax= mw()->tax_manager->get();
                    ?>
                    <span class="edit" field="content_body" rel="post">
                        inkl. <?=intval(!empty($tax['0']['rate']) ? $tax['0']['rate'] : 0)?>% MwSt. zzgl.
                    </span>
                    <span>
                        <a href="<?php print site_url("delivery-conditions"); ?>" style="color:#23a1d1;margin-left:5px;">Versand</a>
                    </span>
                </div>
        </span> -->
        <!-- Ratting -->
        <span class="ratting float-right">
            <module type="rating" content-id="<?php print CONTENT_ID; ?>"/>
        </span>
    </div>
    <!-- Short Description -->
    <div class="short-desc section">
        <h5 class="pd-sub-title">Quick Overview</h5>
        <div class="edit" field="quick_overview" rel="post">
            <p>There are many variations of passages of Lorem Ipsum avaable, b majority have suffered alteration in some form, by injected humour, or randomised words which don't look even
                slightly believable. make an ami jani na.</p>
        </div>
    </div>

    <div class="clearfix"></div>

    <module type="shop/cart_add" template="default"/>

    <div class="clearfix"></div>

    <!--    <!-- Usefull Link -->
    <!--    <ul class="usefull-link section">-->
    <!--        <li><a href="#"><i class="pe-7s-mail"></i> Email to a Friend</a></li>-->
    <!--        <li><a href="#"><i class="pe-7s-like"></i> Wishlist</a></li>-->
    <!--        <li><a href="#"><i class="pe-7s-print"></i> print</a></li>-->
    <!--    </ul>-->

    <div class="clearfix"></div>

    <!-- Share -->
    <!-- <module type="sharer" id="product-sharer"> -->
</div>
