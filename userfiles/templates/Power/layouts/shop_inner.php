<?php
if (!is_ajax()) {
    include template_dir() . "header.php";
}
?>

    <module type="layouts" template="skin-1" />

    <!-- PAGE SECTION START -->
    <div class="page-section section pt-120 pb-120">
        <div class="container">
            <div class="row mb-40">
                <!-- Single Product Images -->
                <div class="col-md-5 col-sm-6 col-xs-12 mb-40">
                    <module type="pictures" template="default-product" rel="content"/>
                </div>
                <!-- Single Product Details -->
                <div class="col-md-7 col-sm-6 col-xs-12 mb-40">
                    <?php load_layout_block('shop/shop-info-1'); ?>
                </div>
            </div>

            <?php if (!is_ajax()): ?>
                <div class="row">
                    <!-- Nav tabs -->
                    <div class="col-xs-12">
                        <ul class="pro-info-tab-list section">
                            <li class="active"><a href="#more-info" data-toggle="tab">More info</a></li>
                            <li><a href="#data-sheet" data-toggle="tab">Data sheet</a></li>
                            <li><a href="#reviews" data-toggle="tab">Reviews</a></li>
                        </ul>
                    </div>
                    <!-- Tab panes -->
                    <div class="tab-content col-xs-12">
                        <div class="pro-info-tab tab-pane active" id="more-info">
                            <div class="edit" field="content_body" rel="post">
                                <p class="description">This text is set by default and is suitable for edit in real time. By default the drag and drop core feature will allow you to position it
                                    anywhere on the site. Get creative &amp; <strong>Make Web</strong>.</p>
                            </div>
                        </div>
                        <div class="pro-info-tab tab-pane" id="data-sheet">
                            <div class="table-responsive">
                                <div class="edit" field="product_sheets" rel="post">
                                    <table class="table-data-sheet">
                                        <tbody>
                                        <tr class="odd">
                                            <td>Compositions</td>
                                            <td>Cotton</td>
                                        </tr>
                                        <tr class="even">
                                            <td>Styles</td>
                                            <td>Casual</td>
                                        </tr>
                                        <tr class="odd">
                                            <td>Properties</td>
                                            <td>Short Sleeve</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="pro-info-tab tab-pane" id="reviews">
                            <module type="comments" content-id="<?php print CONTENT_ID; ?>"/>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <!-- PAGE SECTION END -->

    <!-- PRODUCT SECTION START -->
    <div class="product-section section pb-120">
        <div class="edit" field="related_products" rel="inherit">

            <div class="container">
                <div class="row">
                    <div class="section-title text-center col-xs-12 mb-60">
                        <h2>related products</h2>
                    </div>
                </div>
                <div class="row">
                    <module type="shop/products" template="related" related="true"/>
                </div>
            </div>
        </div>
    </div>
    <!-- PRODUCT SECTION END -->
<?php
if (!is_ajax()) {
    include template_dir() . "footer.php";
}
?>