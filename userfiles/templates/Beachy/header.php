<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns#" <?php print lang_attributes(); ?>>

<head>
    <title>{content_meta_title}</title>
    <?php if (Config::get('custom.disableGoggleIndex') == 1) : ?>
        <meta name="robots" content="noimageindex,nomediaindex" />
    <?php endif; ?>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <meta property="og:title" content="{content_meta_title}" />
    <meta name="keywords" content="{content_meta_keywords}" />
    <meta name="description" content="{content_meta_description}" />
    <meta property="og:type" content="{og_type}" />
    <meta property="og:url" content="{content_url}" />
    <meta property="og:image" content="{content_image}" />
    <meta property="og:description" content="{og_description}" />
    <meta property="og:site_name" content="{og_site_name}" />
    <script>
        mw.require('icon_selector.js');
        mw.lib.require('bootstrap4');
        mw.lib.require('bootstrap_select');

        mw.iconLoader()
            .addIconSet('fontAwesome')
            .addIconSet('iconsMindLine')
            .addIconSet('iconsMindSolid')
            .addIconSet('materialDesignIcons')
            .addIconSet('mwIcons')
            .addIconSet('materialIcons');
    </script>

    <script>
        $(document).ready(function() {
            $('.selectpicker').selectpicker();
        });
    </script>

    <?php
    //Seo data for google anaylytical
    print(template_head(true));
    $is_installed_status = Config::get('microweber.is_installed');
    if (!empty($is_installed_status)) {
        if (function_exists('basicGoogleAnalytical')) {
            basicGoogleAnalytical();
        }
    }
    //end code
    ?>
    <?php $template_url = template_url(); ?>

    <link rel="stylesheet" href="<?php print $template_url; ?>assets/css/main-style.css" type="text/css" />
    <link rel="stylesheet" href="<?php print $template_url; ?>assets/css/responsive.css" type="text/css" />





    <link href="<?php print $template_url; ?>dist/main.min.css" rel="stylesheet" />

    <?php print get_template_stylesheet(); ?>
    <link rel="stylesheet" href="<?php print $template_url; ?>assets/css/typography.css" type="text/css" />
    <?php include('template_settings.php'); ?>
    <?php
    if($GLOBALS['custom_active_category']){
        ?>
        <script src="<?php print $template_url; ?>assets/js/jquery.smartmenus.min.js" ></script>
        <link rel="stylesheet" href="<?php print $template_url; ?>assets/css/sm-core-css.min.css" />
        <link rel="stylesheet" href="<?php print $template_url; ?>assets/css/sm-simple.css" />
        <?php
    }
    ?>
</head>

<body class="<?php print helper_body_classes(); ?> <?php print $sticky_navigation; ?> ">
    <input type="hidden" id="page_id_for_layout_copy" value="<?= PAGE_ID; ?>">
    <?php
    
    $hide_header_page_id = DB::table('header_show_hides')->select('id')->where('page_id',PAGE_ID)->first();
    if($hide_header_page_id){
        $headerShowCss = "none";
    }else{
        $headerShowCss = "flex";
    }

    $shop_cat = $GLOBALS['shop_data'][0]['id'];
    $blog_data = $GLOBALS['blog_data'][0]['id'];

    $active = intval($GLOBALS['custom_active_category']);
    $showHeader = header_cate_status();

    ?>
    <div class="main">
        <div class="navigation-holder <?php print $header_style; ?><?php if ($search_bar == 'false') : ?> no_search_bar <?php endif; ?>">
            <?php if ($header_style == 'header_style_1') : ?>
                <?php include('partials/header/header_style_1.php'); ?>
            <?php elseif ($header_style == 'header_style_2') : ?>
                <?php include('partials/header/header_style_2.php'); ?>
            <?php elseif ($header_style == 'header_style_3') : ?>
                <?php include('partials/header/header_style_3.php'); ?>
            <?php elseif ($header_style == 'header_style_4') : ?>
                <?php include('partials/header/header_style_4.php'); ?>
            <?php elseif ($header_style == 'header_style_5') : ?>
                <?php include('partials/header/header_style_5.php'); ?>
            <?php elseif ($header_style == 'header_style_6') : ?>
                <?php include('partials/header/header_style_6.php'); ?>
            <?php elseif ($header_style == 'header_style_7') : ?>
                <?php include('partials/header/header_style_7.php'); ?>
            <?php elseif ($header_style == 'header_style_8') : ?>
                <?php include('partials/header/header_style_8.php'); ?>
            <?php elseif ($header_style == 'header_style_9') : ?>
                <?php include('partials/header/header_style_9.php'); ?>
            <?php else : ?>
                <?php include('partials/header/header_style_1.php'); ?>
            <?php endif; ?>
        </div>

        <?php
        $url =  url_segment();
        $last_url =  end($url);
        $last_url = (!empty($last_url)) ? $last_url : 'home';
        $pages = DB::table('content')->select('id')->where('content_type', 'page')->where('url', $last_url)->first();

        if (!isset($pages)) {
            dt_url_redirect_redirectUrl();
        }
        ?>



        <script>
            $(document).ready(function() {
                $('.header-cat ul').removeClass('nav');
                $('.header-cat ul').removeClass('nav-list');
                $('.header-cat ul li').removeClass('has-sub-menu');
                $('.header-cat ul').removeClass('active-parent');
                $('.header-cat ul li').removeClass('active-parent');
                $('.header-cat ul li a').removeClass('active-parent');

                $('.header-cat .well>ul').addClass('sm sm-simple sm-luminous');

                if ($('.header-cat .well>ul').length > 0) {
                    $('.header-cat .well>ul').smartmenus();
                }

            });
        </script>