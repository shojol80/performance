<?php include template_dir() . "header.php"; ?>

<?php
$content_data = content_data(CONTENT_ID);
$in_stock = true;
if (isset($content_data['qty']) and $content_data['qty'] != 'nolimit' and intval($content_data['qty']) == 0) {
    $in_stock = false;
}

if (isset($content_data['qty']) and $content_data['qty'] == 'nolimit') {
    $available_qty = '';
} elseif (isset($content_data['qty']) and $content_data['qty'] != 0) {
    $available_qty = $content_data['qty'];
} else {
    $available_qty = 0;
}

$item = get_content_by_id(CONTENT_ID);
$itemData = content_data($content['id']);
$itemTags = content_tags($content['id']);

if (!isset($itemData['label'])) {
    $itemData['label'] = '';
}
if (!isset($itemData['label-color'])) {
    $itemData['label-color'] = '';
}

$next = next_content();
$prev = prev_content();

?>

<div class="shop-inner-page" id="shop-content-<?php print CONTENT_ID; ?>">
    <section class="p-t-100 p-b-50 fx-particles">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <module type="breadcrumb"/>
                </div>
            </div>
            <div class="row">
                <div class="offset-md-6 col-md-6">
                    <div class="next-previous-content float-right">
                        <?php if ($next != false) { ?>
                            <a href="<?php print $next['url']; ?>"
                            class="next-content tip btn btn-outline-default" data-tip="#next-tip"><i
                                class="fas fa-chevron-left"></i></a>

                        <div id="next-tip" style="display: none">
                            <div class="next-previous-tip-content text-center">
                                <img src="<?php print get_picture($next['id']); ?>" alt=""
                                    width="90" />

                                <h6><?php print $next['title']; ?></h6>
                            </div>
                        </div>
                        <?php } ?>
                        <?php if ($prev != false) { ?>
                            <a href="<?php print content_link($prev['id']); ?>"
                            class="prev-content tip btn btn-outline-default" data-tip="#prev-tip"><i
                                class="fas fa-chevron-right"></i></a>
                        <div id="prev-tip" style="display: none">
                            <div class="next-previous-tip-content text-center">
                                <img src="<?php print get_picture($prev['id']); ?>" alt=""
                                    width="90" />
                                <h6><?php print $prev['title']; ?></h6>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <!-- start -->
            <div class="product-info mobile-view-title">
                <div class="product-info-content">
                    <div class="heading">
                        <h1 class="edit d-inline-block" field="title" rel="content"><?php print content_title(); ?></h1>
                    </div>
                </div>
            </div>
            <!-- end -->
            <div class="row">
                <div class="col-xl-12">


                    <div class="row product-holder justify-content-xl-between">
                        <div class="col-12 col-md-6 col-xl-6">
                            <module type="pictures" rel="content" template="skin-6" />
                        </div>

                        <div class="col-12 col-md-6 col-xl-6 relative product-info-wrapper">
                            <div class="product-info">
                                <div class="product-info-content">
                                    <div class="heading mobile-view-hide">
                                        <h1 class="edit d-inline-block" field="title" rel="content">
                                            <?php print content_title(); ?></h1>
                                    </div>

                                    <div class="row">
                                        <div class="col-12 col-md-12">
                                            <?php
                                                if(function_exists('category_shop_inner_show')){
                                                    echo category_shop_inner_show(content_id());
                                                }
                                            ?>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col bold">
                                            <span
                                                class="count"><?php echo get_comments('content_id=' . $content['id'] . '&count=1'); ?></span>
                                            <?php if (get_comments('content_id=' . $content['id'] . '&count=1') == 1): ?>Rezension<?php else: ?>Bewertungen<?php endif; ?>
                                        </div>

                                        <div class="col text-right bold">
                                            <span class="">Eine Rezension schreiben</span>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12">
                                            <?php if (isset($content_data['sku'])): ?>
                                            <p>
                                                <?php _e("SKU Number") ?>
                                                <span>#<?php print $content_data['sku']; ?></span>
                                            </p>
                                            <?php endif; ?>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12">
                                            <div class="description-short">
                                                <div class="edit typography-area" field="content_body" rel="content">
                                                    <p>There are many variations of passages of Lorem Ipsum available,
                                                        but the majority have suffered alteration in some form, by
                                                        injected humour, or randomised words which don't look even
                                                        slightly believable. </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row m-t-20">
                                        <div class="col-6">
                                            <h5><?php _e("Optionen") ?></h5>
                                        </div>

                                        <div class="col-6 text-right">
                                            <div class="availability">
                                                <?php if ($in_stock == true): ?>
                                                <span class="text-success"><i class="fas fa-circle"
                                                        style="font-size: 8px;"></i> <?php _e("Auf Lager") ?></span>
                                                <span
                                                    class="text-muted"><?php if ($available_qty != ''): ?>(<?php echo $available_qty; ?>)<?php endif; ?></span>
                                                <?php else: ?>
                                                <span class="text-danger"><i class="fas fa-circle"
                                                        style="font-size: 8px;"></i>
                                                    <?php _e("Nicht vorr�ttig") ?></span>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="bold">
                                        <module type="shop/cart_add" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row m-t-20">
                        <div class="col-12 text-left">
                            <h5 class="hr">Rezensionen</h5>
                        </div>
                    </div>

                    <div class="bg-silver p-30 m-b-20">
                        <module type="comments" template="skin-1" />
                    </div>


                    <div class="safe-mode nodrop m-t-40">
                        <div class="row m-t-20">
                            <div class="col-12 text-left">
                                <h5 class="hr">Verwandte Produkte</h5>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12">
                                <module type="shop/products" related="true" limit="4" hide_paging="true" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

</div>

<script>

$("#varianted_option").on("change", function(){
    var dataid = $("#varianted_option option:selected").attr('data-id');
    var selectedVariantImageURL = $("[image-id='"+dataid+"']").attr("href");
    $(".shop-inner-page .elevatezoom .elevatezoom-holder img").attr("href", selectedVariantImageURL);
    $(".shop-inner-page .elevatezoom .elevatezoom-holder img").attr("src", selectedVariantImageURL);
    $(".zoomWindow").css("background-image", "url('"+selectedVariantImageURL+"')");
});

</script>
<?php include template_dir() . "footer.php"; ?>
