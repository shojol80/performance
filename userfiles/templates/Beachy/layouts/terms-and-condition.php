<?php
/*
type: layout
name: Terms and condition
position: 3
description: Checkout
*/
?>
<?php include template_dir() . "header.php"; ?>

<section>
    <div class="agbModule-heading">
        <h2 class="edit" field="agbModule_heading" rel="content">Terms and condition</h2>
    </div>
    <div class="container agbModule-content">
        <div class="row">
            <div class="col-md-12">
                <module type="legals/agb"/>
            </div>
        </div>
    </div>
</section>

<?php include template_dir() . "footer.php"; ?>