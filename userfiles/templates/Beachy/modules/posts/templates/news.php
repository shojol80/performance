<?php

/*

type: layout

name: News

description: News

*/
?>
<?php
$blog_category_header_ignore = (array)json_decode($GLOBALS['custom_blog_category_header_ignore']) ?? [];

$showHeader = category_hide_or_show();

if(is_admin()){ ?>
    <div class="row mb-3">
        <div class="col-md-12">
            <div id="hide_blog" style="display:flex;align-items:center" class="<?php print $showHeader['button']??'';?>">
            <p style="margin-bottom:0px;margin-right:10px;">Category show in header :</p>
            <input type="checkbox" data-toggle="toggle" data-size="mini" name="blog_cat_head" id="blog_cat_head" data-on="Off" data-off="On" value="<?php (in_array(PAGE_ID,$blog_category_header_ignore)) ? print 0 : print PAGE_ID ;?>" <?php (in_array(PAGE_ID,$blog_category_header_ignore)) ? print "checked" : "" ;?>>

            </label>
        </div>
    </div>
    </div>

<?php } ?>

<script>

    $('#blog_cat_head').change(function (){

        var blog_cat = $('#blog_cat_head').val();
        var page_id = <?=PAGE_ID?>;
        console.log(blog_cat,page_id);

        $.post('<?= url('/') ?>/api/v1/not_show', { blog_cat: blog_cat,page_id: page_id }, (res) => {
            if($(this).prop('checked')){
                mw.notification.success('Category off in header');
                $('#blog_cat_head').val("0");
                $('.header-cat').hide();

            }else{
                mw.notification.success('Category on in header');
                $('#blog_cat_head').val('<?=PAGE_ID?>');
                $('.header-cat').attr('style','display: block !important;');

            }
        });


    });
</script>

    <div class="row">
        <div class="col-xl-10 mx-auto">
            <div class="row new-world-news">
            <?php
            $all_data = rss_data_get($data);
            $data = $all_data['data'];
            $rssLink = $all_data['rssLink'];
            $rssoption = $all_data['rssoption'];
            ?>
                <?php if (!empty($data)): ?>
                    <?php foreach ($data as $key => $item): ?>
                        <?php $item = rss_url_image_link($item, $rssoption);?>
                        <?php $itemData = content_data($item['id']); ?>

                        <?php if ($key == 0): ?>
                            <div class="col-12 post-big" itemscope itemtype="<?php print $schema_org_item_type_tag ?>">
                                <div class="post-holder">
                                    <div class="thumbnail justify-content-bottom align-items-end d-flex flex-cloumns" style="background-image: url('<?php print thumbnail($item['image'], 1135, 540, true); ?>');">
                                        <div>
                                            <?php if (!isset($show_fields) or $show_fields == false or in_array('created_at', $show_fields)): ?>
                                                <small><?php echo date('d M Y', strtotime($item['created_at'])); ?></small>
                                            <?php endif; ?>

                                            <?php if (!isset($show_fields) or $show_fields == false or in_array('title', $show_fields)): ?>
                                                <div class="">
                                                    <a href="<?php print $item['link'] ?>"><h3 class="post-title-news"><?php print $item['title'] ?></h3></a>
                                                </div>
                                            <?php endif; ?>

                                            <?php if (!isset($show_fields) or $show_fields == false or in_array('description', $show_fields)): ?>
                                                <?php if (intval($rssoption) == 1 || intval($rssoption) == 2) { ?>
                                                    <p class="blog-desc"><?php print limitTextWords($item['description'],15,true,true) ?></p>
                                                <?php }else{ ?>
                                                    <p class="blog-desc"><?php print $item['description'] ?></p>
                                                <?php }  ?>
                                            <?php endif; ?>

                                            <a href="<?php print $item['link'] ?>" class="btn btn-primary m-t-10"><span><?php (get_option('data-read-more-text',$params['id']) != NULL) ? print (get_option('data-read-more-text',$params['id'])) : print ("Bericht lesen");?></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php else: ?>
                            <div class="col-md-6" itemscope itemtype="<?php print $schema_org_item_type_tag ?>">
                                <div class="post-holder">
                                    <a href="<?php print $item['link'] ?>" itemprop="url">
                                        <div class="thumbnail-holder">
                                            <?php if (!isset($show_fields) or $show_fields == false or in_array('thumbnail', $show_fields)): ?>
                                                <div class="thumbnail">
                                                    <img src="<?php print thumbnail($item['image'], 535, 285, true); ?>"/>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </a>

                                    <?php if (!isset($show_fields) or $show_fields == false or in_array('created_at', $show_fields)): ?>
                                        <small><?php echo date('d M Y', strtotime($item['created_at'])); ?></small>
                                    <?php endif; ?>
                                    <?php if (!isset($show_fields) or $show_fields == false or in_array('title', $show_fields)): ?>
                                        <div class="blog-post-title">
                                        <a href="<?php print $item['link'] ?>"><h3 class="post-title"><?php print $item['title'] ?></h3></a>
                                        </div>
                                    <?php endif; ?>

                                    <?php if (!isset($show_fields) or $show_fields == false or in_array('description', $show_fields)): ?>
                                        <?php if (intval($rssoption) == 1 || intval($rssoption) == 2) { ?>
                                                    <p class="blog-desc-news"><?php print limitTextWords($item['description'],15,true,true) ?></p>
                                                <?php }else{ ?>
                                                    <p class="blog-desc-news"><?php print $item['description'] ?></p>
                                                <?php }  ?>
                                    <?php endif; ?>
                                    <a href="<?php print $item['link'] ?>" class="button-8"><span><?php (get_option('data-read-more-text',$params['id']) != NULL) ? print (get_option('data-read-more-text',$params['id'])) : print ("Bericht lesen");?></span></a>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
<input type="hidden" name="blog_category_status" id="blog_<?=PAGE_ID?>" data-<?=PAGE_ID?>="blog" value="<?=PAGE_ID?>">
    <?php
if (intval($rssoption) != 1 && intval($rssoption) != 2) {
    ?>

<?php if (isset($pages_count) and $pages_count > 1 and isset($paging_param)): ?>
    <module type="pagination" pages_count="<?php echo $pages_count; ?>" paging_param="<?php echo $paging_param; ?>"/>
<?php endif;
}

print rss_pagination($rssoption, $paging_param );
?>
