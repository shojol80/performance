<?php

/*

type: layout

name: Posts Slider

description: Posts Slider

*/
?>

<?php include('slick_options.php'); ?>
<?php
$blog_category_header_ignore = (array)json_decode($GLOBALS['custom_blog_category_header_ignore']) ?? [];

$showHeader = category_hide_or_show();

if(is_admin()){ ?>
    <div class="row mb-3">
        <div class="col-md-12">
            <div id="hide_blog" style="display:flex;align-items:center" class="<?php print $showHeader['button']??'';?>">
            <p style="margin-bottom:0px;margin-right:10px;">Category show in header :</p>
            <input type="checkbox" data-toggle="toggle" data-size="mini" name="blog_cat_head" id="blog_cat_head" data-on="Off" data-off="On" value="<?php (in_array(PAGE_ID,$blog_category_header_ignore)) ? print 0 : print PAGE_ID ;?>" <?php (in_array(PAGE_ID,$blog_category_header_ignore)) ? print "checked" : "" ;?>>

            </label>
        </div>
    </div>
    </div>

<?php } ?>

<script>

    $('#blog_cat_head').change(function (){

        var blog_cat = $('#blog_cat_head').val();
        var page_id = <?=PAGE_ID?>;
        console.log(blog_cat,page_id);

        $.post('<?= url('/') ?>/api/v1/not_show', { blog_cat: blog_cat,page_id: page_id }, (res) => {
            if($(this).prop('checked')){
                mw.notification.success('Category off in header');
                $('#blog_cat_head').val("0");
                $('.header-cat').hide();

            }else{
                mw.notification.success('Category on in header');
                $('#blog_cat_head').val('<?=PAGE_ID?>');
                $('.header-cat').attr('style','display: block !important;');

            }
        });


    });
</script>
<div class="row blog-posts slickslider">
<?php
    $all_data = rss_data_get($data);
    $data = $all_data['data'];
    $rssLink = $all_data['rssLink'];
    $rssoption = $all_data['rssoption'];
    ?>
    <?php if (!empty($data)): ?>
        <?php foreach ($data as $item):
            $item = rss_url_image_link($item, $rssoption);?>
            <div class="row m-0 post">
                <div class="col-12 d-flex flex-column h-100" itemscope itemtype="<?php print $schema_org_item_type_tag ?>">
                    <div class="description">
                        <?php if (!isset($show_fields) or $show_fields == false or in_array('title', $show_fields)): ?>
                            <h3 class=" m-b-20"><?php print $item['title'] ?></h3>
                        <?php endif; ?>

                        <p class="date m-b-10"><?php print $item['created_at'] ?></p>

                        <?php if (!isset($show_fields) or $show_fields == false or in_array('description', $show_fields)): ?>
                            <?php if (intval($rssoption) == 1 || intval($rssoption) == 2) { ?>
                                        <p><?php print limitTextWords($item['description'],15,true,true) ?></p>
                                    <?php }else{ ?>
                                        <p><?php print $item['description'] ?></p>
                                    <?php }  ?>
                        <?php endif; ?>
                    </div>

                    <div class="m-t-auto">
                        <a href="<?php print $item['link'] ?>" class="button-8"><span><?php (get_option('data-read-more-text',$params['id']) != NULL) ? print (get_option('data-read-more-text',$params['id'])) : print ("Bericht lesen");?></span></a>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>
</div>
<input type="hidden" name="blog_category_status" id="blog_<?=PAGE_ID?>" data-<?=PAGE_ID?>="blog" value="<?=PAGE_ID?>">
<?php
if (intval($rssoption) != 1 && intval($rssoption) != 2) {
    ?>

<?php if (isset($pages_count) and $pages_count > 1 and isset($paging_param)): ?>
    <module type="pagination" pages_count="<?php echo $pages_count; ?>" paging_param="<?php echo $paging_param; ?>" />
<?php endif;
}

print rss_pagination($rssoption, $paging_param );
?>
