<?php

/*

type: layout

name: News 3

description: News 3

*/
?>
<?php
$blog_category_header_ignore = (array)json_decode($GLOBALS['custom_blog_category_header_ignore']) ?? [];

$showHeader = category_hide_or_show();

if(is_admin()){ ?>
    <div class="row mb-3">
        <div class="col-md-12">
            <div id="hide_blog" style="display:flex;align-items:center" class="<?php print $showHeader['button']??'';?>">
            <p style="margin-bottom:0px;margin-right:10px;">Category show in header :</p>
            <input type="checkbox" data-toggle="toggle" data-size="mini" name="blog_cat_head" id="blog_cat_head" data-on="Off" data-off="On" value="<?php (in_array(PAGE_ID,$blog_category_header_ignore)) ? print 0 : print PAGE_ID ;?>" <?php (in_array(PAGE_ID,$blog_category_header_ignore)) ? print "checked" : "" ;?>>

            </label>
        </div>
    </div>
    </div>

<?php } ?>

<script>

    $('#blog_cat_head').change(function (){

        var blog_cat = $('#blog_cat_head').val();
        var page_id = <?=PAGE_ID?>;
        console.log(blog_cat,page_id);

        $.post('<?= url('/') ?>/api/v1/not_show', { blog_cat: blog_cat,page_id: page_id }, (res) => {
            if($(this).prop('checked')){
                mw.notification.success('Category off in header');
                $('#blog_cat_head').val("0");
                $('.header-cat').hide();

            }else{
                mw.notification.success('Category on in header');
                $('#blog_cat_head').val('<?=PAGE_ID?>');
                $('.header-cat').attr('style','display: block !important;');

            }
        });


    });
</script>

<div class="row">
    <div class="col-xl-12 mx-auto">
        <div class="row new-world-news-3">
        <?php
        $all_data = rss_data_get($data);
        $data = $all_data['data'];
        $rssLink = $all_data['rssLink'];
        $rssoption = $all_data['rssoption'];
        ?>
            <?php if (!empty($data)): ?>
                <div class="col-xl-7 m-b-30">
                    <div class="row">
                        <?php foreach ($data as $key => $item):
                            $item = rss_url_image_link($item, $rssoption);?>
                            <?php $itemData = content_data($item['id']); ?>
                            <?php if ($key == 0): ?>

                                <div class="col-12">
                                    <div class="post-big">
                                        <div class="post-holder">
                                            <div class="thumbnail justify-content-bottom align-items-end d-flex flex-cloumns" style="background-image: url('<?php print thumbnail($item['image'], 790, 390, true); ?>');">
                                                <div>
                                                    <?php if (!isset($show_fields) or $show_fields == false or in_array('created_at', $show_fields)): ?>
                                                        <small><?php echo date('d M Y', strtotime($item['created_at'])); ?></small>
                                                    <?php endif; ?>

                                                    <?php if (!isset($show_fields) or $show_fields == false or in_array('title', $show_fields)): ?>
                                                        <a href="<?php print $item['link'] ?>">
                                                            <h3 class="post-title-news"><?php print $item['title'] ?></h3>
                                                        </a>
                                                    <?php endif; ?>
                                                    <a href="<?php print $item['link'] ?>" class="btn btn-primary m-t-10"><span><?php (get_option('data-read-more-text',$params['id']) != NULL) ? print (get_option('data-read-more-text',$params['id'])) : print ("Bericht lesen");?></span></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <?php endif; ?>

                            <?php if ($key == 1 OR $key == 2): ?>
                                <div class="col-6 d-none d-xl-block">
                                    <div class="row m-b-30">
                                        <div class="col-xl-5">
                                            <div class="post-holder">
                                                <a href="<?php print $item['link'] ?>">
                                                    <img src="<?php print thumbnail($item['image'], 300, 225, true); ?>" alt=""/>
                                                </a>
                                            </div>
                                        </div>

                                        <div class="col-xl-7">
                                            <div class="post-holder">
                                                 <?php if (!isset($show_fields) or $show_fields == false or in_array('created_at', $show_fields)): ?>
                                                    <small><?php echo date('d M Y', strtotime($item['created_at'])); ?></small>
                                                <?php endif; ?>
                                                <?php if (!isset($show_fields) or $show_fields == false or in_array('title', $show_fields)): ?>
                                                    <a href="<?php print $item['link'] ?>">
                                                        <h4 class="post-title-news"><?php print $item['title'] ?></h4>
                                                    </a>
                                                <?php endif; ?>

                                                <?php if (!isset($show_fields) or $show_fields == false or in_array('description', $show_fields)):
                                                    if (intval($rssoption) == 1 || intval($rssoption) == 2) { ?>
                                                        <p class="d-none"><?php print limitTextWords($item['description'],15,true,true) ?></p>
                                                    <?php }else{ ?>
                                                        <p class="d-none"><?php print character_limiter($item['description'], 50) ?></p>
                                                    <?php }  ?>
                                                <?php endif; ?>

                                                <a href="<?php print $item['link'] ?>" class="button-8"><span><?php (get_option('data-read-more-text',$params['id']) != NULL) ? print (get_option('data-read-more-text',$params['id'])) : print ("Bericht lesen");?></span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                </div>

                <div class="col-xl-5 d-none d-xl-block">
                    <?php foreach ($data as $key => $item):
                        $item = rss_url_image_link($item, $rssoption); ?>
                        <?php $itemData = content_data($item['id']); ?>
                        <?php if ($key == 3): ?>
                            <div class="row">
                                <div class="col-12">
                                    <div class="post-big high">
                                        <div class="post-holder">
                                            <div class="thumbnail justify-content-bottom align-items-end d-flex flex-cloumns" style="background-image: url('<?php print thumbnail($item['image'], 565, 643, true); ?>');">
                                                <div>
                                                    <?php if (!isset($show_fields) or $show_fields == false or in_array('created_at', $show_fields)): ?>
                                                        <small><?php echo date('d M Y', strtotime($item['created_at'])); ?></small>
                                                    <?php endif; ?>

                                                    <?php if (!isset($show_fields) or $show_fields == false or in_array('title', $show_fields)): ?>
                                                        <a href="<?php print $item['link'] ?>">
                                                            <h3 class="post-title"><?php print $item['title'] ?></h3>
                                                        </a>
                                                    <?php endif; ?>
                                                    <a href="<?php print $item['link'] ?>" class="btn btn-primary m-t-10"><span><?php (get_option('data-read-more-text',$params['id']) != NULL) ? print (get_option('data-read-more-text',$params['id'])) : print ("Bericht lesen");?></span></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php endif; ?>
                    <?php endforeach; ?>

                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
<input type="hidden" name="blog_category_status" id="blog_<?=PAGE_ID?>" data-<?=PAGE_ID?>="blog" value="<?=PAGE_ID?>">
<?php
if (intval($rssoption) != 1 && intval($rssoption) != 2) {
    ?>
<?php }

print rss_pagination($rssoption, $paging_param );
?>
