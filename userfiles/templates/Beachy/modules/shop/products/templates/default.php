<?php

/*

type: layout

name: Default

description: Default

*/
?>

<?php $template_url = template_url(); ?>
<script type="text/javascript" src="<?php print $template_url; ?>assets/plugins/lazyload/jquery.lazy.min.js"></script>
<script type="text/javascript" src="<?php print $template_url; ?>assets/plugins/lazyload//jquery.lazy.plugins.min.js"></script>
<?php


$is_logged = is_logged();
if ($is_logged) {

?>
    <link href="<?php print $template_url; ?>assets/css/select2.min.css" rel="stylesheet" />

    <script src="<?php print $template_url; ?>assets/js/select2.min.js"></script>


    <script>
        $(document).ready(function() {
            $('.js-example-basic-multiple').select2();
        });
    </script>
<?php
} ?>


<style>
    .dt_t_countdown_data {
        border: 0 !important;
        border-radius: 5px;
    }

    .theme-countdown {
        background: #fff;
        padding: 10px;
        border-radius: 5px;
    }

    .dt-old-price {
        position: relative;
        text-align: center;
    }

    .dt-old-price p {
        font-size: 15px;
        color: #f00;
        text-decoration: line-through;
    }

    /* Countdown Style 1 Here */
    .dt-cdown-box {
        position: relative;
        margin: 10px auto;
    }

    .dt-cdown-box .dt_t_countdown_data {
        border: 1px solid #e8e8e8;
        position: relative;
        text-align: center;
    }

    .dt-cdown-box .dt_t_countdown_data>div {
        position: relative;
        display: inline-block;
    }


    .dt-countdown-style-1 {
        width: 100%;
        display: flex;
        justify-content: center;
    }

    .dt-countdown-style-1 .dt-cdown-box .dt_t_countdown_data {
        display: flex;
        padding: 10px 0px !important;
    }

    .dt-countdown-style-1 .dt-cdown-box .dt_t_countdown_data .days-wrapper {
        display: flex;
        flex-direction: column;
        min-width: 20%;
        align-items: center;
    }

    .dt-countdown-style-1 .dt-cdown-box .dt_t_countdown_data>div p {
        position: relative;
        font-size: 12px;
        border: 3px solid #3a4b5d;
        color: #3a4b5d;
        height: 40px;
        width: 40px;
        border-radius: 50%;
        font-weight: 700;
        display: inline-block;
        vertical-align: middle;
        padding-top: 8px;
    }

    .dt-countdown-style-1 .dt-cdown-box .dt_t_countdown_data>div {
        margin-right: 3px;
        margin-top: 5px;
        margin-bottom: 5px;
    }

    .dt-countdown-style-1 .dt-cdown-box .dt_t_countdown_data>div span.su {
        font-size: 12px;
        font-weight: 700;
        color: #3a4b5d;
        display: inline-block;
        margin-top: 5px;
    }

    .dt-countdown-style-1 .dt-cdown-box .dt_t_countdown_data>div:last-child {
        margin-right: 0px;
    }



    /* Countdown Style 2 Here */
    .dt-countdown-style-2 {
        width: 100%;
        display: flex;
        justify-content: center;
    }


    .dt-countdown-style-2 .dt-cdown-box .dt_t_countdown_data {
        display: flex;
        padding: 10px 5px !important;
    }

    .dt-countdown-style-2 .dt-cdown-box .dt_t_countdown_data .days-wrapper {
        display: flex;
        flex-direction: column;
        min-width: 20%;
        align-items: center;
    }

    .dt-countdown-style-2 .dt-cdown-box .dt_t_countdown_data>div p {
        position: relative;
        font-size: 12px;
        border: 3px solid #bfbfbf;
        border-left-width: 1px;
        border-right-width: 1px;
        color: #353535;
        height: 40px;
        width: 35px;
        border-radius: 0;
        font-weight: 700;
        display: inline-block;
        vertical-align: middle;
        padding-top: 8px;
    }

    .dt-countdown-style-2 .dt-cdown-box .dt_t_countdown_data>div {
        margin-right: 2px;
        margin-top: 5px;
        margin-bottom: 5px;
    }

    .dt-countdown-style-2 .dt-cdown-box .dt_t_countdown_data>div span.su {
        font-size: 12px;
        font-weight: 700;
        color: #3a4b5d;
        display: inline-block;
        margin-top: 5px;
    }

    .dt-countdown-style-2 .dt-cdown-box .dt_t_countdown_data>div:last-child {
        margin-right: 0px;
    }


    /* DT Countdown Style 3 */
    .dt-countdown-style-3 {
        position: relative;
        width: 100%;
        display: flex;
        justify-content: center;
    }

    .dt-countdown-style-3 .dt-cdown-box .dt_t_countdown_data {
        display: flex;
        flex-wrap: wrap;
    }

    .dt-countdown-style-3 .dt-cdown-box .dt_t_countdown_data>div {
        position: relative;
        margin: 5px;
        display: flex;
        flex-direction: column;
        align-items: center;
    }

    .dt-countdown-style-3 .dt-cdown-box .dt_t_countdown_data>div p {
        background-color: #25374b;
        color: #fff;
        width: 35px;
        height: 40px;
        font-size: 18px;
        padding-top: 8px;
        text-align: center;
    }

    .dt-countdown-style-3 .dt-cdown-box .dt_t_countdown_data>div span.su {
        color: #25374b;
        font-size: 12px;
        text-transform: uppercase;
        font-weight: 500;
        margin-top: 5px;
    }



    /* DT Countdown Style 4 */
    .dt-countdown-style-4 {
        position: relative;
        width: 100%;
        display: flex;
        justify-content: center;
    }

    .dt-countdown-style-4 .dt-cdown-box {
        background-color: #252014;
    }

    .dt-countdown-style-4 .dt-cdown-box .dt_t_countdown_data {
        border-radius: 0px;
        padding: 10px 0 !important;
        display: flex;
        flex-wrap: wrap;
    }

    .dt-countdown-style-4 .dt-cdown-box .dt_t_countdown_data>div {
        position: relative;
        margin: 5px;
        display: flex;
        flex-direction: column;
        align-items: center;
    }

    .dt-countdown-style-4 .dt-cdown-box .dt_t_countdown_data>div p {
        color: #fff;
        width: 30px;
        height: 50px;
        font-size: 20px;
    }

    .dt-countdown-style-4 .dt-cdown-box .dt_t_countdown_data>div span.su {
        color: #fff;
        font-size: 12px;
        text-transform: uppercase;
        font-weight: 500;
        display: block;
        margin-top: -16px;
    }

    .dt-countdown-style-4 .dt-cdown-box .dt_t_countdown_data>div span.su-res {
        color: #fff;
    }

    /* DT Countdown Style 5 */
    .dt-countdown-style-5 {
        position: relative;
        width: 100%;
        display: flex;
        justify-content: center;
    }

    .dt-countdown-style-5 .dt-cdown-box {
        background-color: #252014;
    }

    .dt-countdown-style-5 .dt-cdown-box .dt_t_countdown_data {
        padding: 10px !important;
        display: flex;
        flex-wrap: wrap;
        border-radius: 0px;
        justify-content: space-between;
    }

    .dt-countdown-style-5 .dt-cdown-box .dt_t_countdown_data>div {
        display: flex;
        align-items: center;
        flex-direction: column;
        position: relative;
        margin: 5px;
    }

    .dt-countdown-style-5 .dt-cdown-box .dt_t_countdown_data>div p {
        color: #ffd565;
        width: 30px;
        height: 70px;
        font-size: 20px;
    }

    .dt-countdown-style-5 .dt-cdown-box .dt_t_countdown_data>div span.su {
        color: #ffd565;
        font-size: 14px;
        font-weight: 500;
        display: block;
        margin-top: -16px;
    }

    .dt-countdown-style-5 .dt-cdown-box .dt_t_countdown_data>div span.su-res {
        color: #ffd565;
    }

    .dt-countdown-style-5 .dt-cdown-box .dt_t_countdown_data>div:after {
        position: absolute;
        content: ':';
        color: #ffd565;
        top: 0px;
        right: -3px;
        font-size: 20px;
    }

    .dt-countdown-style-5 .dt-cdown-box .dt_t_countdown_data>div:last-child:after {
        display: none;
    }




    /* DT Countdown Style 6 */
    .dt-countdown-style-6 {
        width: 100%;
        display: flex;
        justify-content: center;
        position: relative;
    }

    .dt-countdown-style-6 .dt-cdown-box {
        background-color: #302010;
    }

    .dt-countdown-style-6 .dt-cdown-box .dt_t_countdown_data {
        border-radius: 0px;
        padding: 0px !important;
        display: flex;
        flex-wrap: wrap;
    }

    .dt-countdown-style-6 .dt-cdown-box .dt_t_countdown_data>div {
        position: relative;
        margin: 0px;
        text-align: center;
        padding: 10px 8px;
        border-right: 1px dotted #fff;
    }

    .dt-countdown-style-6 .dt-cdown-box .dt_t_countdown_data>div p {
        color: #fff;
        height: 50px;
        font-size: 18px;
    }

    .dt-countdown-style-6 .dt-cdown-box .dt_t_countdown_data>div span.su {
        color: #fff;
        font-size: 12px;
        text-transform: uppercase;
        font-weight: 500;
        display: block;
    }

    .dt-countdown-style-6 .dt-cdown-box .dt_t_countdown_data>div:last-child {
        border-right: none;
    }

    .dt-countdown-style-6 .dt-cdown-box .dt_t_countdown_data>div span.su-res {
        color: #fff;
    }


    /* DT Countdown Style 7 */

    .dt-countdown-style-7 {
        width: 100%;
        display: flex;
        justify-content: center;
    }

    .dt-countdown-style-7 .dt-cdown-box .dt_t_countdown_data {
        display: flex;
        flex-wrap: wrap;
        padding: 10px 5px !important;
    }

    .dt-countdown-style-7 .dt-cdown-box .dt_t_countdown_data>div p {
        position: relative;
        font-size: 20px;
        border: 3px solid #bfbfbf;
        border-top-style: double;
        border-bottom-style: double;
        border-left-width: 1px;
        border-right-width: 1px;
        color: #353535;
        border-radius: 0;
        font-weight: 700;
        display: inline-block;
        vertical-align: middle;
        padding: 8px;
    }

    .dt-countdown-style-7 .dt-cdown-box .dt_t_countdown_data>div {
        margin: 3px;
    }

    .dt-countdown-style-7 .dt-cdown-box .dt_t_countdown_data>div span.su {
        font-size: 14px;
        font-weight: 500;
        color: #3a4b5d;
        display: block;
        text-transform: uppercase;
        margin-top: 5px;
    }

    .dt-countdown-style-7 .dt-cdown-box .dt_t_countdown_data>div span.su-res {
        margin-top: 5px;
    }

    .dt-countdown-style-7 .dt-cdown-box .dt_t_countdown_data>div:last-child {
        margin-right: 0px;
    }



    /* DT Countdown Style 8 */

    .dt-countdown-style-8 {
        width: 100%;
        display: flex;
        justify-content: center;
    }

    .dt-countdown-style-8 .dt-cdown-box .dt_t_countdown_data {
        display: flex;
        flex-wrap: wrap;
        padding: 10px 5px !important;
    }


    .dt-countdown-style-8 .dt-cdown-box .dt_t_countdown_data>div p {
        position: relative;
        font-size: 20px;
        border: 3px solid #bfbfbf;
        border-top-style: double;
        border-bottom-style: double;
        border-left-width: 1px;
        border-right-width: 1px;
        color: #fff;
        height: 40px;
        width: 40px;
        font-weight: 700;
        display: inline-block;
        vertical-align: middle;
        background-color: #000;
        border-radius: 5px;
        padding-top: 5px;
    }

    .dt-countdown-style-8 .dt-cdown-box .dt_t_countdown_data>div {
        margin: 3px;
    }

    .dt-countdown-style-8 .dt-cdown-box .dt_t_countdown_data>div span.su {
        font-size: 14px;
        font-weight: 500;
        color: #3a4b5d;
        display: block;
        text-transform: uppercase;
        margin-top: 5px;
    }

    .dt-countdown-style-8 .dt-cdown-box .dt_t_countdown_data>div span.su-res {
        margin-top: 5px;
    }

    .dt-countdown-style-8 .dt-cdown-box .dt_t_countdown_data>div:last-child {
        margin-right: 0px;
    }

    .dt-cdown-box .dt_t_countdown_data>div span.su-res {
        display: none !important;
    }

    @media screen and (max-width: 1200px) {
        .dt-cdown-box .dt_t_countdown_data>div span.su {
            display: none !important;
        }

        .dt-cdown-box .dt_t_countdown_data>div span.su-res {
            display: block !important;
        }
    }

    @media screen and (max-width: 767px) {
        .dt-cdown-box .dt_t_countdown_data>div span.su {
            display: block !important;
        }

        .dt-cdown-box .dt_t_countdown_data>div span.su-res {
            display: none !important;
        }
    }
</style>
<?php
//$tn = $tn_size;
//if (!isset($tn[0]) or ($tn[0]) == 150) {
//    $tn[0] = 350;
//}
//if (!isset($tn[1])) {
//    $tn[1] = $tn[0];
//}
?>
<?php if (isset($_GET['wishlist_id'])) {
    $ids = array(0);
    $pro_ids = DB::table('wishlist_session_products')->where('wishlist_id', '=', $_GET['wishlist_id'])->pluck('product_id')->toArray();
    $data = collect($data)->whereIn('id', $pro_ids)->toArray();
?>
    <module type="shop/products" template="skin-1" hide_paging="true" limit="100" />
<?php
}
if (isset($_GET['slug'])) {
?>
    <module type="shop/products" template="skin-1" hide_paging="true" limit="100" />
<?php
}




if (is_admin()) {
    $shop_category_header_ignore = (array)json_decode($GLOBALS['custom_shop_category_header_ignore']) ?? [];
    $showHeader = category_hide_or_show();
    $check = in_array(PAGE_ID, $shop_category_header_ignore);
?>
    <div id="hide_shop" style="display:flex;align-items:center" class="<?php print $showHeader['button'] ?? ''; ?>">
        <p style="margin-bottom:0px;margin-right:10px;">Category show in header :</p>
        <input type="checkbox" data-toggle="toggle" data-size="mini" name="shop" id="shop_cat" data-on="Off" data-off="On" value="<?php ($check) ? print 0 : print PAGE_ID; ?>" <?php ($check) ? print "checked" : ""; ?>>

    </div>
<?php } ?>


<script>
    $('#shop_cat').change(function() {
        var shop_id = $('#shop_cat').val();
        var page_id = <?= PAGE_ID ?>


        $.post('<?= url('/') ?>/api/v1/not_show', {
            shop_cat: shop_id,
            page_id: page_id
        }, (res) => {
            if ($(this).prop('checked')) {
                mw.notification.success('Category off in header');
                $('#shop_cat').val('0');
                $('.header-cat').hide();
            } else if ($(this).prop('checked', false)) {
                mw.notification.success('Category on in header');
                $('#shop_cat').val('<?= PAGE_ID ?>');
                $('.header-cat').attr('style', 'display: block !important;');

            }
        });

    });
</script>
<?php

if (!empty($data) && !isset($_GET['slug']) && !isset($_GET['wishlist_id'])) :
    $all_products=collect($data);

    $items_id = $all_products->pluck('id');

    $cont=DB::table('content_data')->select('field_value','field_name','rel_id')->whereIn('rel_id',$items_id)->get();

    $all_cat=contents_categories($items_id);

    $taxrate = ($is_logged) ? (int)taxRateCountry(user_id()) : taxRate();

    $all_cdata=$cont->groupBy('rel_id');

    $all_real_con=array();
    foreach($all_cdata as $key => $d){

        foreach($d as $rel_key => $value){
            $fvalue=$value->field_value;
            $fkey=$value->field_name;
            $final[$fkey]=$fvalue;
        }
        $all_content_data[$key]=$final;
    }
    ?>
    <div class="row shop-products">
        <?php foreach ($data as $item) :

            $content_data = $all_content_data[$item['id']];
            $in_stock = true;


            if (isset($content_data['qty']) and $content_data['qty'] != 'nolimit' and intval($content_data['qty']) == 0) {
                $in_stock = false;
            }
            $categories = $all_cat[$item['id']];

            $itemData = $content_data;
            $itemTags = content_tags($item['id']);

            if (!isset($itemData['label'])) {
                $itemData['label'] = '';
            }
            if (!isset($itemData['label-color'])) {
                $itemData['label-color'] = '';
            }

            $itemCats = '';
            if ($categories) {
                foreach ($categories as $category) {
                    $itemCats .= $category['title'] . ', ';
                }
            }
            ?>

            <div class="col-12 col-md-6 col-lg-4 col-xl-4 item-<?php print $item['id'] ?>" data-masonry-filter="<?php print $itemCats; ?>" itemscope itemtype="">
                <div class="product">
                    <?php   if (is_array($item['prices'])) :
                                foreach ($item['prices'] as $k => $v) : ?>
                                    <input type="hidden" name="price" value="<?php print $v ?>" />
                                    <input type="hidden" name="content_id" value="<?php print $item['id'] ?>" />
                                    <?php break;
                                endforeach;
                            endif;
                            if ($itemData['label'] != '') : ?>
                                <div class="product-label" style="background-color: <?php echo $itemData['label-color']; ?>;"><?php echo $itemData['label']; ?></div>
                            <?php endif;
                            if ($show_fields == false or in_array('thumbnail', $show_fields)) : ?>

                        <div class="image lazy" data-src="<?php print $item['tn_image']; ?>">
                            <a href="<?php print $item['link'] ?>" class="d-flex h-100 w-100"></a>
                            <?php /*<div class="hover">
                                <?php if ($show_fields == false or ($show_fields != false and in_array('add_to_cart', $show_fields))): ?>
                                    <a href="javascript:;" onclick="mw.cart.add('.shop-products .item-<?php print $item['id'] ?>');" class="btn btn-custom-one"><i class="material-icons">shopping_cart</i></a>
                                <?php endif; ?>
                                <?php if ($show_fields == false or ($show_fields != false and in_array('read_more', $show_fields))): ?>
                                    <a href="<?php print $item['link'] ?>" class="btn btn-default"><i class="material-icons">remove_red_eye</i></a>
                                <?php endif; ?>
                            </div>*/ ?>
                        </div>

                    <?php endif; ?>


                    <div class="m-t-20">
                        <?php if ($show_fields == false or in_array('title', $show_fields)) : ?>
                            <a href="<?php print $item['link'] ?>">
                                <div class="heading-holder">
                                    <h5><?php print $item['title'] ?></h5>
                                </div>
                            </a>
                        <?php endif; ?>


                        <div class="row">
                            <div class="col-12 price-holder">
                                <div class="product-price">
                                    <!-- //offer price-->
                                    <?php
                                    $offer = \MicroweberPackages\Offer\Models\Offer::getByProductId($item['id']);
                                    if (isset($offer['price']['offer_price'])) {
                                        $val4 = $item['original_price'];
                                        $val4 = normalPrice($val4);
                                        $val4 = $val4 + taxPrice($val4);

                                    ?>
                                        <div class="dt-old-price">
                                            <p><?php print currency_format(roundPrice($val4)); ?></p>
                                        </div>

                                    <?php
                                    }
                                    if ($show_fields == false or in_array('price', $show_fields)) :
                                        if (isset($item['prices']) and is_array($item['prices'])) :

                                            $vals2 = array_values($item['prices']);
                                            $val1 = array_shift($vals2);
                                            $val1 = normalPrice($val1);
                                            $val1 = $val1 + taxPrice($val1);

                                            ?>
                                            <p>
                                                <span class="price"><?php print currency_format(roundPrice($val1)); ?></span>
                                            </p>
                                        <?php endif;
                                        endif; ?>
                                </div>
                                <div class="product-tax-text" style="">
                                    <?php //$tax = mw()->tax_manager->get();
                                    ?>
                                    <span class="edit">
                                        inkl. <?php print $taxrate; ?>% MwSt.
                                    </span>
                                    <span data-toggle="modal" data-target="#termModal" style="margin-left:5px;display:inline-block;">
                                        zzgl. Versand
                                    </span>
                                </div>
                            </div>
                            <div class="col-12">
                                <?php if ($is_logged) { ?>

                                    <div class="product-wishlist">
                                        <span class="material-icons wishlist-logo" id="wishlist-logo-<?= $item['id']; ?>">
                                            favorite
                                        </span>
                                        <label for="wishlist-select-<?= $item['id']; ?>"></label>
                                        <select id="" class="wishlist-select-<?= $item['id']; ?> js-example-basic-multiple" name="states[]" multiple="multiple">
                                        </select>
                                    </div>


                                <?php } ?>

                            </div>
                            <div class="col-12 product-checkout-button">
                                <div class="edit" style="margin-bottom:8px" field="product_addToCart_btn" rel="content">
                                    <?php if (!isset($in_stock) or $in_stock == false) : ?>
                                        <a href="javascript:;" disabled="disabled" class="btn btn-primary product-cart-icon cart-disable"><i class="material-icons">shopping_cart</i> Out of stock</a>
                                    <?php else : ?>

                                        <a href="javascript:;" onclick="mw.cart.add('.shop-products .item-<?php print $item['id'] ?>'); carttoggole();" class="btn btn-primary product-cart-icon"><i class="material-icons">shopping_cart</i> in den Warenkorb</a>
                                    <?php endif; ?>
                                </div>
                                <div class="edit" field="product_quickCheckout_btn" rel="content">
                                    <?php if (user_id() == 1) : ?>
                                        <!--                                         <input  type="text" class="clipboard-data---><?php //print $item['id']
                                                                                                                                    ?>
                                        <!--"  style="-->
                                        <!--     width: 10px;-->
                                        <!--    height: 10px;-->
                                        <!--    margin-left: -11px;-->
                                        <!--">-->
                                        <!-- <span class="clipboard-data-" style="display:none;"></span> -->
                                        <button class="btn btn-primary copy-url" type="button" data-id="<?php echo $item['id']; ?>" data-lang="<?= url_segment(0); ?>"><i class="fa fa-files-o" style="margin-right:5px;" aria-hidden="true"></i>Schnelle Kaufabwicklung</button>
                                        <!-- <button id="copyText" class="btn btn-info copy-url" type="button" onclick="guest_checkout('<?php print $item['id'] ?>','<?= url_segment(0); ?>')"><i class="fa fa-files-o" aria-hidden="true"></i>Quick Checkout</button> -->
                                    <?php endif; ?>
                                </div>
                            </div>
                            <?php
                            if (isset($offer['price']['offer_price']) && $offer['price']['expires_at'] != 0) {
                                //                        dd($offer);
                                if (\Carbon\Carbon::now()->diffInSeconds($offer['price']['created_at'], false) > 0) {
                                    $remaining = \Carbon\Carbon::parse($offer['price']['created_at'])->diffInSeconds($offer['price']['expires_at'], false);
                                } else {
                                    $remaining = \Carbon\Carbon::now()->diffInSeconds($offer['price']['expires_at'], false);
                                }
                                $remaining = $remaining > 0 ? $remaining : 0;
                                $counter = Config::get('custom.counter');
                            ?>



                                <div class="dt-countdown-style-<?= $counter ?>">
                                    <!-- "dt-countdown-style-1" This Number Will Be Dynamic Based On Select Design From backend -->
                                    <div class="dt-cdown-box">
                                        <div class="dt_t_countdown_data" data-end="<?= $remaining ?>"></div>
                                    </div>
                                </div>


                            <?php }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>
<input type="hidden" name="category_status" id="shop_<?= PAGE_ID ?>" data-<?= PAGE_ID ?>="shop" value="<?= PAGE_ID ?>">

<?php if (isset($pages_count) and $pages_count > 1 and isset($paging_param) && !isset($_GET['slug']) && !isset($_GET['wishlist_id'])) : ?>
    <module type="pagination" template="bootstrap4" pages_count="<?php echo $pages_count; ?>" paging_param="<?php echo $paging_param; ?>" />
<?php endif; ?>




<script type="text/javascript">
    <?php if ($is_logged && !isset($_GET['slug']) && !isset($_GET['wishlist_id'])) { ?>

        <?php if (!empty($data)) :
            foreach ($data as $item) : ?>
                $(".wishlist-select-<?php echo $item['id']; ?>").on('select2:unselect', function(e) {
                    removeProduct(<?php echo $item['id']; ?>, e.params.data.id)
                    if ($(".wishlist-select-<?php echo $item['id']; ?>").val().length == 0) {
                        $("#wishlist-logo-<?php echo $item['id']; ?>").text("favorite_border");
                    }
                });

                $(".wishlist-select-<?php echo $item['id']; ?>").on('select2:select', function(e) {
                    addProduct(<?php echo $item['id']; ?>, e.params.data.id)
                    $("#wishlist-logo-<?php echo $item['id']; ?>").text("favorite");
                });

            <?php endforeach;
            endif; ?>

        function removeProduct(productId, sessionId) {
            $.post("<?php print api_url('remove_wishlist_sessions'); ?>", {
                productId: productId,
                sessionId: sessionId
            }, () => {});
        }

        function addProduct(productId, sessionId) {
            $.post("<?php print api_url('add_wishlist_sessions'); ?>", {
                productId: productId,
                sessionId: sessionId
            }, () => {});
        }
    <?php } ?>

    function wishlist_filter(wId) {
        $.post("<?php print site_url('en/shop'); ?>", {
            wishlist_id: wId
        }, () => {});
    }
    $("#input_text").hide();
    $("#clickBtn").on('click', function() {
        $("#clickBtn").hide();
        $("#clickBtn").parent().hide();
        $("#input_text").show();
        share_wishlist();
    });

    $(document).on('click', '#input_text', function() {
        this.select();
        document.execCommand('copy');
    });

    function share_wishlist() {
        return $.post($('form#wishlist_short_url_form').attr('action'), $('form#wishlist_short_url_form').serialize(), (res) => {
            $('#input_text').val(res.url)
        });
    }

    $(document).on('click', '#edit_sss', function() {

        let name = $(this).data('name');

        console.log(name);
        // console.log(id);


        $("#exampleInputEmailedit").val(name);
        $("#exampleInputEmailedithide").val(name);
    });


    $(document).on('click', '#delete_sss', function() {

        let name = $(this).data('name');

        console.log(name);
        // console.log(id);
        $.post("<?php print api_url('delete_wishlist_sessions'); ?>", {
            name: name
        }, function(sessions) {
            if (sessions === 'false') {
                // emailHelp.show();
                location.reload();

            } else {
                location.reload();
            }
        });


    });
</script>
<script>
    $(document).ready(function() {

        function copyTextToClipboard(text) {
            var textArea = document.createElement("textarea");

            //
            // *** This styling is an extra step which is likely not required. ***
            //
            // Why is it here? To ensure:
            // 1. the element is able to have focus and selection.
            // 2. if the element was to flash render it has minimal visual impact.
            // 3. less flakyness with selection and copying which **might** occur if
            //    the textarea element is not visible.
            //
            // The likelihood is the element won't even render, not even a
            // flash, so some of these are just precautions. However in
            // Internet Explorer the element is visible whilst the popup
            // box asking the user for permission for the web page to
            // copy to the clipboard.
            //

            // Place in the top-left corner of screen regardless of scroll position.
            textArea.style.position = 'fixed';
            textArea.style.top = 0;
            textArea.style.left = 0;

            // Ensure it has a small width and height. Setting to 1px / 1em
            // doesn't work as this gives a negative w/h on some browsers.
            textArea.style.width = '2em';
            textArea.style.height = '2em';

            // We don't need padding, reducing the size if it does flash render.
            textArea.style.padding = 0;

            // Clean up any borders.
            textArea.style.border = 'none';
            textArea.style.outline = 'none';
            textArea.style.boxShadow = 'none';

            // Avoid flash of the white box if rendered for any reason.
            textArea.style.background = 'transparent';


            textArea.value = text;

            document.body.appendChild(textArea);
            textArea.focus();
            textArea.select();
            document.execCommand('copy');

        }

        function copyClipBoardText(className) {
            /* Get the text field */
            // var copyText = document.getElementsByClassName(class);
            var copyText = document.getElementsByClassName(className);
            // console.log(copyText);
            /* Select the text field */
            copyText[0].select();

            /* Copy the text inside the text field */
            document.execCommand("copy");

            /* Alert the copied text */
            // alert("Copied the text: " + copyText[0].value);
        }
        $(document).on('click', '.copy-url', function() {
            // event.preventDefault();
            let id = $(this).data('id');
            let lang = $(this).data('lang');
            $.ajax({
                method: 'POST',
                url: "<?php print api_url('guest_checkout'); ?>",
                data: {
                    iid: id,
                    lang: lang
                },
                success: function(response) {
                    if (response.success) {
                        // $('.clipboard-data-'+id).val(response.url);
                        // console.log(response.url);
                        copyTextToClipboard(response.url);
                        // copyClipBoardText('clipboard-data-'+id);

                    }
                }
            });
            // $.post("<?php print api_url('guest_checkout'); ?>", {iid: id, lang: lang}, (res) => {
            //     console.log(res);
            // });
        });


        //Update trial clock
        function updateDTTemplateTrialClock(el) {
            let time_interval = setInterval(function() {
                let total = el.data('end');

                if (!total) {
                    el.hide();
                    el.html('');
                    clearInterval(time_interval);
                    return;
                }

                const seconds = Math.floor(total % 60);
                const minutes = Math.floor((total / 60) % 60);
                const hours = Math.floor((total / (60 * 60)) % 24);
                const days = Math.floor(total / (60 * 60 * 24));
                --total;

                el.data('end', total);
                el.css('padding', '10px 15px');
                el.html(`<div class="days-wrapper"><p>${days < 10? ' 0'+days : days}</p> <span class="su">Tage</span><span class="su-res">T</span></div> <div class="hours-wrapper"><p>${hours < 10?  '0'+hours : hours}</p><span class="su">Stunden</span><span class="su-res">S</span></div> <div class="minutes-wrapper"><p>${minutes < 10?  '0'+minutes : minutes}</p> <span class="su">Minuten</span><span class="su-res">M</span></div><div class="seconds-wrapper"><p>${seconds < 10?  '0'+seconds : seconds}</p><span class="su">Sekunden</span><span class="su-res">S</span></div>`);

            }, 1000);
        }

        function show_dt_template_trial_countdown() {
            if (!$('.dt_t_countdown_data').length) return;

            $('.dt_t_countdown_data').each(function() {
                let _st = $(this).data('end');
                if (_st) {
                    updateDTTemplateTrialClock($(this))
                }
            });
        }

        $(document).ready(function() {
            show_dt_template_trial_countdown();
        })

    });

    <?php if ($is_logged && !isset($_GET['slug']) && !isset($_GET['wishlist_id'])) { ?>
        $(document).ready(() => {
            $.get(`<?= api_url('get_wishlist_sessions'); ?>`, result => {
                const selected = [];
                const list = [];
                result.forEach(function(session) {
                    list.push('<option value=' + session['id'] + '>' + session['name'] + '</option>');
                    $("#wishlist-list").append('<li title="' + session['name'] + '"><a href="shop?wishlist_id=' + session["id"] + '" data-category-id="' + session['id'] + '" title="' + session['name'] + '" class="depth-0">' + session['name'] + '</a><button type="button" id="delete_sss" class="btn" data-toggle="modal" data-name="' + session['name'] + '" ><span class="material-icons">delete</span></button><button type="button" id="edit_sss" class="btn" data-toggle="modal" data-target="#exampleModalCenteredit" data-name="' + session['name'] + '" ><span class="material-icons">create</span></button></li>');
                    session['products'].forEach(function(prod) {
                        if (selected[parseInt(prod['product_id'])] === undefined) {
                            selected[parseInt(prod['product_id'])] = [];
                        }
                        selected[parseInt(prod['product_id'])].push(session.id.toString())
                    })
                });

                <?php if (!empty($data)) : ?>
                    <?php foreach ($data as $item) : ?>
                        var wishlistProduct = $(".wishlist-select-<?php echo $item['id']; ?>");
                        wishlistProduct.empty();
                        wishlistProduct.append('<option disabled value="null"></option>');
                        list.forEach(function(value) {
                            wishlistProduct.append(value);
                        });

                        var didd = <?php echo $item['id']; ?>;
                        wishlist_details(didd);

                    <?php endforeach; ?>
                <?php endif; ?>

                selected.forEach(function(value, index) {
                    const wishlistProduct2 = $(".wishlist-select-" + index.toString());
                    wishlistProduct2.select2().val(value).trigger("change");
                });

                function wishlist_details(didd) {
                    if (selected[didd] && selected[didd].length > 0) {
                        $("#wishlist-logo-" + didd).text("favorite");
                    } else {
                        $("#wishlist-logo-" + didd).text("favorite_border");
                    }
                }

            });
        });
    <?php } ?>

 
</script>