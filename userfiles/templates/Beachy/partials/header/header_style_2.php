<?php if ($profile_link == 'true') : ?>
<script>
        var $window = $(window),
            $document = $(document);
        $document.ready(function() {
            $('.js-register-modal').on('click', function() {
                $(".js-login-window").hide();
                $(".js-forgot-window").hide();
                $(".js-register-window").show();
            });
            $('.js-login-modal').on('click', function() {
                $(".js-register-window").hide();
                $(".js-forgot-window").hide();
                $(".js-login-window").show();
            });
        });
    </script>
<?php endif; ?>
<nav class="navigation" style="display:<?php print $headerShowCss; ?>;">
    <div class="container">
        <div class="navbar-header">
            <module type="logo" class="logo" id="header-logo" data-alt-logo="true" />
            <div class="menu-overlay">
                <div class="menu">
                    <div class="toggle-inside-menu">
                        <a href="javascript:;" class="js-menu-toggle mobile-menu-btn">
                            <span></span>
                            <span></span>
                            <span></span>
                        </a>
                    </div>
                    <?php include('parts/mobile_search_bar.php'); ?>
                    <?php include('parts/mobile_profile_link.php'); ?>
                    <module type="menu" name="header_menu" id="header_menu" template="navbar" />
                </div>
                <div class="header-cat <?php print  @$showHeader['header'] ?? 'hide'; ?>">
                    <div class="header-categories">
                        <?php if (@$shop_cat == @$active) {
                        ?>
                            <module type="shop_categories" content-id="<?php print $active; ?>" />
                        <?php
                        } elseif (@$blog_data == @$active) {
                        ?>
                            <module type="categories" content-id="<?php print $active; ?>" />
                        <?php
                        } ?>
                    </div>
                </div>
            </div>
            <ul class="member-nav main-member-nav visible-search">
                <?php include('parts/desktop_profile_link.php'); ?>
                <?php include('parts/shopping_cart.php'); ?>
                <?php include('parts/desktop_search_bar.php'); ?>
                <li class="ml-3">
                    <div class="toggle">
                        <a href="javascript:;" class="js-menu-toggle mobile-menu-btn">
                            <span></span>
                            <span></span>
                            <span></span>
                        </a>
                    </div>
                </li>
            </ul>
        </div>
    </div>

    <?php include('parts/header_posts_holder.php'); ?>
</nav>
<script>
    $(document).ready(function() {
        $(".header-cat .nav>li>ul>li").has("ul").addClass("sub-cat");
    })
</script>