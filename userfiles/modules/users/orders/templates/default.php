
<?php if (isset($orders) and is_array($orders)):?>



    <h3 class="m-b-20 edit" field="order_modal_heading_one" rel="content">meine Bestellungen</h3>
    <div class="edit" field="order_modal_top" rel="content">
        <module type="text" />
    </div>

    <style>
        .order-chat-box-wrapper .input-area {
            width: 100% !important;
        }

        .input-area .emojiPickerIconWrap {
            width: 100%;
        }

        .input-area .emojiPickerIconWrap input {
            width: 100% !important;
            height: 31px;
        }


    </style>
	<?php $status = DB::table('subscription_order_status')->where('order_id','<>', null)->get(); ?>
    <?php
    foreach ($orders as $order) {
        ?>
		<?php $orderShow = 0; ?>
		<?php if(isset($status) && count($status) > 0) {
			foreach ($status as $subOrder) {
				if($subOrder->order_id == $order['id']) {
					$orderShow = 1;
				}
			}
		}
		?>
		<?php if($orderShow == 0) : ?>
        <?php $cart = get_cart('order_id=' . $order['id']); ?>
        <?php if (is_array($cart) and !empty($cart)): ?>
            <div class="mw-ui-box mw-ui-box-content my-order">
                <div class="order-chat-option">
                <span id="my-order-ChatOption" class="order-chat-modal order-chat-modal-<?=$order['id']?>" data-id="<?=$order['id']?>" data-toggle="modal" data-target="#order-chat-modal-<?=$order['id']?>">
                    <span class="badge" id="notifi-<?=$order['id']?>">00</span>
                    <i class="fa fa-comments"  aria-hidden="true"></i>
                </span>

                </div>

                <div class="my-order-status pull-right">
<!--                    --><?php //if ($order['order_status'] == 'completed') { ?>
<!--                        <span class="my-order-status-completed text-green">Abgeschlossen</span>-->
<!--                    --><?php //} else { ?>
<!--                        <span class="my-order-status-pending text-red">Steht aus</span>-->
<!--                    --><?php //} ?>
                </div>

                <h4>Bestellung #<span><?php print $order['id']; ?></span> -
                    <small>Aufgegeben am: <?php print $order['created_at']; ?></small>
                </h4>
                <div class="order-new-info">
                    <?php
                    $paymnet_method = $order['payment_gw'];
                    $paymnet_method = explode("/",$paymnet_method);

                    ?>
                    <p><strong>Payment method:</strong><?php print end($paymnet_method); ?></p>
                    <p id="parcel_number_<?=$order['id']?>"><strong>Parcel numbe:</strong></p>
                    <p id="order_status_<?=$order['id']?>"><strong>Order-Status:</strong></p>
                </div>
                <hr class="m-b-0"/>
                <div class="table-responsive productOrderTable">
                    <table width="100%" class="table" cellspacing="0" class="mw-ui-table mw-ui-table-basic product-order-table">
                        <thead>
                        <tr>
                            <th>Bild</th>
                            <th>Produktname</th>
                            <th>Menge</th>
                            <th>Preis</th>
                            <th>Gesamt</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($cart as $product) { ?>
                            <?php $theproduct = get_content_by_id($product['rel_id']); ?>
                            <?php if ($theproduct): ?>
                                <tr>
                                    <td>
                                        <img src="<?php print get_picture($theproduct['id']); ?>" width="70" alt=""/>
                                    </td>
                                    <td><?php print $theproduct['title']; ?></td>
                                    <td><?php print $product['qty']; ?></td>
                                    <td><?php print $product['price']; ?></td>
                                    <td><?php print (intval($product['qty']) * intval($product['price'])); ?></td>
                                    <td>
                                    <span class="re-order-btn">
                                        <button class="btn btn-success" onclick="mw.cart.add_and_checkout(<?php echo $product['rel_id']; ?>,<?php echo $product['price']; ?>)">Re-order</button>
                                    </span>
                                    </td>
                                </tr>
                            <?php endif; ?>
                        <?php } ?>
                        </tbody>
                    </table>


                </div>
            </div>
            <br/>
        <?php endif; ?>
        <script>

            $(".order-chat-modal-<?=$order['id']?>").on("click", function(){
                $("#chat-popup-<?=$order['id']?>").addClass("show");
                // $("#emojionearea<?=$order['id']?>").emojioneArea({
                //     pickerPosition: "top",
                //     tonesStyle: "bullet"
                // });
                $('#emojionearea<?=$order['id']?>').emojiPicker({
                    height: '150px',
                    width:  '250px'
                });

            });

            $(document).on('click', ".order-chat-modal", function(){
                const id = $(this).data('id');
                $.post("<?= url('/') ?>/api/v1/get_chat", { id:id }, res => res.data)
                    .then(res => res.data)
                    .then(data => {
                        console.log(data)

                        if(data.messages) {
                            let message_list = '';
                            data.messages.forEach(msg => {
                                let recipient = msg.recipient;
                                let sender = msg.sender
                                console.log(msg);


                                message_list += `
                                <li class="clearfix getChat_massage">
                                <div class="order-outgoing-msg-info message-data ${recipient == 'me'? 'text-right' : ''}">
                                <span class="message-data-time" title="${msg.created_at}">
                                <span class="name">${sender && sender.name ? sender.name : ''}</span>,
                            <span class="time">${msg.time}</span>
                        </span>
                            <br>
                                <span class="email">${sender && sender.email? sender.email : ''}</span>
                            </div>
                            <div class="message ${recipient == 'me'? 'my-message float-right' : 'other-message'}">${msg.message}</div>
                        </li>
                            `;


                            } )

                            let el_id = `#order-chat-modal-`+id+` #chat-area`+id ;

                            $(el_id).html(message_list)
                            $(`#notifi-${id}`).html('00')
                            $("#chat-area"+id).stop().animate({ scrollTop: $("#chat-area"+id)[0].scrollHeight}, 1000);



                        }





                    })
                    .then( o => {
                        $.post("<?= url('/') ?>/api/v1/seen_chat", { id:id } ).catch(err => {})
                    })
                    .catch(err => {
                        console.log(err)
                    })


            });



            $('#submitOrderText<?=$order['id']?>').on('click', function(){
                var inputElm = document.querySelector('input#emojionearea<?=$order['id']?>');
                var popup = document.querySelector('#chat-popup<?=$order['id']?>');
                //const chatBtn = document.querySelector('#my-order-ChatOption');
                // const submitBtn = document.querySelector('.submitOrderText');
                var chatArea = document.querySelector('#chat-area<?=$order['id']?>');
                let userInput = inputElm.value;

                var msg = $("#emojionearea<?=$order['id']?>").val();


                $.post("<?= url('/') ?>/api/v1/send_chat", { id: <?=$order['id']?> ,data: msg }, res => res.data)
                    .then(res => res.data)
                    .then(data => {

                        if(data) {
                            let temp = '';
                            let recipient = data.recipient;
                            let sender = data.sender
                            console.log(data,recipient,sender);

                            console.log(temp)
                            temp += `
                                <li class="clearfix Sentchat_Massage">
                                <div class="order-outgoing-msg-info message-data ${recipient == 'me'? 'text-right' : ''}">
                                <span class="message-data-time" title="${msg.created_at}">
                                <span class="name">${sender && sender.name ? sender.name : ''}</span>,
                            <span class="time">${data.time}</span>
                        </span>
                            <br>
                                <span class="email">${sender && sender.email? sender.email : ''}</span>
                            </div>
                            <div class="message ${recipient == 'me'? 'my-message float-right' : 'other-message'}">${data.message}</div>
                        </li>
                            `;

                            chatArea.insertAdjacentHTML("beforeend", temp);
                            $("#chat-area<?=$order['id']?>").stop().animate({ scrollTop: $("#chat-area<?=$order['id']?>")[0].scrollHeight}, 1000);

                        }

                    })
                    .catch(err => {
                        console.log(err)
                    })



                inputElm.value = '';
                $(".emojionearea-editor").html("");
            });


            $("#emojionearea<?=$order['id']?>").keydown(function(event) {
                if(event.which == 13) {
                    if($("#emojionearea<?=$order['id']?>").val() ) {
                        $('#submitOrderText<?=$order['id']?>').click();


                    }
                }

            });

        </script>
		<?php endif; ?>
    <?php } ?>



    <!-- <script src="https://cdn.jsdelivr.net/npm/@joeattardi/emoji-button@3.1.1/dist/index.min.js"></script> -->

    <link rel="stylesheet" type="text/css" href="<?= url('/') ?>/public/emoji-picker/jquery.emojipicker.css">
    <script type="text/javascript" src="<?= url('/') ?>/public/emoji-picker/jquery.emojipicker.js"></script>
    <!-- Emoji Data -->
    <link rel="stylesheet" type="text/css" href="<?= url('/') ?>/public/emoji-picker/jquery.emojipicker.tw.css">
    <script type="text/javascript" src="<?= url('/') ?>/public/emoji-picker/jquery.emojis.js"></script>


    <script>

        $(document).ready(function () {
            $.post("<?= url('/') ?>/api/v1/unseen", { ids:'all' }, res => res.data)
                .then(res => res.data)
                .then(data => {



                    if(data) {

                        for(const item in data) {
                            let newd = data[item];
                            let tracking = newd.tracking;
                            if(typeof (tracking[Object.keys(tracking)[0]]) != 'undefined'){
                                $(`#parcel_number_${item}`).html('<strong>Parcel numbe:</strong>'+tracking[Object.keys(tracking)[0]].package_number)
                            }
                            $(`#notifi-${item}`).html(newd.unseen)

                            $(`#order_status_${item}`).html('<strong>Order-Status:</strong>'+newd.status)

                        }

                    }
                })
                .catch(err => {
                    console.log(err)
                })
        });
        //  $("#my-order-ChatOption").on("click", function(){
        //     $(".chat-popup").toggleClass("show");
        // });




        // const emojiBtn = document.querySelector('#emoji-btn');

        //   chat button toggler

        // chatBtn.addEventListener('click', ()=>{
        //     //popup.classList.toggle('show');
        //     $(".chat-popup").addClass("show");
        //     $("#emojionearea1").emojioneArea({
        //         pickerPosition: "top",
        //         tonesStyle: "bullet"
        //     });
        // })




        $(".order-chat-option").on("click", function(){
            var clickedOrderNumber = $(this).siblings( "h4" ).children("span").text();
            $(".currentOrderId").html('Order Id: #'+clickedOrderNumber);
            $(".currentOrderNo").val(clickedOrderNumber);

        });





        // send msg .













    </script>
<?php else: ?>



    <div class="edit" field="order_modal_heading_two" rel="content">
        <h3>Sie haben keine Bestellungen</h3>
    </div>




<?php endif; ?>


<?php if (isset($status) &&  count($status) > 0) : ?>
    <div class="">
        <module type="shop/subscriptionProduct/template/default"/>
    </div>
<?php endif; ?>
