<?php

/*

type: layout

name: Default new

description: Default

*/
?>
<style>
    .thank-you-default{
        padding:50px 0;
    }
    .thank-you-default-header,
    .thank-you-default-button {
        text-align: center;
    }
    .thank-you-default-header p,
    .information p{
        font-size:16px;
    }
    .thank-you-default-button{
        margin:20px 0;
    }
    .information h3,
    .address h3{
        margin-bottom:30px;
    }
    .information{
        display: flex;
        justify-content: flex-end;
    }
    .address span{
        display:block;
    }
    .thank-you-table-product-img{
        width: 80px !important;
    }
    .thank-you-table-product-img img{
        height: 70px !important;
        width:100%;
    }
    .thank-you-table-product-name p{
        font-size:16px;
    }
    .thank-you-table td{
        vertical-align: middle !important;
        line-height: 20px !important;
    }

    .thankYou-checkout-total-table-row{
        display:flex;
        justify-content:flex-end;
    }

    .thank-you-table-product-name a {
        color: #000;
        text-decoration: none;
    }
    @media screen and (max-width: 767px){
        .thank-you-table-product-name h3{
            font-size: 16px;
        }
        .thankYou-checkout-total-table-row{
            justify-content:flex-start;
        }
        .information{
            justify-content:center;
            text-align:center;
        }
        .address{
            text-align:center;
            margin-bottom:20px
        }
    }
</style>
<div class="thank-you-default">
    <div class="thank-you-default-header">
        <h2>Vielen Dank für Ihre Bestellung bei uns.</h2>
        <p>Wir haben Ihnen eine Bestellbestätigung per E-Mail geschickt.</p>
        <p>Wir empfehlen die unten aufgeführte Bestellbestätigung auszudrucken.</p>
    </div>
    <?php
        $last_order_information = DB::table('cart_orders')->where('order_completed',1)->get()->last();
        if($last_order_information){
            $name = $last_order_information->first_name.' '.$last_order_information->last_name;
            $address = $last_order_information->address;
            $city_country = $last_order_information->city.', '.$last_order_information->country;
            $order_number = $last_order_information->id;
            $payment_path = explode("/",$last_order_information->payment_gw);
            $payment_method = end($payment_path);
            $total_price = $last_order_information->payment_amount;
            $shipping_cost = $last_order_information->payment_shipping;
            $product_price =  $total_price -  $shipping_cost;
            $tax_amount = $last_order_information->taxes_amount;
            $tax_rate = (is_logged()) ? (int)taxRateCountry(user_id()) : taxRate();
            $total_price_without_tax =  $total_price -  $tax_amount;
            $order_pdf_link = delivery_bill_url($order_number);
        }else{
            $name = NULL;
            $address = NULL;
            $city_country = NULL;
            $order_number = NULL;
            $payment_method = NULL;
            $total_price = NULL;
            $shipping_cost = NULL;
            $product_price =  NULL;
            $tax_amount = NULL;
            $tax_rate= NULL;
            $total_price_without_tax = NULL;
            $order_pdf_link = '#';
        }
    ?>
    <div class="thank-you-default-button">
        <a href="<?php print(site_url()); ?>" class="btn btn-primary"><i style="padding-top:4px" class="fa fa-arrow-left" aria-hidden="true"></i> Zuruck</a>
        <a href="<?php print  $order_pdf_link; ?>" class="btn btn-primary">Pdf delivery note</a>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="address">
                <h3>Address</h3>
                <span><?php print $name; ?></span>
                <span><?php print $address; ?></span>
                <span><?php print $city_country; ?></span>
            </div>
        </div>
        <div class="col-md-6">
            <div class="information">
                <div class="information-inner">
                    <h3>Information</h3>
                    <p><strong>Order Number:</strong> <?php print $order_number ?></p>
                    <p><strong>Payment Method:</strong> <?php print  $payment_method; ?></p>
                </div>
            </div>
        </div>
    </div>
    <div class="" style="margin-top:30px">
        <div class="" style="margin-top:30px">
            <table class="table table-bordered table-striped thank-you-table">
                <thead>
                    <tr>
                        <th style="min-width:70px">Bild</th>
                        <th>Produktname</th>
                        <th>Menge</th>
                        <th>Gesamt</th>
                    </tr>
                </thead>
                <?php
                $ordered_all_product = DB::table('cart')->where('order_id', $order_number)->get();
                // dd($ordered_all_product);
                ?>
                <tbody>
                    <?php if($ordered_all_product): ?>
                        <?php foreach($ordered_all_product as $order_product):  ?>
                            <tr>
                                <?php
                                if(isset(get_content(array("id" => $order_product->rel_id))[0])){
                                    $img_link = get_content(array("id" => $order_product->rel_id))[0]['media'][0]['filename'];
                                    $product_url = get_content(array("id" => $order_product->rel_id))[0]['url'];


                                }else{
                                    $img_link = '#';
                                    $product_url = '#';
                                }
                                ?>
                                <td class="thank-you-table-product-img">
                                    <img src="<?php print $img_link; ?>" alt="">
                                </td>
                                <td class="thank-you-table-product-name">
                                    <h3><b><a href="<?php print  $product_url; ?>" target="_blank"><?php print $order_product->title  ?></a></b></h3>
                                </td>
                                <td class="thank-you-table-product-quantity">
                                    <span><?php print  $order_product->qty  ?></span>
                                </td>
                                <td class="thank-you-table-product-price">
                                    <span><?php print currency_format(($order_product->price+taxPrice($order_product->price))*$order_product->qty);  ?></span>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
        <div class="row thankYou-checkout-total-table-row">
            <div class="col-md-6">
                <table class="table table-bordered table-striped thankYou-checkout-total-table">
                    <tr>
                        <td>
                            <b>Warengesamtpreis</b>
                        </td>
                        <td>
                            <b><?php print currency_format($product_price); ?></b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Versandkosten:
                        </td>
                        <td>
                            <?php print currency_format($shipping_cost); ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Gesamtpreis</b>
                        </td>
                        <td>
                            <?php print currency_format($total_price); ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            darin enthalten <?php print  $tax_rate.' % USt.'; ?>
                        </td>
                        <td>
                            <?php print currency_format($tax_amount); ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Nettobetrag</b>
                        </td>
                        <td>
                            <?php print currency_format($total_price_without_tax); ?>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>