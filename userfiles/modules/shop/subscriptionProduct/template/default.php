<style>

    .data h6{
        line-height : 1.7;
    }
    .data{
        border : 1px solid  #FFF8DC;
        padding: 20px 10px;
        margin-bottom: 20px;
        background-color: #FFFFFF;
    }
    .data button{
        border: 1px solid #663399;
        color: #FFFFFF;
        padding: 10px 15px;
        background-color: #222222;
    }
    .data button:hover{
        opacity: .8;
        color: #FFFFFF;
    }
    .subscription-product-info {
        display: flex;
        flex-direction: column;
        justify-content: center;
    }
    .subscription-product-image{
        height:auto !important;
    }
   
</style>
<div class="">
    <!-- active subcription -->
    <?php
        $item = DB::table('subscription_order_status')->where('order_id','<>',null)->where('order_status','=',"active")->where('order_type','=',"new")->where('user_id', user_id())->get();
        $item1 = DB::table('subscription_order_status')->where('order_id','<>',null)->where('order_status','=',"inactive")->where('order_type','=',"new")->where('user_id', user_id())->get();
    ?>
    <?php if(isset($item) || isset($item1)): ?>
        <h4><?php _e("Your Subscriptions Product"); ?></h4><hr>
        <?php 
        if (count($item)>0) { ?>
            <h5><?php _e("Active Subscriptions"); ?></h5><br>
            <?php foreach ($item as $value) : 
                $product = get_cart("order_id=$value->order_id");
                $sub_inter = DB::table('subscription_items')->where('id','=', $value->subscription_id)->first();
                $sub_dur = DB::table('subscription_status')->where('product_id','=', $value->product_id)->first();
                foreach ($product as $pro) : 
                // dd($product);
                ?>
                    <div class="row data">
                        <div class="col-5">
                            <div class="row">
                                <div class="col-6">
                                    <img class="subscription-product-image" src="<?php  print $pro['picture'] ?>" alt="product image" height="100" width="250">
                                </div>
                                <div class="col-6 subscription-product-info">
                                    <h5><?php print $pro['title'] ?></h5>
                                    <h6><i class="far fa-eye"></i>  <?php _e("subscription"); ?></h6>
                                    <h6><?php _e("QTY"); ?>: <?php print $pro['qty'] ?></h6>
                                    <h6>
                                        <?php
                                            $price = $value->order_price;
                                            $price *= $pro['qty']; 
                                            print $price; 
                                        ?> €
                                    </h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-3" style="padding-top:4%;">
                            <h6><?php _e("Next Shipment"); ?></h6>
                            <h5><?php
                                $date = $value->created_at;
                                $nextDate = date('F, jS', strtotime($date . ' +'.$sub_inter->sub_interval));
                                print $nextDate;
                            ?>
                            </h5>
                        </div>
                        <div class="col-4"  style="padding-left:5px;padding-top:4%;"> 
                            <button class="btn" onclick="show_single_order(<?php print $value->id ?>)"><?php _e("Manage subscription"); ?></button>
                        </div>
                    </div>
                <?php break;
                    endforeach; 
                ?>
            <?php endforeach; ?>
        <?php
        } else { ?>
        <div class="row data">
                <h6><?php _e("You have no any active subscription product"); ?></h6>
            </div>

        <?php } ?>

        <!-- inactive subcription -->
        
        <?php 
        if (count($item1)>0) { ?>
        <h5><?php _e("Inactive Subscriptions"); ?></h5><br>
            <?php foreach ($item1 as $value) : 
                $product = get_cart("order_id=$value->order_id");
                // dd($product);
                $sub_inter = DB::table('subscription_items')->where('id','=', $value->subscription_id)->first();
                $sub_dur = DB::table('subscription_status')->where('product_id','=', $value->product_id)->first();
                foreach ($product as $pro) : 
                ?>
                    <div class="row data">
                        <div class="col-5">
                            <div class="row">
                                <div class="col-6">
                                    <img class="subscription-product-image" src="<?php  print $pro['picture'] ?>" alt="product image" height="100" width="250">
                                </div>
                                <div class="col-6 subscription-product-info>
                                      <h5><?php print $pro['title'] ?></h5>
                                    <h6><i class="far fa-eye"></i>  <?php _e("subscription"); ?></h6>
                                    <h6><?php _e("QTY"); ?>: <?php print $pro['qty'] ?></h6>
                                    <h6>
                                        <?php
                                            $price = $value->order_price;
                                            $price *= $pro['qty']; 
                                            print $price; 
                                        ?> €
                                    </h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-3" style="padding-top:4%;">
                            <h6><?php _e("Next Shipment"); ?></h6>
                            <h5>
                            <?php
                                $date = $value->created_at;
                                $nextDate = date('F, jS', strtotime($date . ' +'.$sub_inter->sub_interval));
                                print $nextDate;
                            ?>
                            </h5>
                        </div>
                        <div class="col-4"  style="padding-left:5px;padding-top:4%;"> 
                            <a href="/subscribe/reactive?id=<?php echo $value->agreement_id; ?>"><button class="btn"><?php _e("Reactive"); ?></button></a>
                        </div>
                    </div>
                <?php break;
                    endforeach; 
                ?>
            <?php endforeach; 
        } else {
            ?>
            <div class="row data">
                <h6><?php _e("You have no any inactive subscription product"); ?></h6>
            </div>

        <?php } ?>
    <?php endif; ?>
</div>
<script>
    function show_single_order(id){
        window.location.href =  "<?=url('/')?>/subscription_product?id="+id;
    }
    
</script>