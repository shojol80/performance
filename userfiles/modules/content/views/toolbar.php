
<script>
    $(document).ready(function() {
        mw.lib.require('mwui_init');
    });
</script>

<?php
$custom_tabs = false;

$type = 'page';
$act = url_param('action', 1);
?>

<?php
if (isset($params['page-id'])) {
    $last_page_front = $params['page-id'];
} else {

    $last_page_front = session_get('last_content_id');
    if ($last_page_front == false) {
        if (isset($_COOKIE['last_page'])) {
            $last_page_front = $_COOKIE['last_page'];
        }
    }
}

$past_page = false;
if ($last_page_front != false) {
    $cont_by_url = mw()->content_manager->get_by_id($last_page_front, true);
    if (isset($cont_by_url) and $cont_by_url == false) {
        $past_page = mw()->content_manager->get("order_by=updated_at desc&limit=1");
        $past_page = mw()->content_manager->link($past_page[0]['id']);
    } else {
        $past_page = mw()->content_manager->link($last_page_front);
    }
} else {
    $past_page = mw()->content_manager->get("order_by=updated_at desc&limit=1");
    if (isset($past_page[0])) {
        $past_page = mw()->content_manager->link($past_page[0]['id']);
    } else {
        $past_page = false;
    }
}
?>
<?php if (isset($past_page) and $past_page != false) : ?>
    <script>
        $(function() {
            mw.tabs({
                nav: "#manage-content-toolbar-tabs-nav a",
                tabs: '#manage-content-toolbar-tabs .mw-ui-box-content'
            });
            $('.go-live-edit-href-set').attr('href', '<?php print $past_page; ?>');
        });
    </script>
<?php endif; ?>

<?php if (isset($params['keyword']) and $params['keyword'] != false) : ?>
    <?php $params['keyword'] = urldecode($params['keyword']); ?>

    <script>
        $(function() {
            $('[autofocus]').focus(function() {
                this.selectionStart = this.selectionEnd = this.value.length;
            });

            $('[autofocus]:not(:focus)').eq(0).focus();
        });
    </script>
<?php endif; ?>

<?php if ($page_info) : ?>
    <?php
    $content_types = array();
    $available_content_types = get_content('order_by=created_at asc&is_deleted=0&fields=content_type&group_by=content_type&parent=' . $page_info['id']);
    $have_custom_content_types_count = 0;
    if (!empty($available_content_types)) {
        foreach ($available_content_types as $available_content_type) {
            if (isset($available_content_type['content_type'])) {
                $available_content_subtypes = get_content('order_by=created_at asc&is_deleted=0&fields=subtype&group_by=subtype&parent=' . $page_info['id'] . '&content_type=' . $available_content_type['content_type']);
                if (!empty($available_content_subtypes)) {
                    $content_types[$available_content_type['content_type']] = $available_content_subtypes;
                }
            }
        }
    }

    $have_custom_content_types_count = count($content_types);

    if ($have_custom_content_types_count < 3) {
        $content_types = false;
    }
    ?>
    <?php if (isset($content_types) and !empty($content_types)) : ?>
        <?php $content_type_filter = (isset($params['content_type_filter'])) ? ($params['content_type_filter']) : false; ?>
        <?php $subtype_filter = (isset($params['subtype_filter'])) ? ($params['subtype_filter']) : false; ?>
        <?php
        $selected = $content_type_filter;
        if ($subtype_filter != false) {
            $selected = $selected . '.' . $subtype_filter;
        }
        ?>

        <script>
            $(function() {
                $("#content_type_filter_by_select").change(function() {
                    var val = $(this).val();
                    if (val != null) {
                        vals = val.split('.');
                        if (vals[0] != null) {
                            mw.$('#<?php print $params['id']; ?>').attr('content_type_filter', vals[0]);
                        } else {
                            mw.$('#<?php print $params['id']; ?>').removeAttr('content_type_filter');
                        }
                        if (vals[1] != null) {
                            mw.$('#<?php print $params['id']; ?>').attr('subtype_filter', vals[1]);
                        } else {
                            mw.$('#<?php print $params['id']; ?>').removeAttr('subtype_filter');
                        }

                        mw.reload_module('#<?php print $params['id']; ?>');
                    }
                });
            });
        </script>
    <?php endif; ?>
<?php endif; ?>


<?php if (!isset($edit_page_info)) : ?>
    <?php mw()->event_manager->trigger('module.content.manager.toolbar.start', $page_info) ?>

    <?php
    $type = 'mdi-post-outline';

    if (is_array($page_info)) {
        if ($page_info['is_shop'] == 1) {
            $type = 'mdi-shopping';
        } elseif ($page_info['subtype'] == 'dynamic') {
            $type = 'mdi-post-outline';
        } else if (isset($page_info['layout_file']) and stristr($page_info['layout_file'], 'blog')) {
            $type = 'mdi-text';
        } else {
            $type = 'mdi-post-outline';
        }
    }
    if (($act == 'products')) {
        $admin_header = "heading-style-one";
    } else {
        $admin_header = "";
    }
    ?>

    <div class="card-header <?php print $admin_header; ?> d-flex justify-content-between">
        <?php if (!isset($params['category-id']) and isset($page_info) and is_array($page_info)) : ?>
            <h5>
                <i class="mdi text-primary mr-2 <?php if ($type == 'shop') : ?>mdi-shopping<?php else : ?><?php print $type; ?><?php endif; ?>"></i>
                <?php print($page_info['title']) ?>

                <?php if (isset($params['page-id']) and intval($params['page-id']) != 0) : ?>
                    <?php $edit_link = admin_url('view:content#action=editpost:' . $params['page-id']); ?>
                    <a href="<?php print $edit_link; ?>" class="btn btn-outline-primary btn-sm" id="edit-content-btn"><?php _e("Edit page"); ?></a>
                <?php endif; ?>
            </h5>
        <?php elseif (isset($params['category-id'])) : ?>
            <div>
                <h5>
                    <?php $cat = get_category_by_id($params['category-id']); ?>
                    <?php if (isset($cat['title'])) : ?>
                        <i class="mdi mdi-folder text-primary mr-3"></i>
                        <strong><?php print $cat['title'] ?></strong>
                    <?php endif; ?>
                </h5>
            </div>
        <?php elseif ($act == 'pages') : ?>
            <h5>
                <i class="mdi mdi-post-outline text-primary mr-3"></i>
                <strong><?php _e("Pages"); ?></strong>
                <a href="<?php echo admin_url(); ?>view:content#action=new:page" class="btn btn-outline-success btn-sm ml-2"><?php _e("Add Page"); ?></a>
            </h5>
        <?php elseif ($act == 'posts') : ?>
            <h5>
                <i class="mdi mdi-text text-primary mr-3"></i>
                <strong><?php _e("Posts"); ?></strong>
                <a href="<?php echo admin_url(); ?>view:content#action=new:post" class="btn btn-outline-success btn-sm ml-2 js-hide-when-no-items"><?php _e("Add Post"); ?></a>

                <!-- generate rss -->
                <button type="button" class="btn btn-outline-success btn-sm ml-2 js-hide-when-no-items" data-toggle="modal" data-target=".rss-modal-lg" onclick="generate_rss_url()"><?php _e("Generate RSS"); ?></button>

                <div class="modal fade rss-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel"><?php _e('RSS Page Link') ?></h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="rssUrl" name="rssUrl">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>




                <!-- add rss -->
                <button type="button" id="add_rss" class="btn btn-outline-success btn-sm ml-2 js-hide-when-no-items" data-toggle="modal" data-target=".bd-example-modal-lg"><?php _e("Add RSS"); ?></button>
                <?php $rssLink  = get_option('rss_link', 'rss_data') ?? null; ?>
                <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel"><?php _e('Add RSS Link') ?></h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form>
                                <div class="modal-body">
                                    <h2 id="cusotm_din">

                                    </h2>
                                    <div id="advice" style="width: 400px; height: auto;">
                                        <!-- <form>
                                        <div id="button_pro">
                                            <div class='space' id='input_1'>
                                                <table>

                                                    <tr>
                                                        <td><input id="input_1" type="text" name="rsslink[]" class='left txt form-control' /></td>


                                                        <td class="addTD"><button type="button" class="add right btn btn-sm btn-success btn-save js-bottom-save">Add</button></td>
                                                    </tr>
                                                </table>


                                            </div>
                                        </div>


                                    </form> -->
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <button id="addRow" type="button" class="btn btn-primary mb-3"><?php _e('Add Row') ?></button>
                                                <div id="newRow">

                                                </div>
                                                <div id="inputFormRow">
                                                    <div class="input-group mb-3">
                                                        <input type="text" name="rsslink[]" class="form-control m-input rss_link_input" placeholder="RSS link" autocomplete="off">
                                                        <div class="input-group-append">
                                                            <button id="removeRow" type="button" class="btn btn-danger"><?php _e('Remove') ?></button>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
                                    <button onclick="rss_show()" type="button" class="btn btn-sm btn-success btn-save js-bottom-save">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </h5>

            <script>
                $('document').ready(function() {
                    // $("#add_rss").on("click", function() {
                    $.post("<?= api_url('rss_data_link') ?>", {}, (res) => {
                        if (res.success = "true") {

                            if (res.data.length > 0) {

                                $.each(res.data, function(index, item) {
                                    console.log(item);
                                    var html = '';
                                    html += '<div id="inputFormRow" class="ddd">';
                                    html += '<div class="input-group mb-3">';
                                    html += '<input type="text" name="rsslink[]" value="' + item + '" class="form-control m-input rss_link_input" placeholder="RSS Link" autocomplete="off" required>';
                                    html += '<div class="input-group-append">';
                                    html += '<button  id="removeRow"  type="button" class="btn btn-danger">Remove</button>';
                                    html += '</div>';
                                    html += '</div>';

                                    $('#newRow').append(html);

                                    // txt_box1 = '<div class="space" id="input_' + id + '" ><table> <tr><td><input id="input_' + id + '" type="text" name="rsslink[]" class="left txt form-control" value="' + item + '"/></td><td><button type="button" class="remove btn btn-sm btn-success btn-save js-bottom-save">Remove</button></td><td class="addTD"><button type="button" class="add right btn btn-sm btn-success btn-save js-bottom-save">Add</button></td></tr></table></div>';
                                    // $("#button_pro").append(txt_box1);
                                });
                            }
                            // var dd = (res.data.length);
                            // console.log(dd);



                            // return false;

                        }

                    }).then((res, err) => {
                        console.log(res, err);
                    });
                    $("#addRow").click(function() {
                        var html = '';
                        html += '<div id="inputFormRow">';
                        html += '<div class="input-group mb-3">';
                        html += '<input type="text" name="rsslink[]" class="form-control m-input rss_link_input" placeholder="RSS Link" autocomplete="off" required>';
                        html += '<div class="input-group-append">';
                        html += '<button  id="removeRow"  type="button" class="btn btn-danger">Remove</button>';
                        html += '</div>';
                        html += '</div>';


                        $('#newRow').append(html);
                    });
                    // });

                    // ================================================================== unused funtion
                    // var id = 2,
                    //     txt_box;
                    // $('#button_pro').on('click', '.add', function() {
                    //     $(this).remove();
                    //     txt_box = '<div class="space" id="input_' + id + '" ><table> <tr><td><input id="input_' + id + '" type="text" name="rsslink[]" class="left txt form-control"/></td><td><button type="button" class="remove btn btn-sm btn-success btn-save js-bottom-save">Remove</button></td><td class="addTD"><button type="button" class="add right btn btn-sm btn-success btn-save js-bottom-save">Add</button></td></tr></table></div>';
                    //     $("#button_pro").append(txt_box);
                    //     id++;
                    // });

                    // $('#button_pro').on('click', '.remove', function() {

                    //     var parent = $(this).closest(".space");
                    //     var parentPrev = $(parent).prev().find(".addTD");

                    //     $(parent).slideUp('medium', function() {
                    //         $(parent).remove();
                    //         if ($('.add').length < 1) {
                    //             $(parentPrev).append('<button type="button" class="add right btn btn-sm btn-success btn-save js-bottom-save">Add</button>');
                    //         }
                    //     });
                    // });

                    // ============================================ unused funtion


                    // remove row
                    $(document).on('click', '#removeRow', function() {
                        $(this).closest('#inputFormRow').remove();
                    });





                });


                // function rss_link_remove() {
                // var link= $("input[name='rsslink[]']").val();
                // var ddd = $(this).parent().parent().parent('#inputFormRow').find('.rss_link_input').val();
                // console.log(ddd);
                // }

                $('#removeRow').each(function() {
                    $(this).click(function() {
                        // var ddd = $(this).parent().parent().parent('#inputFormRow').find('.rss_link_input').val();
                        console.log('=================');
                    });
                })

                function rss_show() {
                    var link = $("input[name='rsslink[]']")
                        .map(function() {
                            return $(this).val();
                        }).get();
                    // console.log(link);


                    console.log(link);
                    $.post("<?= api_url('rss_link_input') ?>", {
                        link: link
                    }).then((res, err) => {
                        console.log(res, err);
                    });
                    mw.notification.success("Your link is saved successfully");
                    location.reload();
                }

                function rss_option() {
                    var option = $('#rssOption').val();
                    $.post("<?= api_url('rss_link_option') ?>", {
                        option: option
                    }).then((res, err) => {
                        console.log(res, err);
                    });
                    mw.notification.success("your option is saved successfully");
                }

                function generate_rss_url() {
                    document.getElementById("rssUrl").value = "<?php echo site_url('rss-blog'); ?>";
                }
            </script>
        <?php elseif ($act == 'products') :
        $product_limit = json_decode(get_drm_product_limit(), true);
        ?>
            <h5>
                <i class="mdi mdi-shopping text-primary mr-3"></i>
                <strong><?php _e("Products"); ?></strong>
                <a href="<?php echo admin_url(); ?>view:content#action=new:product" class="btn btn-outline-success btn-sm mt-1 js-hide-when-no-items"><?php _e("Add Product"); ?></a>
            </h5>
            <?php
        if (@$product_limit['limit_amount'] and @$product_limit['products']) {
            ?>
            <h5 class="av-product">
                <?php _e('Available Products'); ?> ( <?= $product_limit['products'] ?>/<?= $product_limit['limit_amount'] ?> )
            </h5>
        <?php
        }
        ?>
        <?php elseif (isset($params['is_shop'])) : ?>
            <h5>
                <span class="mdi mdi-shopping text-primary mr-3"></span>
                <strong><?php _e("My Shop"); ?></strong>
                <a href="<?php echo admin_url(); ?>view:content#action=new:product" class="btn btn-outline-success btn-sm ml-2 js-hide-when-no-items"><?php _e("Add Product"); ?></a>
            </h5>
        <?php else : ?>
            <h5 class="d-inline-block">
                <i class="mdi mdi-earth text-primary mr-3"></i>
                <strong><?php _e("Website"); ?></strong>
                <a href="<?php echo admin_url(); ?>view:content#action=new:page" class="btn btn-outline-success btn-sm ml-2 js-hide-when-no-items"><?php _e("Add Page"); ?></a>
            </h5>
        <?php endif; ?>

        <?php
        $cat_page = false;
        if (isset($params['category-id']) and $params['category-id']) {
            $cat_page = get_page_for_category($params['category-id']);
        }

        $url_param_action = url_param('action', true);
        $url_param_view = url_param('view', true);

        $url_param_type = 'page';

        if ($type == 'shop' or $url_param_view == 'shop' or $url_param_action == 'products') {
            $url_param_type = 'product';
        } else if ($cat_page and isset($cat_page['is_shop']) and intval($cat_page['is_shop']) != 0) {
            $url_param_type = 'product';
        } else if ($url_param_action == 'categories' or $url_param_view == 'category') {
            $url_param_type = 'category';
        } else if ($url_param_action == 'showposts' or $url_param_action == 'posts' or $type == 'dynamicpage') {
            $url_param_type = 'post';
        } else if ($cat_page and isset($cat_page['subtype']) and ($cat_page['subtype']) == 'dynamic') {
            $url_param_type = 'product';
        }

        $add_new_btn_url = admin_url('view:content#action=new:') . $url_param_type;
        ?>

        <div class="js-hide-when-no-items product-details-admin">

            <?php if (isset($params['add-to-page-id']) and intval($params['add-to-page-id']) != 0) : ?>
                <div class="mw-ui-dropdown">
                    <span class="mw-ui-btn mw-icon-plus"><span class=""></span></span>
                    <div class="mw-ui-dropdown-content">
                        <div class="mw-ui-btn-vertical-nav">
                            <?php event_trigger('content.create.menu'); ?>

                            <?php $create_content_menu = mw()->module_manager->ui('content.create.menu'); ?>
                            <?php if (!empty($create_content_menu)) : ?>
                                <?php foreach ($create_content_menu as $type => $item) : ?>
                                    <?php $title = (isset($item['title'])) ? ($item['title']) : false; ?>
                                    <?php $class = (isset($item['class'])) ? ($item['class']) : false; ?>
                                    <?php $html = (isset($item['html'])) ? ($item['html']) : false; ?>
                                    <?php $type = (isset($item['content_type'])) ? ($item['content_type']) : false; ?>
                                    <?php $subtype = (isset($item['subtype'])) ? ($item['subtype']) : false; ?>
                                    <span class="mw-ui-btn <?php print $class; ?>"><a href="<?php print admin_url('view:content'); ?>#action=new:<?php print $type; ?><?php if ($subtype != false) : ?>.<?php print $subtype; ?><?php endif; ?>&amp;parent_page=<?php print $params['page-id'] ?>"> <?php print $title; ?> </a></span>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

            <?php if (isset($params['category-id'])) : ?>
                <?php $edit_link = admin_url('view:content#action=editcategory:' . $params['category-id']); ?>
                <a href="<?php print $edit_link; ?>" class="btn btn-outline-primary btn-sm" id="edit-category-btn"><?php _e("Edit category"); ?></a>
            <?php endif; ?>

            <?php if (isset($content_types) and !empty($content_types)) : ?>
                <div>
                    <select id="content_type_filter_by_select" class="selectpicker" data-style="btn-sm" <?php if (!$selected) : ?> style="display:none" <?php endif; ?>>
                        <option value=""><?php _e('All'); ?></option>
                        <?php foreach ($content_types as $k => $items) : ?>
                            <optgroup label="<?php print ucfirst($k); ?>">
                                <option value="<?php print $k; ?>" <?php if ($k == $selected) : ?> selected="selected" <?php endif; ?>><?php print ucfirst($k); ?></option>
                                <?php foreach ($items as $item) : ?>
                                    <?php if (isset($item['subtype']) and $item['subtype'] != $k) : ?>
                                        <option value="<?php print $k; ?>.<?php print $item['subtype']; ?>" <?php if ($k . '.' . $item['subtype'] == $selected) : ?> selected="selected" <?php endif; ?>><?php print ucfirst($item['subtype']); ?></option>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </optgroup>
                        <?php endforeach; ?>
                    </select>

                    <?php if (!$selected) : ?>
                        <span class="mw-ui-btn mw-icon-menu" onclick="$('#content_type_filter_by_select').toggle(); $(this).hide();"></span>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
            <script>
                $(document).ready(function() {
                    $('.js-search-by-selector').on('change', function() {
                        if ($(this).find('option:selected').val() == 'keywords') {
                            $('.js-search-by-tags').hide();
                            $('.js-search-by-keywords').show();
                        }
                        if ($(this).find('option:selected').val() == 'tags') {
                            $('.js-search-by-tags').show();
                            $('.js-search-by-keywords').hide();
                        }
                    });
                });
            </script>
            <div class="d-inline-block">
                <select class="selectpicker js-search-by-selector" data-width="150" data-style="btn-sm">
                    <option value="keywords" selected><?php _e("search by keyword"); ?></option>
                    <option value="tags"><?php _e("search by tags"); ?></option>
                </select>
            </div>

            <div class="js-search-by d-inline-block">
                <div class="js-hide-when-no-items">
                    <div class="js-search-by-keywords">
                        <div class="form-inline">
                            <div class="input-group mb-0 prepend-transparent mx-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text px-1"><i class="mdi mdi-magnify"></i></span>
                                </div>

                                <input type="text" class="form-control form-control-sm" style="width: 100px;" value="<?php if (isset($params['keyword']) and $params['keyword'] != false) : ?><?php print $params['keyword'] ?><?php endif; ?>" <?php if (isset($params['keyword']) and $params['keyword'] != false) : ?>autofocus="autofocus" <?php endif; ?> placeholder="<?php _e("Search"); ?>" onkeyup="event.keyCode==13?mw.url.windowHashParam('search',this.value):false" />
                            </div>

                            <button type="button" class="btn btn-sm btn-icon" style="background-color:#074A74;color:#fff;border-color:#074A74" onclick="mw.url.windowHashParam('search',$(this).prev().find('input').val())"><i class="mdi mdi-magnify"></i></button>
                        </div>
                    </div>

                    <div class="js-search-by-tags" style="display: none;">
                        <div id="posts-select-tags" class="js-toggle-search-mode-tags d-flex align-items-center" style="width:120px; height: 30px;"></div>
                    </div>
                </div>
            </div>

            <?php mw()->event_manager->trigger('module.content.manager.toolbar.end', $page_info); ?>
        </div>
    </div>
<?php endif; ?>

<?php if ($page_info) : ?>
    <?php mw()->event_manager->trigger('module.content.manager.toolbar', $page_info) ?>
<?php endif; ?>
<?php $custom_tabs = mw()->module_manager->ui('content.manager.toolbar'); ?>
<?php if (!empty($custom_tabs)) : ?>
    <div id="manage-content-toolbar-tabs">
        <div class="mw-ui-btn-nav mw-ui-btn-nav-tabs" id="manage-content-toolbar-tabs-nav">
            <?php foreach ($custom_tabs as $item) : ?>
                <?php $title = (isset($item['title'])) ? ($item['title']) : false; ?>
                <?php $class = (isset($item['class'])) ? ($item['class']) : false; ?>
                <?php $html = (isset($item['html'])) ? ($item['html']) : false; ?>
                <a class="mw-ui-btn tip" data-tip="<?php print $title; ?>"> <span class="<?php print $class; ?>"></span> <span> <?php print $title; ?> </span>
                </a>
            <?php endforeach; ?>
        </div>
        <div class="mw-ui-box">
            <?php foreach ($custom_tabs as $item) : ?>
                <?php $title = (isset($item['title'])) ? ($item['title']) : false; ?>
                <?php $class = (isset($item['class'])) ? ($item['class']) : false; ?>
                <?php $html = (isset($item['html'])) ? ($item['html']) : false; ?>
                <div class="mw-ui-box-content" style="display: none;"><?php print $html; ?></div>
            <?php endforeach; ?>
        </div>
    </div>
<?php endif; ?>
<?php if (!isset($edit_page_info)) : ?>
    <div class="card-body pt-3 pb-0">
        <div class="toolbar row js-hide-when-no-items">
            <div class="col-sm-6 d-flex align-items-center justify-content-center justify-content-sm-start my-1">
                <div class="custom-control custom-checkbox mb-0">
                    <input type="checkbox" class="custom-control-input" id="posts-check">
                    <label class="custom-control-label" for="posts-check"><?php _e("Check all"); ?></label>
                </div>

                <div class="d-inline-block ml-3">
                    <div class="js-bulk-actions" style="display: none;">
                        <select class="selectpicker js-bulk-action" title="Bulk actions" data-style="btn-sm" data-width="auto">

                            <?php
                            if (user_can_access('module.content.edit')) :
                                ?>
                                <option value="assign_selected_posts_to_category"><?php _e("Move to category"); ?></option>
                                <option value="publish_selected_posts"><?php _e("Published"); ?></option>
                                <option value="unpublish_selected_posts"><?php _e("Unpublish"); ?></option>
                            <?php endif; ?>

                            <?php
                            if (user_can_access('module.content.destroy')) :
                                ?>
                                <option value="delete_selected_posts"><?php _e("Delete"); ?></option>
                            <?php endif; ?>

                        </select>
                    </div>
                </div>
            </div>
            <script>
                $('.select_posts_for_action').on('change', function() {
                    var all = mwd.querySelector('.select_posts_for_action:checked');
                    if (all === null) {
                        $('.js-bulk-actions').hide();
                    } else {
                        $('.js-bulk-actions').show();
                    }
                });

                $('.js-bulk-action').on('change', function() {
                    var selectedBulkAction = $('.js-bulk-action option:selected').val();
                    if (selectedBulkAction == 'assign_selected_posts_to_category') {
                        assign_selected_posts_to_category();
                    } else if (selectedBulkAction == 'publish_selected_posts') {
                        publish_selected_posts();
                    } else if (selectedBulkAction == 'unpublish_selected_posts') {
                        unpublish_selected_posts();
                    } else if (selectedBulkAction == 'delete_selected_posts') {
                        delete_selected_posts();
                    }
                });
            </script>
            <?php
            $order_by_field = '';
            $order_by_type = '';
            if (isset($params['data-order'])) {
                $explode_date_order = explode(' ', $params['data-order']);
                if (isset($explode_date_order[1])) {
                    $order_by_field = $explode_date_order[0];
                    $order_by_type = $explode_date_order[1];
                }
            }
            ?>

            <div class="js-table-sorting col-sm-6 text-right my-1 d-flex justify-content-center justify-content-sm-end align-items-center">
                <!-- blog post option -->
               <?php if ($act == 'posts') : ?>
                    <?php $rssLink = get_option('rss_link', 'rss_data') ?? null;
                        if ($rssLink != null) : ?>
                            <?php $rssOption = get_option('rss_option', 'rss_data'); ?>
                            <select onchange="rss_option()" id="rssOption" name="rssOption" class="selectpicker js-search-by-selector" data-width="120" data-style="btn-sm" tabindex="-98" aria-label="RSS option selected">
                                <option selected><?php _e('Add your option'); ?></option>
                                <option <?php if (intval($rssOption) == 0) {
                                    echo "selected";
                                        } ?> value="0"><?php _e('Own Posts'); ?></option>
                                <option <?php if (intval($rssOption) == 1) {
                                    echo "selected";
                                        } ?> value="1"><?php _e('Merge RSS'); ?></option>
                                <option <?php if (intval($rssOption) == 2) {
                                    echo "selected";
                                        } ?> value="2"><?php _e('Only RSS'); ?></option>
                            </select>
                    <?php endif; ?>
                <?php endif; ?>
                <span><?php _e("Sort By"); ?>:</span>

                <div class="d-inline-block mx-1">
                    <button type="button" class="js-sort-btn btn btn-outline-secondary btn-sm icon-right" data-state="<?php if ($order_by_field == 'created_at') : ?><?php echo $order_by_type;
                    else : echo 'DESC'; ?><?php endif; ?>" data-sort-type="created_at" onclick="postsSort({id:'pages_edit_container_content_list', el:this});">
                        <?php _e("Date"); ?> <i class="mdi mdi-chevron-down text-muted"></i>
                    </button>
                </div>

                <div class="d-inline-block">
                    <button type="button" class="js-sort-btn btn btn-outline-secondary btn-sm icon-right" data-state="<?php if ($order_by_field == 'title') : ?><?php echo $order_by_type; ?><?php endif; ?>" data-sort-type="title" onclick="postsSort({id:'pages_edit_container_content_list', el:this});">
                        <?php _e("Title"); ?> <i class="mdi mdi-chevron-down text-muted"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<script>
    $("#rss-submit").click(function() {
        alert("Handler for .click() called.");
    });
</script>


<script>
    $(document).ready(function() {
        var el = $("#posts-check")
        el.on('change', function() {
            mw.check.toggle('#mw_admin_posts_sortable');

            var all = $('#mw_admin_posts_sortable input[type="checkbox"]');
            var checked = all.filter(':checked');
            if (checked.length && checked.length === all.length) {
                el[0].checked = true;
            } else {
                el[0].checked = false;
            }

            var all = mwd.querySelector('.select_posts_for_action:checked');
            if (all === null) {
                $('.js-bulk-actions').hide();
            } else {
                $('.js-bulk-actions').show();
            }
        });

    });
</script>

<script>
    mw.require('forms.js', true);


    $(document).ready(function() {
        var postsSelectTags = mw.select({
            element: '#posts-select-tags',
            placeholder: 'Filter by tag',
            multiple: true,
            autocomplete: true,
            size: 'small',
            tags: false,
            ajaxMode: {
                paginationParam: 'page',
                searchParam: 'keyword',
                endpoint: mw.settings.api_url + 'tagging_tag/autocomplete',
                method: 'get'
            }
        });

        $(postsSelectTags).on("change", function(event, val) {
            var parent_mod = mwd.getElementById('pages_edit_container_content_list');
            parent_mod.setAttribute('tags', '');
            if (val.length > 0) {

                var tagSeperated = '';
                for (i = 0; i < val.length; i++) {
                    tagSeperated += val[i].title + ',';
                }

                parent_mod.setAttribute('tags', tagSeperated);
            }
            mw.reload_module(parent_mod);
        });
    });

    postsSort = function(obj) {

        var group = mw.tools.firstParentWithClass(obj.el, 'js-table-sorting');
        var parent_mod = mwd.getElementById('pages_edit_container_content_list');


        var others = group.querySelectorAll('.js-sort-btn'),
            i = 0,
            len = others.length;
        for (; i < len; i++) {
            var curr = others[i];
            if (curr !== obj.el) {
                $(curr).removeClass('ASC DESC active');
            }
        }
        obj.el.attributes['data-state'] === undefined ? obj.el.setAttribute('data-state', 0) : '';
        var state = obj.el.attributes['data-state'].nodeValue;

        var jQueryEl = $(obj.el);

        var tosend = {}
        tosend.type = obj.el.attributes['data-sort-type'].nodeValue;
        if (state === '0') {
            tosend.state = 'ASC';
            //            obj.el.className = 'js-sort-btn btn btn-outline-primary btn-sm icon-right ASC';
            obj.el.setAttribute('data-state', 'ASC');

            jQueryEl.find('i').removeClass('mdi-chevron-down');
            jQueryEl.find('i').addClass('mdi-chevron-up');
        } else if (state === 'ASC') {
            tosend.state = 'DESC';
            //            obj.el.className = 'js-sort-btn btn btn-outline-primary btn-sm icon-right DESC';
            obj.el.setAttribute('data-state', 'DESC');

            jQueryEl.find('i').removeClass('mdi-chevron-up');
            jQueryEl.find('i').addClass('mdi-chevron-down');
        } else if (state === 'DESC') {
            tosend.state = 'ASC';
            //            obj.el.className = 'js-sort-btn btn btn-outline-primary btn-sm icon-right ASC';
            obj.el.setAttribute('data-state', 'ASC');

            jQueryEl.find('i').removeClass('mdi-chevron-down');
            jQueryEl.find('i').addClass('mdi-chevron-up');
        } else {
            tosend.state = 'ASC';
            //            obj.el.className = 'js-sort-btn btn btn-outline-primary btn-sm icon-right ASC';
            obj.el.setAttribute('data-state', 'ASC');

            jQueryEl.find('i').removeClass('mdi-chevron-down');
            jQueryEl.find('i').addClass('mdi-chevron-up');
        }

        if (parent_mod !== undefined) {
            parent_mod.setAttribute('data-order', tosend.type + ' ' + tosend.state);
            mw.reload_module(parent_mod);
        }
    }
</script>

