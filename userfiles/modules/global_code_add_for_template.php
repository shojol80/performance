<script type="text/javascript" src="<?php print modules_url(); ?>microweber/js/bootstrap-toggle.min.js"></script>
<link href="<?php print modules_url(); ?>microweber/css/bootstrap-toggle.min.css" rel="stylesheet"/>

<script>
    $(document).ready(function() {

        if(window.location.href.indexOf("#loginModal") != -1) {
        $("#loginModal").modal("show");
        }

});
</script>

<script type="text/javascript">
    window.base_url = "<?php print url("/"); ?>";
</script>

<script>
    $(document).ready(function() {

        setInterval(() => {
            $('.ordreChat_Modal').each(function() {
                if ($(this).is(':visible')) {
                    let order_id = $(this).attr('data-order-id');
                    load_order_chat(order_id);
                }
            });
        }, 5000);

        function load_order_chat(id) {
            $.post("<?= url('/') ?>/api/v1/get_chat", {
                id: id
            }, res => res.data)
                .then(res => res.data)
                .then(data => {
                    if (data.messages) {
                        let message_list = '';
                        data.messages.forEach(msg => {
                            let recipient = msg.recipient;
                            let sender = msg.sender
                            console.log(msg);
                            message_list += `
                                <li class="clearfix getChat_massage">
                                        <div class="order-outgoing-msg-info message-data ${recipient == 'me'? 'text-right' : ''}">
                                        <span class="message-data-time" title="${msg.created_at}">
                                            <span class="name">${sender && sender.name ? sender.name : ''}</span>,
                                            <span class="time">${msg.time}</span>
                                        </span>
                                        <br>
                                        <span class="email">${sender && sender.email? sender.email : ''}</span>
                                    </div>
                                    <div class="message ${recipient == 'me'? 'my-message float-right' : 'other-message'}">${msg.message}</div>
                                </li>
                            `;
                        })

                        let el_id = `#order-chat-modal-` + id + ` #chat-area` + id;
                        $(el_id).html(message_list)
                        $(`#notifi-${id}`).html('00')
                        $("#chat-area" + id).stop().animate({
                            scrollTop: $("#chat-area" + id)[0].scrollHeight
                        }, 1000);
                    }





                })
                .then(o => {
                    $.post("<?= url('/') ?>/api/v1/seen_chat", {
                        id: id
                    }).catch(err => {})
                })
                .catch(err => {
                    console.log(err)
                })
        }
    })
</script>

<?php
if( strpos( $_SERVER['HTTP_ACCEPT'], 'image/webp' ) === false ) {
    ?>
    <script>
        $(document).ready(function() {

            $('.rawurlstyle').each(function(){
                var valuerow = $(this).attr('data-raw-url');
                $(this).css("backgroundImage","url("+ valuerow +")");
            });

            $('.rawurlsrc').each(function(){
                var valuerow = $(this).attr('data-raw-url');
                $(this).attr("src",valuerow);
            });
        });
    </script>
    <?php
}
?>
