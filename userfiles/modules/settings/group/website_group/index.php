<script>
    $(document).ready(function () {
        $('body .main > main').addClass('page-settings');
    });
</script>

<?php
$show_inner = false;

if (isset($_GET['group']) and $_GET['group']) {
    $group = $_GET['group'];

    if ($group == 'general') {
        $show_inner = 'settings/group/website';
    } elseif ($group == 'updates') {
        $show_inner = 'updates';
    } elseif ($group == 'email') {
        $show_inner = 'settings/group/email';
    } elseif ($group == 'template') {
        $show_inner = 'settings/group/template';
    } elseif ($group == 'advanced') {
        $show_inner = 'settings/group/advanced';
    } elseif ($group == 'files') {
        $show_inner = 'files/admin';
    } elseif ($group == 'login') {
        $show_inner = 'settings/group/users';
    } elseif ($group == 'language') {
        $show_inner = 'settings/group/language';
    }
    //elseif ($group == 'privacy') {
      //  $show_inner = 'settings/group/privacy';
    //}
    elseif ($group == 'searchhits') {
        $show_inner = 'settings/group/searchhits';
    }elseif ($group == 'postlimit') {
        $show_inner = 'settings/group/postlimit';
    }elseif ($group == 'blog_menu') {
        $show_inner = 'settings/group/blog_menu';
    }elseif ($group == 'seo') {
        $show_inner = 'settings/group/seo_settings';
    } elseif ($group == 'performance') {
        $show_inner = 'settings/group/performance';
    }else{
        $show_inner = false;
    }
}
?>

<?php if ($show_inner): ?>
    <module type="<?php print $show_inner ?>"/>
    <?php return; ?>
<?php endif ?>

<div class="card bg-none style-1 mb-0">
    <div class="card-header px-0">
        <h5><i class="mdi mdi-earth text-primary mr-3"></i> <strong><?php _e('Website settings'); ?></strong></h5>
        <div>

        </div>
    </div>
    <?php
        if(Schema::hasTable('admin_website_menu')) {
            $website_setting_menus = DB::table('admin_website_menu')->where('shortcut', 0)->orderBy('position', 'asc')->get()->toArray();    
        }else{
            $website_setting_menus = [];
        }
    ?>
    <div class="card-body pt-3 px-0">
        <div class="card style-1 mb-3">
            <div class="card-body pt-3 px-5">
                <div class="row select-settings" id="website-setting-sortable" data-type="0">
                    <?php foreach($website_setting_menus as  $setting_manu): ?>    
                        <?php
                            if($setting_manu->link){
                                $menu_link = $setting_manu->link;
                            }else if($setting_manu->mw_link){
                                $menu_link = '?'.$setting_manu->mw_link;
                            }else if($setting_manu->dt_link){
                                $menu_link = admin_url().$setting_manu->dt_link;
                            }else if($setting_manu->dt_temp_link){
                                $menu_link = site_url().$setting_manu->dt_temp_link;
                            }else{
                                $menu_link = "#";
                                if($setting_manu->onclick){
                                    $onclicklink = $setting_manu->onclick;
                                }else{
                                    $onclicklink = "";
                                }
                            }
                        ?>
                        
                        <div class="col-12 col-sm-6 col-lg-4 " data-shortcut="<?php print $setting_manu->shortcut ?>" data-index="<?php print $setting_manu->id ?>">
                            <a href="<?php print $menu_link; ?>" class="d-flex my-3" <?php if($setting_manu->onclick){ print  $onclicklink; } ?>>
                                <div class="icon-holder">
                                    <?php if($setting_manu->icon): ?>
                                        <i class="<?php print $setting_manu->icon;  ?>"></i>
                                    <?php elseif($setting_manu->img): ?>
                                        <img src="<?php print modules_url().$setting_manu->img; ?>"  alt="">
                                    <?php else: ?>
                                        <i class="mdi mdi-cog mdi-20px"></i>
                                    <?php endif; ?>
                                
                                </div>
                                <div class="info-holder">
                                    <span class="text-primary font-weight-bold"><?php _e($setting_manu->name); ?></span><br/>
                                    <small class="text-muted"><?php _e($setting_manu->sub_name);  ?></small>
                                </div>
                            </a>
                        </div>
                    <?php endforeach; ?>   
                    <?php if(isset($website_setting_menus) && empty($website_setting_menus)): ?>
                        <div class="empty-website-setting-menu"> <?php _e("Empty Website Setting Menu"); ?> </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

