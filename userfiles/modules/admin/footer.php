</div>
<!--  /.main.container -->
</div>
<!--  /#mw-admin-main-block -->
<?php
$usertokenDrm = Config::get('microweber.userToken');
$modules_url = modules_url();

// $excludeurl = Config::get('global.exluded_url');

// if (function_exists('dt_admin_contains_any') && dt_admin_contains_any(url()->current(),$excludeurl) ) {
//  $usertokenDrm=true;
// }

?>
<div id="myModal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: <?php if (isset($usertokenDrm) && !empty($usertokenDrm)) { ?>none;<?php } else {  ?> block; <?php } ?>">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">

            </div>
            <div class="js-login-window">
                <!-- <div class="modal-header">
        <h3 id="myModalLabel">Verbinde jetzt DROPTIENDA® mit DROPMATIX®</h3>
    </div> -->
                <div class="modal-body">
                    <div class="drm-login-image">
                        <img src="<?php print $modules_url; ?>drm-login-image.jpg" alt="">
                        <div class="image-heading">
                            <h5>Verbinde jetzt DROPTIENDA® mit DROPMATIX®</h5>
                        </div>
                    </div>
                    <p style="font-weight:bold">Du hast bereits einen DROPMATIX Account?
                        Dann logge dich jetzt ein.</p>
                    <p id="false_txt"></p>

                    <div class="form-group">
                        <label for="installUserInput" class="mw-ui-label">Nutzername</label>
                        <input type="text" class="form-control" id="installUserInput" name="installUserName" placeholder="User Name">
                    </div>
                    <div class="form-group">
                        <label for="installUserPass" class="mw-ui-label">Passwort</label>
                        <input type="password" class="form-control" id="installUserPass" name="installUserPass" placeholder="Password">
                    </div>
                    <p id="false_txt" class="text-danger"></p>
                    <a href="https://drm.software/registration/sign-up" target="_blank" style="display: block;margin-bottom: 10px;text-decoration: underline;text-align:right;color:#074A74">Passwort vergessen ?</a>
                    <div class="form-group">
                        <div class="admin_login-btn">
                            <button type="submit" class="btn btn-custom action-button" id="close_btn">Anmeldung</button>
                            <!-- <span class="tooltiptext">Stimmen Sie mit unserem überein Geschäftsbedingungen</span> -->

                        </div>
                    </div>

                    <p class="or"><span>oder</span></p>
                    <div class="act create">
                        <a href="#" class="js-show-register-window"><span field="login_massage_two" rel="module">Einen neuen Account erstellen<i class="fa fa-forward" style="margin-left: 10px;" aria-hidden="true"></i></span></a>
                    </div>
                </div>

            </div>
            <div class="js-register-window">
                <div class="modal-body">
                    <h3 style="text-align: center;">Erstelle jetzt einen neuen Account.</h3>
                    <p style="text-align: center;font-weight:600">Wir freuen uns, dich in unserer Community begrüßen zu dürfen. </p>
                    <div class="form-group">
                        <input class="form-control input-lg" type="text" id="installUserInputNamer" name="installUserNameCheckr" placeholder="<?php _lang('Name', "templates/bamboo"); ?>">
                    </div>

                    <div class="form-group">
                        <input class="form-control input-lg" type="email" id="installUserInputr" name="installUserNamer" placeholder="<?php _lang('Email', "templates/bamboo"); ?>">
                    </div>

                    <div class="form-group m-t-20">
                        <input class="form-control input-lg" type="password" id="installUserPassr" name="installUserPassr" placeholder="<?php _lang('Passwort', "templates/bamboo"); ?>">
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" class="terms_checkbox">ich akzeptiere die <a href="https://drm.software/term-and-conditions" style="text-decoration: underline;color:#000;" target="_blank">AGB</a>
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" class="terms_checkbox">ich akzeptiere die <a href="https://drm.software/privacy-policy" style="text-decoration: underline;color:#000;" target="_blank">Datenschutzerklärung</a>
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" class="terms_checkbox"> Ich willige ein, dass meine Daten wie Namen, Vorname, Adresse, Telefonnummer, Email- und IP-Adresse sowie Kontodaten und Kredikarteninformationen in Drittländer transferiert werden. Dort können unter Umständen Polizeibehörden und ggf. Geheimdienste auf die Daten zugreifen.
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" class="terms_checkbox"> Ich bin damit einverstanden im Falle eines Verkaufes eine pauschale Transaktionsgebühr von 0,30 Cent zzgl. 1,4 % des Brutto Verkaufspreises zum Ende jedes laufenden Monats zu bezahlen.
                        </label>
                    </div>
                    <div class="admin_login-btn">
                        <button type="submit" class="btn btn-custom drm-reg-btn" id="close_btn" disabled>Jetzt anmelden und Shop verbinden</button>
                    </div>
                    <p class="or"><span>oder</span></p>
                    <div class="act create">
                        <a href="#" class="js-show-login-window"><span><i class="fa fa-backward" style="margin-right: 10px;" aria-hidden="true"></i>zurück zum Login</span></a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<!--After Registration Modal -->
<div class="modal" id="afterReg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body" style="text-align: center;">
                <h3>Sie haben sich erfolgreich registriert.</h3>
                <h4>Bitte überprüfen Sie die E-Mail, um Ihre zu aktivieren <a href="http://drm.software/" style="display:inline-block">DRM</a> Konto. Unmittelbar danach ist Ihr Geschäft einsatzbereit.</h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>

<?php if (is_admin()) {
    (function_exists('licence_drm')) ? print licence_drm() : '';
} ?>


<script src="<?php print $modules_url; ?>/admin/js/jquery-image-scroll.js"></script>

<script>
    $(window).on('load', function() {

        $('.screen').scrollImage();
    });


    function setConfig(userT, userP) {
        console.log('here');


        $.post("<?= api_url('config_set_drm') ?>", {
            userToken: userT,
            userPassToken: userP
        });
    }

    $(document).ready(function() {


        $(".action-button").on("click", function() {
            var email = document.querySelector("#installUserInput").value;
            var password = document.querySelector("#installUserPass").value;
            console.log(email, password);


            $.post("<?= url('/') ?>/api/v1/drm_token_get", {
                installUserName: email,
                installUserPass: password,
                url: "<?= url('/') ?>"
            }, (res) => {

                if (res.success) {


                    console.log('dfhfiasfuhiasu');

                    setConfig(res.userToken, res.userPassToken)

                    $('#myModal').hide();

                    // return false;

                } else {
                    // console.log(res)
                    var htmlvalue = res.message;
                    $("#false_txt").html(htmlvalue);
                    return false;
                }

            });

        });
        $(".drm-reg-btn").on("click", function() {
            var name = document.querySelector("#installUserInputNamer").value;
            var email = document.querySelector("#installUserInputr").value;
            var password = document.querySelector("#installUserPassr").value;
            var register = "dt";
            console.log(email, password);


            $.post("<?= url('/') ?>/api/v1/drm_token_get", {
                name: name,
                installUserName: email,
                installUserPass: password,
                url: "<?= url('/') ?>",
                is_register: register
            }, (res) => {

                if (res.success) {



                    // console.log('dfhfiasfuhiasu');

                    setConfig(res.userToken, res.userPassToken)

                    $('#myModal').hide();
                    $('#afterReg').modal("show");
                    // return false;

                } else {
                    // console.log(res)
                    var htmlvalue = res.message;
                    $("#false_txt").html(htmlvalue);
                    return false;
                }

            });

        });

        $(".terms_checkbox").change(function() {
            if ($('.terms_checkbox:checked').length == $('.terms_checkbox').length) {
                $('.drm-reg-btn').removeAttr('disabled');
                $('.tooltiptext').hide();
            } else {
                $('.drm-reg-btn').prop('disabled', true);
                $('.tooltiptext').show();
            }
        });
        $('.js-register-window').hide();
        $('.js-show-register-window').on('click', function() {
            $('.js-login-window').hide();
            $('.js-register-window').show();
        });
        $('.js-show-login-window').on('click', function() {

            $('.js-register-window').hide();
            $('.js-login-window').show();
        });
    });
</script>


</body>

</html>